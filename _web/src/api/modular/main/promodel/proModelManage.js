import { axios } from '@/utils/request'

/**
 * 查询产品型号表
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
export function proModelPage (parameter) {
  return axios({
    url: '/proModel/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 产品型号表列表
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
export function proModelList (parameter) {
  return axios({
    url: '/proModel/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加产品型号表
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
export function proModelAdd (parameter) {
  return axios({
    url: '/proModel/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑产品型号表
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
export function proModelEdit (parameter) {
  return axios({
    url: '/proModel/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除产品型号表
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
export function proModelDelete (parameter) {
  return axios({
    url: '/proModel/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出产品型号表
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
export function proModelExport (parameter) {
  return axios({
    url: '/proModel/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
