import { axios } from '@/utils/request'

/**
 * 查询工单表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
export function workOrderPage (parameter) {
  return axios({
    url: '/workOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 工单表列表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
export function workOrderList (parameter) {
  return axios({
    url: '/workOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加工单表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
export function workOrderAdd (parameter) {
  return axios({
    url: '/workOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑工单表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
export function workOrderEdit (parameter) {
  return axios({
    url: '/workOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除工单表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
export function workOrderDelete (parameter) {
  return axios({
    url: '/workOrder/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出工单表
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
export function workOrderExport (parameter) {
  return axios({
    url: '/workOrder/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
