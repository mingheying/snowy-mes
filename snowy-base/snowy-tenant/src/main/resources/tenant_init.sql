/*
 Navicat Premium Data Transfer

 Source Server         : 本机Mysql
 Source Server Type    : MySQL
 Source Server Version : 50710
 Source Host           : localhost:3306
 Source Schema         : snowy

 Target Server Type    : MySQL
 Target Server Version : 50710
 File Encoding         : 65001

 Date: 22/12/2021 10:42:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for act_de_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_de_databasechangelog`;
CREATE TABLE `act_de_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_databasechangelog
-- ----------------------------
INSERT INTO `act_de_databasechangelog` VALUES ('1', 'flowable', 'META-INF/liquibase/flowable-modeler-app-db-changelog.xml', '2021-12-21 22:19:06', 1, 'EXECUTED', '8:e70d1d9d3899a734296b2514ccc71501', 'createTable tableName=ACT_DE_MODEL; createIndex indexName=idx_proc_mod_created, tableName=ACT_DE_MODEL; createTable tableName=ACT_DE_MODEL_HISTORY; createIndex indexName=idx_proc_mod_history_proc, tableName=ACT_DE_MODEL_HISTORY; createTable tableN...', '', NULL, '3.8.9', NULL, NULL, '0096346278');
INSERT INTO `act_de_databasechangelog` VALUES ('3', 'flowable', 'META-INF/liquibase/flowable-modeler-app-db-changelog.xml', '2021-12-21 22:19:06', 2, 'EXECUTED', '8:3a9143bef2e45f2316231cc1369138b6', 'addColumn tableName=ACT_DE_MODEL; addColumn tableName=ACT_DE_MODEL_HISTORY', '', NULL, '3.8.9', NULL, NULL, '0096346278');

-- ----------------------------
-- Table structure for act_de_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_de_databasechangeloglock`;
CREATE TABLE `act_de_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_databasechangeloglock
-- ----------------------------
INSERT INTO `act_de_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_de_model
-- ----------------------------
DROP TABLE IF EXISTS `act_de_model`;
CREATE TABLE `act_de_model`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `model_key` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `model_comment` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_updated` datetime(6) NULL DEFAULT NULL,
  `last_updated_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `version` int(11) NULL DEFAULT NULL,
  `model_editor_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `thumbnail` longblob NULL,
  `model_type` int(11) NULL DEFAULT NULL,
  `tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_proc_mod_created`(`created_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_de_model_history
-- ----------------------------
DROP TABLE IF EXISTS `act_de_model_history`;
CREATE TABLE `act_de_model_history`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `model_key` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `model_comment` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_updated` datetime(6) NULL DEFAULT NULL,
  `last_updated_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `removal_date` datetime(6) NULL DEFAULT NULL,
  `version` int(11) NULL DEFAULT NULL,
  `model_editor_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `model_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `model_type` int(11) NULL DEFAULT NULL,
  `tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_proc_mod_history_proc`(`model_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_model_history
-- ----------------------------
INSERT INTO `act_de_model_history` VALUES ('1df99c91-62d0-11ec-85b9-488ad28256ed', '请假管理', 'leave_01', '请假管理流程', NULL, '2021-12-22 03:19:50.874000', 'superAdmin', '2021-12-22 03:19:50.874000', 'superAdmin', '2021-12-22 10:37:33.726000', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"leave_01\",\"name\":\"请假管理\",\"documentation\":\"请假管理流程\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', 'f82f9def-6292-11ec-ba55-488ad28256ed', 0, NULL);

-- ----------------------------
-- Table structure for act_de_model_relation
-- ----------------------------
DROP TABLE IF EXISTS `act_de_model_relation`;
CREATE TABLE `act_de_model_relation`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `parent_model_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `model_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `relation_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_relation_parent`(`parent_model_id`) USING BTREE,
  INDEX `fk_relation_child`(`model_id`) USING BTREE,
  CONSTRAINT `fk_relation_child` FOREIGN KEY (`model_id`) REFERENCES `act_de_model` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_relation_parent` FOREIGN KEY (`parent_model_id`) REFERENCES `act_de_model` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_evt_log
-- ----------------------------
DROP TABLE IF EXISTS `act_evt_log`;
CREATE TABLE `act_evt_log`  (
  `LOG_NR_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` longblob NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`LOG_NR_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ge_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_bytearray`;
CREATE TABLE `act_ge_bytearray`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  `GENERATED_` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_BYTEARR_DEPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ge_property
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_property`;
CREATE TABLE `act_ge_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ge_property
-- ----------------------------
INSERT INTO `act_ge_property` VALUES ('batch.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('cfg.execution-related-entities-count', 'true', 1);
INSERT INTO `act_ge_property` VALUES ('cfg.task-related-entities-count', 'true', 1);
INSERT INTO `act_ge_property` VALUES ('common.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('entitylink.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('eventsubscription.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('identitylink.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('job.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('next.dbid', '1', 1);
INSERT INTO `act_ge_property` VALUES ('schema.history', 'create(6.5.0.6)', 1);
INSERT INTO `act_ge_property` VALUES ('schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('task.schema.version', '6.5.0.6', 1);
INSERT INTO `act_ge_property` VALUES ('variable.schema.version', '6.5.0.6', 1);

-- ----------------------------
-- Table structure for act_hi_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_actinst`;
CREATE TABLE `act_hi_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_PROCINST`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_EXEC`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_attachment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_attachment`;
CREATE TABLE `act_hi_attachment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `URL_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONTENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_comment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_comment`;
CREATE TABLE `act_hi_comment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `MESSAGE_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `FULL_MSG_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_detail
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_detail`;
CREATE TABLE `act_hi_detail`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_ACT_INST`(`ACT_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TIME`(`TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_NAME`(`NAME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TASK_ID`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_entitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_entitylink`;
CREATE TABLE `act_hi_entitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_identitylink`;
CREATE TABLE `act_hi_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_procinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_procinst`;
CREATE TABLE `act_hi_procinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `END_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `PROC_INST_ID_`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_I_BUSKEY`(`BUSINESS_KEY_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_taskinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_taskinst`;
CREATE TABLE `act_hi_taskinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_INST_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_tsk_log
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_tsk_log`;
CREATE TABLE `act_hi_tsk_log`  (
  `ID_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_varinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_varinst`;
CREATE TABLE `act_hi_varinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_NAME_TYPE`(`NAME_`, `VAR_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_EXE`(`EXECUTION_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_id_bytearray`;
CREATE TABLE `act_id_bytearray`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_group
-- ----------------------------
DROP TABLE IF EXISTS `act_id_group`;
CREATE TABLE `act_id_group`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_info
-- ----------------------------
DROP TABLE IF EXISTS `act_id_info`;
CREATE TABLE `act_id_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PASSWORD_` longblob NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_membership
-- ----------------------------
DROP TABLE IF EXISTS `act_id_membership`;
CREATE TABLE `act_id_membership`  (
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`, `GROUP_ID_`) USING BTREE,
  INDEX `ACT_FK_MEMB_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_priv
-- ----------------------------
DROP TABLE IF EXISTS `act_id_priv`;
CREATE TABLE `act_id_priv`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PRIV_NAME`(`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_priv_mapping
-- ----------------------------
DROP TABLE IF EXISTS `act_id_priv_mapping`;
CREATE TABLE `act_id_priv_mapping`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PRIV_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_PRIV_MAPPING`(`PRIV_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PRIV_MAPPING` FOREIGN KEY (`PRIV_ID_`) REFERENCES `act_id_priv` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_property
-- ----------------------------
DROP TABLE IF EXISTS `act_id_property`;
CREATE TABLE `act_id_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_property
-- ----------------------------
INSERT INTO `act_id_property` VALUES ('schema.version', '6.5.0.6', 1);

-- ----------------------------
-- Table structure for act_id_token
-- ----------------------------
DROP TABLE IF EXISTS `act_id_token`;
CREATE TABLE `act_id_token`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TOKEN_VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATE_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `IP_ADDRESS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_AGENT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATA_` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_user
-- ----------------------------
DROP TABLE IF EXISTS `act_id_user`;
CREATE TABLE `act_id_user`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `FIRST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LAST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DISPLAY_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EMAIL_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PWD_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PICTURE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_procdef_info
-- ----------------------------
DROP TABLE IF EXISTS `act_procdef_info`;
CREATE TABLE `act_procdef_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_INFO_JSON_BA`(`INFO_JSON_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_re_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_re_deployment`;
CREATE TABLE `act_re_deployment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_re_model
-- ----------------------------
DROP TABLE IF EXISTS `act_re_model`;
CREATE TABLE `act_re_model`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `META_INFO_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE`(`EDITOR_SOURCE_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE_EXTRA`(`EDITOR_SOURCE_EXTRA_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_DEPLOYMENT`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_re_procdef
-- ----------------------------
DROP TABLE IF EXISTS `act_re_procdef`;
CREATE TABLE `act_re_procdef`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_VERSION_` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PROCDEF`(`KEY_`, `VERSION_`, `DERIVED_VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_actinst`;
CREATE TABLE `act_ru_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_PROC`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_PROC_ACT`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_EXEC`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_EXEC_ACT`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_deadletter_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_deadletter_job`;
CREATE TABLE `act_ru_deadletter_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_entitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_entitylink`;
CREATE TABLE `act_ru_entitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_event_subscr
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_event_subscr`;
CREATE TABLE `act_ru_event_subscr`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONFIGURATION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EVENT_SUBSCR_CONFIG_`(`CONFIGURATION_`) USING BTREE,
  INDEX `ACT_FK_EVENT_EXEC`(`EXECUTION_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_execution`;
CREATE TABLE `act_ru_execution`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) NULL DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) NULL DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_MI_ROOT_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `CACHED_ENT_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) NULL DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int(11) NULL DEFAULT NULL,
  `TASK_COUNT_` int(11) NULL DEFAULT NULL,
  `JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `TIMER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `SUSP_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXEC_BUSKEY`(`BUSINESS_KEY_`) USING BTREE,
  INDEX `ACT_IDC_EXEC_ROOT`(`ROOT_PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_SUPER`(`SUPER_EXEC_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_history_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_history_job`;
CREATE TABLE `act_ru_history_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ADV_HANDLER_CFG_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_identitylink`;
CREATE TABLE `act_ru_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_GROUP`(`GROUP_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ATHRZ_PROCEDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_TSKASS_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_IDL_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_job`;
CREATE TABLE `act_ru_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_suspended_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_suspended_job`;
CREATE TABLE `act_ru_suspended_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_task
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_task`;
CREATE TABLE `act_ru_task`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELEGATION_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) NULL DEFAULT NULL,
  `SUB_TASK_COUNT_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TASK_CREATE`(`CREATE_TIME_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TASK_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_timer_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_timer_job`;
CREATE TABLE `act_ru_timer_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TIMER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_variable
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_variable`;
CREATE TABLE `act_ru_variable`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_VAR_BYTEARRAY`(`BYTEARRAY_ID_`) USING BTREE,
  INDEX `ACT_IDX_VARIABLE_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_button
-- ----------------------------
DROP TABLE IF EXISTS `flw_button`;
CREATE TABLE `flw_button`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `process_definition_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程实例id',
  `act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动节点id',
  `act_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动节点名称',
  `submit_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '提交（Y-是，N-否）',
  `save_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '保存（Y-是，N-否）',
  `back_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退回（Y-是，N-否）',
  `turn_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转办（Y-是，N-否）',
  `next_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '指定（Y-是，N-否）',
  `entrust_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '委托（Y-是，N-否）',
  `end_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '终止（Y-是，N-否）',
  `trace_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '追踪（Y-是，N-否）',
  `suspend_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '挂起（Y-是，N-否）',
  `jump_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转（Y-是，N-否）',
  `add_sign_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '加签（Y-是，N-否）',
  `delete_sign_btn` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '减签（Y-是，N-否）',
  `version` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程节点按钮表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_category
-- ----------------------------
DROP TABLE IF EXISTS `flw_category`;
CREATE TABLE `flw_category`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
  `sort` int(11) NOT NULL COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_category
-- ----------------------------
INSERT INTO `flw_category` VALUES (1290140237082759170, '考勤类', 'kaoqin', 100, '考勤类', 0, '2020-08-03 12:19:37', 1265476890672672808, '2020-08-06 16:18:18', 1265476890672672808);
INSERT INTO `flw_category` VALUES (1290670559956320258, '行政类', 'xingzheng', 100, '行政类', 0, '2020-08-04 23:26:56', 1265476890672672808, '2020-08-06 16:13:59', 1265476890672672808);
INSERT INTO `flw_category` VALUES (1308401074679459841, '差旅类', 'travel', 100, '差旅类', 0, '2020-09-22 21:41:40', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for flw_channel_definition
-- ----------------------------
DROP TABLE IF EXISTS `flw_channel_definition`;
CREATE TABLE `flw_channel_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_CHANNEL_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_draft
-- ----------------------------
DROP TABLE IF EXISTS `flw_draft`;
CREATE TABLE `flw_draft`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `process_definition_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程定义id',
  `form_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单布局数据',
  `form_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单填写数据',
  `process_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程名称',
  `category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类编码',
  `category_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '申请草稿表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_ev_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `flw_ev_databasechangelog`;
CREATE TABLE `flw_ev_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ev_databasechangelog
-- ----------------------------
INSERT INTO `flw_ev_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml', '2021-12-21 22:19:00', 1, 'EXECUTED', '8:1b0c48c9cf7945be799d868a2626d687', 'createTable tableName=FLW_EVENT_DEPLOYMENT; createTable tableName=FLW_EVENT_RESOURCE; createTable tableName=FLW_EVENT_DEFINITION; createIndex indexName=ACT_IDX_EVENT_DEF_UNIQ, tableName=FLW_EVENT_DEFINITION; createTable tableName=FLW_CHANNEL_DEFIN...', '', NULL, '3.8.9', NULL, NULL, '0096340917');

-- ----------------------------
-- Table structure for flw_ev_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `flw_ev_databasechangeloglock`;
CREATE TABLE `flw_ev_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ev_databasechangeloglock
-- ----------------------------
INSERT INTO `flw_ev_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for flw_event
-- ----------------------------
DROP TABLE IF EXISTS `flw_event`;
CREATE TABLE `flw_event`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `process_definition_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程定义id',
  `act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动节点id',
  `act_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动节点名称',
  `node_type` tinyint(4) NOT NULL COMMENT '事件节点类型（字典 1全局 2节点）',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型（字典 见事件类型字典）',
  `script` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '脚本',
  `exec_sort` int(11) NOT NULL COMMENT '执行顺序',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程事件配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_event_definition
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_definition`;
CREATE TABLE `flw_event_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_EVENT_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_event_deployment
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_deployment`;
CREATE TABLE `flw_event_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_event_resource
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_resource`;
CREATE TABLE `flw_event_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_form
-- ----------------------------
DROP TABLE IF EXISTS `flw_form`;
CREATE TABLE `flw_form`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `form_id` bigint(20) NOT NULL COMMENT '表单id',
  `process_definition_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程定义id',
  `act_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动节点id',
  `act_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动节点名称',
  `node_type` tinyint(4) NOT NULL COMMENT '表单节点类型（字典 1全局 2节点）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程表单配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_form_resource
-- ----------------------------
DROP TABLE IF EXISTS `flw_form_resource`;
CREATE TABLE `flw_form_resource`  (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单编码',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单名称',
  `type` tinyint(4) NULL DEFAULT 2 COMMENT '表单类型（字典 1自行开发 2在线设计）',
  `category` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单分类',
  `form_json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'PC端表单数据，适用于在线设计表单',
  `form_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PC端表单URL。适用于自行开发的表单',
  `app_form_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '移动端表单URL',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程脚本表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_form_resource
-- ----------------------------
INSERT INTO `flw_form_resource` VALUES (1294922365303664641, 'leaveFormWrite', '请假填写单', 2, 'kaoqin', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":2,\"rowspan\":2,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">请假申请单</p>\"},\"key\":\"html_1597568556394\"}]}]},{\"tds\":[]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597680070926\"},{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"姓名\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"name\",\"key\":\"input_1597568614214\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"deptName\",\"key\":\"input_1597568763728\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"请假时间\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":true,\"showTime\":false,\"disabled\":false,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"time\",\"key\":\"date_1597568665026\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"radio\",\"label\":\"请假类型\",\"icon\":\"icon-danxuan-cuxiantiao\",\"options\":{\"disabled\":false,\"hidden\":false,\"defaultValue\":\"1\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"事假\"},{\"value\":\"2\",\"label\":\"病假\"},{\"value\":\"3\",\"label\":\"婚假\"},{\"value\":\"4\",\"label\":\"丧假\"},{\"value\":\"5\",\"label\":\"产假\"},{\"value\":\"6\",\"label\":\"陪产假\"}]},\"model\":\"type\",\"key\":\"radio_1597569035169\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"textarea\",\"label\":\"备注\",\"icon\":\"icon-edit\",\"options\":{\"width\":\"100%\",\"minRows\":4,\"maxRows\":6,\"maxLength\":null,\"defaultValue\":\"\",\"clearable\":false,\"hidden\":false,\"disabled\":false,\"placeholder\":\"请输入\"},\"model\":\"remark\",\"key\":\"textarea_1597568894122\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597568752172\"}],\"config\":{\"layout\":\"vertical\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '请假填写单', 0, '2020-08-16 17:02:05', 1265476890672672808, '2020-08-18 09:42:32', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1294922455393120258, 'leaveFormRead', '请假只读单', 2, 'kaoqin', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":2,\"rowspan\":2,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">请假申请单</p>\"},\"key\":\"html_1597568556394\"}]}]},{\"tds\":[]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597680134338\"},{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"姓名\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"name\",\"key\":\"input_1597568614214\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"deptName\",\"key\":\"input_1597568763728\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"请假时间\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":true,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"time\",\"key\":\"date_1597568665026\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"radio\",\"label\":\"请假类型\",\"icon\":\"icon-danxuan-cuxiantiao\",\"options\":{\"disabled\":true,\"hidden\":false,\"defaultValue\":\"\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"事假\"},{\"value\":\"2\",\"label\":\"病假\"},{\"value\":\"3\",\"label\":\"婚假\"},{\"value\":\"4\",\"label\":\"丧假\"},{\"value\":\"5\",\"label\":\"产假\"},{\"value\":\"6\",\"label\":\"陪产假\"}]},\"model\":\"type\",\"key\":\"radio_1597569102078\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"textarea\",\"label\":\"备注\",\"icon\":\"icon-edit\",\"options\":{\"width\":\"100%\",\"minRows\":4,\"maxRows\":6,\"maxLength\":null,\"defaultValue\":\"\",\"clearable\":false,\"hidden\":false,\"disabled\":true,\"placeholder\":\"请输入\"},\"model\":\"remark\",\"key\":\"textarea_1597568894122\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597568752172\"}],\"config\":{\"layout\":\"vertical\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '请假只读单', 0, '2020-08-16 17:02:27', 1265476890672672808, '2020-08-18 10:41:24', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1295542814805032961, 'formalFormWrite', '转正填写单', 2, 'xingzheng', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"icon\":\"icon-ai-code\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">转正申请单</p>\"},\"key\":\"html_1597716603199\"}]}]},{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"基础信息\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716627935\"}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"申请人\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"applyUserName\",\"key\":\"input_1597716641999\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"申请日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":false,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"appyDate\",\"key\":\"date_1597716651343\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"applyDept\",\"key\":\"input_1597716702769\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"职位\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"applyPosition\",\"key\":\"input_1597716704432\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"入职日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":false,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"entryDate\",\"key\":\"date_1597716762482\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"原定转正日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":false,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"formalDate\",\"key\":\"date_1597716764298\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"主管意见\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716917249\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评价因素\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716936582\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评价要点\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716938115\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评分\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716939964\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":4,\"list\":[{\"type\":\"text\",\"label\":\"勤务态度\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717056583\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"A.严格遵守工作制度，有效利用工作时间\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717074899\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreA1\",\"key\":\"select_1597717139965\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"B.对新工作持积极态度\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717087514\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreB1\",\"key\":\"select_1597717207246\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"C.忠于职守、坚守岗位\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717092168\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreC1\",\"key\":\"select_1597717210999\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"D.以协作精神工作，协助上级，配合同事\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717114348\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreD1\",\"key\":\"select_1597717214390\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":4,\"list\":[{\"type\":\"text\",\"label\":\"工作效率\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718178533\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"A.工作速度快，不误工期\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718183341\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreA2\",\"key\":\"select_1597718199894\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"B.业务处置得当，经常保持良好成绩\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718189309\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreB2\",\"key\":\"select_1597718202917\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"C.工作方法合理，时间和经费的使用十分有效\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718192222\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreC2\",\"key\":\"select_1597718205853\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"D.工作中没有半途而废，不了了之和造成后遗症的现象\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718195214\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreD2\",\"key\":\"select_1597718208718\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597716599116\"}],\"config\":{\"layout\":\"vertical\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '转正填写单', 0, '2020-08-18 10:07:32', 1265476890672672808, '2020-08-23 10:06:06', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1295542814805032962, 'formalFormWriteWithAssess', '转正填写单_主管填写', 2, 'xingzheng', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"icon\":\"icon-ai-code\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">转正申请单</p>\"},\"key\":\"html_1597716603199\"}]}]},{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"基础信息\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716627935\"}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"申请人\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"applyUserName\",\"key\":\"input_1597716641999\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"申请日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"appyDate\",\"key\":\"date_1597716651343\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"applyDept\",\"key\":\"input_1597716702769\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"职位\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"applyPosition\",\"key\":\"input_1597716704432\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"入职日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"entryDate\",\"key\":\"date_1597716762482\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"原定转正日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"formalDate\",\"key\":\"date_1597716764298\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"主管意见\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716917249\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评价因素\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716936582\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评价要点\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716938115\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评分\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716939964\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":4,\"list\":[{\"type\":\"text\",\"label\":\"勤务态度\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717056583\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"A.严格遵守工作制度，有效利用工作时间\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717074899\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreA1\",\"key\":\"select_1597717139965\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"B.对新工作持积极态度\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717087514\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreB1\",\"key\":\"select_1597717207246\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"C.忠于职守、坚守岗位\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717092168\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreC1\",\"key\":\"select_1597717210999\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"D.以协作精神工作，协助上级，配合同事\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717114348\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreD1\",\"key\":\"select_1597717214390\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":4,\"list\":[{\"type\":\"text\",\"label\":\"工作效率\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718178533\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"A.工作速度快，不误工期\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718183341\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreA2\",\"key\":\"select_1597718199894\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"B.业务处置得当，经常保持良好成绩\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718189309\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreB2\",\"key\":\"select_1597718202917\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"C.工作方法合理，时间和经费的使用十分有效\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718192222\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreC2\",\"key\":\"select_1597718205853\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"D.工作中没有半途而废，不了了之和造成后遗症的现象\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718195214\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreD2\",\"key\":\"select_1597718208718\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597716599116\"}],\"config\":{\"layout\":\"vertical\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '转正填写单_主管填写', 0, '2020-08-18 10:07:32', 1265476890672672808, '2020-08-24 09:35:18', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1295542994690342913, 'formalFormRead', '转正只读单', 2, 'xingzheng', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"icon\":\"icon-ai-code\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">转正申请单</p>\"},\"key\":\"html_1597716603199\"}]}]},{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"基础信息\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716627935\"}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"申请人\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"applyUserName\",\"key\":\"input_1597716641999\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"申请日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"appyDate\",\"key\":\"date_1597716651343\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"applyDept\",\"key\":\"input_1597716702769\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"职位\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"applyPosition\",\"key\":\"input_1597716704432\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"入职日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"entryDate\",\"key\":\"date_1597716762482\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"原定转正日期\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":false,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"formalDate\",\"key\":\"date_1597716764298\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":4,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"主管意见\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716917249\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评价因素\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716936582\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评价要点\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716938115\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"评分\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597716939964\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":4,\"list\":[{\"type\":\"text\",\"label\":\"勤务态度\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717056583\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"A.严格遵守工作制度，有效利用工作时间\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717074899\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreA1\",\"key\":\"select_1597717139965\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"B.对新工作持积极态度\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717087514\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreB1\",\"key\":\"select_1597717207246\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"C.忠于职守、坚守岗位\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717092168\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreC1\",\"key\":\"select_1597717210999\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"D.以协作精神工作，协助上级，配合同事\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597717114348\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreD1\",\"key\":\"select_1597717214390\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":4,\"list\":[{\"type\":\"text\",\"label\":\"工作效率\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718178533\"}]},{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"A.工作速度快，不误工期\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718183341\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreA2\",\"key\":\"select_1597718199894\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"B.业务处置得当，经常保持良好成绩\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718189309\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreB2\",\"key\":\"select_1597718202917\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"C.工作方法合理，时间和经费的使用十分有效\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718192222\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreC2\",\"key\":\"select_1597718205853\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"text\",\"label\":\"D.工作中没有半途而废，不了了之和造成后遗症的现象\",\"icon\":\"icon-zihao\",\"options\":{\"textAlign\":\"left\",\"hidden\":false,\"showRequiredMark\":false},\"key\":\"text_1597718195214\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"select\",\"label\":\"\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":true,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"1\"},{\"value\":\"2\",\"label\":\"2\"},{\"value\":\"3\",\"label\":\"3\"},{\"value\":\"4\",\"label\":\"4\"},{\"value\":\"5\",\"label\":\"5\"}],\"showSearch\":false},\"model\":\"scoreD2\",\"key\":\"select_1597718208718\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1597716599116\"}],\"config\":{\"layout\":\"vertical\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '转正只读单', 0, '2020-08-18 10:08:15', 1265476890672672808, '2020-08-24 09:35:51', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1304313274803888130, 'travelWrite', '出差填写单', 2, 'travel', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"icon\":\"icon-ai-code\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">出差申请单</p>\"},\"key\":\"html_1599807751285\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"姓名\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"name\",\"key\":\"input_1599807772599\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"dept\",\"key\":\"input_1599807774961\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"出差时间\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":true,\"showTime\":false,\"disabled\":false,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"travelDate\",\"key\":\"date_1599807843566\",\"rules\":[{\"required\":true,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"batch\",\"label\":\"交通费\",\"icon\":\"icon-biaoge\",\"list\":[{\"type\":\"input\",\"label\":\"出发地\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"startPlace\",\"key\":\"input_1599807902416\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]},{\"type\":\"input\",\"label\":\"到达地\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"endPlace\",\"key\":\"input_1599807905299\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]},{\"type\":\"select\",\"label\":\"交通工具\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"飞机\",\"label\":\"飞机\"},{\"value\":\"火车\",\"label\":\"火车\"},{\"value\":\"汽车\",\"label\":\"汽车\"}],\"showSearch\":false},\"model\":\"traffic\",\"key\":\"select_1599807912449\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]},{\"type\":\"number\",\"label\":\"金额\",\"icon\":\"icon-number\",\"options\":{\"width\":\"100%\",\"defaultValue\":0,\"min\":null,\"max\":null,\"precision\":null,\"step\":1,\"hidden\":false,\"disabled\":false,\"placeholder\":\"请输入\"},\"model\":\"money\",\"key\":\"number_1599807922017\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}],\"options\":{\"scrollY\":0,\"disabled\":false,\"hidden\":false,\"showLabel\":false,\"hideSequence\":false,\"width\":\"100%\"},\"model\":\"trafficList\",\"key\":\"batch_1599807855999\"}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1599807746899\"}],\"config\":{\"layout\":\"horizontal\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '出差填写单', 0, '2020-09-11 14:58:12', 1265476890672672808, '2020-09-11 15:14:56', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1304317596870582274, 'travelRead', '出差只读单', 2, 'travel', '{\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"html\",\"label\":\"HTML\",\"icon\":\"icon-ai-code\",\"options\":{\"hidden\":false,\"defaultValue\":\"<p style=\\\"text-align:center;font-size:32px;font-weight:bolder;margin-bottom:0px\\\">出差申请单</p>\"},\"key\":\"html_1599807751285\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"姓名\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"name\",\"key\":\"input_1599807772599\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"部门\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":true},\"model\":\"dept\",\"key\":\"input_1599807774961\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"date\",\"label\":\"出差时间\",\"icon\":\"icon-calendar\",\"options\":{\"width\":\"100%\",\"defaultValue\":\"\",\"rangeDefaultValue\":[],\"range\":true,\"showTime\":false,\"disabled\":true,\"hidden\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"rangePlaceholder\":[\"开始时间\",\"结束时间\"],\"format\":\"YYYY-MM-DD\"},\"model\":\"travelDate\",\"key\":\"date_1599807843566\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}]}]},{\"tds\":[{\"colspan\":2,\"rowspan\":1,\"list\":[{\"type\":\"batch\",\"label\":\"交通费\",\"icon\":\"icon-biaoge\",\"list\":[{\"type\":\"input\",\"label\":\"出发地\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"startPlace\",\"key\":\"input_1599807902416\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]},{\"type\":\"input\",\"label\":\"到达地\",\"icon\":\"icon-write\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":null,\"hidden\":false,\"disabled\":false},\"model\":\"endPlace\",\"key\":\"input_1599807905299\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]},{\"type\":\"select\",\"label\":\"交通工具\",\"icon\":\"icon-xiala\",\"options\":{\"width\":\"100%\",\"multiple\":false,\"disabled\":false,\"clearable\":false,\"hidden\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"飞机\",\"label\":\"飞机\"},{\"value\":\"火车\",\"label\":\"火车\"},{\"value\":\"汽车\",\"label\":\"汽车\"}],\"showSearch\":false},\"model\":\"traffic\",\"key\":\"select_1599807912449\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]},{\"type\":\"number\",\"label\":\"金额\",\"icon\":\"icon-number\",\"options\":{\"width\":\"100%\",\"defaultValue\":0,\"min\":null,\"max\":null,\"precision\":null,\"step\":1,\"hidden\":false,\"disabled\":false,\"placeholder\":\"请输入\"},\"model\":\"money\",\"key\":\"number_1599807922017\",\"rules\":[{\"required\":false,\"message\":\"必填项\"}]}],\"options\":{\"scrollY\":0,\"disabled\":true,\"hidden\":false,\"showLabel\":false,\"hideSequence\":false,\"width\":\"100%\"},\"model\":\"trafficList\",\"key\":\"batch_1599807855999\"}]}]}],\"options\":{\"width\":\"100%\",\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1599807746899\"}],\"config\":{\"layout\":\"horizontal\",\"labelCol\":{\"span\":4},\"wrapperCol\":{\"span\":18},\"hideRequiredMark\":false,\"customStyle\":\"\"}}', NULL, NULL, '出差只读单', 0, '2020-09-11 15:15:23', 1265476890672672808, '2020-09-11 15:16:17', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1354453645117394946, 'tests_codes', '用车申请单', 1, 'approval', NULL, 'carApplyForm', NULL, '用车申请单', 0, '2021-01-27 23:38:08', 1265476890672672808, '2021-01-28 00:36:55', 1265476890672672808);
INSERT INTO `flw_form_resource` VALUES (1354468627381014530, 'tests_codes_readonly', '用车审批单', 1, 'approval', NULL, 'carApplyFormReadOnly', NULL, '用车审批单', 0, '2021-01-28 00:37:40', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for flw_option
-- ----------------------------
DROP TABLE IF EXISTS `flw_option`;
CREATE TABLE `flw_option`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `process_definition_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程定义id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题规则',
  `jump_first` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自动完成第一个任务（Y-是，N-否）',
  `smart_complete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '跳过相同处理人（Y-是，N-否）',
  `version` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程选项配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_ru_batch
-- ----------------------------
DROP TABLE IF EXISTS `flw_ru_batch`;
CREATE TABLE `flw_ru_batch`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) NULL DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BATCH_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_ru_batch_part
-- ----------------------------
DROP TABLE IF EXISTS `flw_ru_batch_part`;
CREATE TABLE `flw_ru_batch_part`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `BATCH_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) NULL DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESULT_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `FLW_IDX_BATCH_PART`(`BATCH_ID_`) USING BTREE,
  CONSTRAINT `FLW_FK_BATCH_PART_PARENT` FOREIGN KEY (`BATCH_ID_`) REFERENCES `flw_ru_batch` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_script
-- ----------------------------
DROP TABLE IF EXISTS `flw_script`;
CREATE TABLE `flw_script`  (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '脚本名称',
  `type` tinyint(4) NOT NULL COMMENT '脚本类别（字典 1流程脚本 2系统脚本）',
  `lang` tinyint(4) NOT NULL COMMENT '脚本语言（字典1 groovy)',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '脚本内容',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程脚本表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_script
-- ----------------------------
INSERT INTO `flw_script` VALUES (1294898800718163970, '打印内容', 1, 1, 'System.out.println(\"这是打印的内容\");', '打印内容', 0, '2020-08-16 15:28:27', 1265476890672672808, '2020-08-16 18:23:26', 1265476890672672808);
INSERT INTO `flw_script` VALUES (1304322960072609794, '设置会签人员', 1, 1, 'import cn.hutool.core.collection.CollectionUtil;\nimport vip.xiaonuo.flowable.core.utils.BpmScriptUtil;\n\nList<Long> userIdList = CollectionUtil.newArrayList();\nuserIdList.add(1275735541155614721);\nuserIdList.add(1280700700074041345);\nuserIdList.add(1280709549107552257);\n\n//调用脚本工具类，给执行实例设置会签人员\nBpmScriptUtil.setVariableForInstance(execution, \"pers\", userIdList);\n//调用脚本工具类，给执行实例设置多实例基数为会签人员数量\nBpmScriptUtil.setVariableForInstance(execution, \"num\", 3);', '流程启动时给流程实例设置会签人员', 0, '2020-09-11 15:36:41', 1265476890672672808, '2020-09-11 16:55:02', 1265476890672672808);
INSERT INTO `flw_script` VALUES (1345925988436881410, '获取表单数据', 1, 1, 'import vip.xiaonuo.flowable.core.utils.BpmScriptUtil;\n//此处仅打印表单数据，请参见后台启动日志，执行此脚本会将表单数据获取并打印出来\nBpmScriptUtil.getFormData(execution);', '获取表单数据', 0, '2021-01-04 10:52:16', 1265476890672672808, '2021-01-11 17:52:16', 1265476890672672808);

-- ----------------------------
-- Table structure for flw_shortcut
-- ----------------------------
DROP TABLE IF EXISTS `flw_shortcut`;
CREATE TABLE `flw_shortcut`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `process_definition_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '流程定义id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类',
  `category_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `version` tinyint(4) NOT NULL COMMENT '定义版本',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图标',
  `sort` int(11) NOT NULL COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程申请入口表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pay_ali_trade_history
-- ----------------------------
DROP TABLE IF EXISTS `pay_ali_trade_history`;
CREATE TABLE `pay_ali_trade_history`  (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `out_trade_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商家订单号',
  `trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付宝订单号',
  `bill_date` datetime(0) NOT NULL COMMENT '交易时间',
  `amount` decimal(10, 2) NOT NULL COMMENT '交易金额',
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名称',
  `body` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品描述',
  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0未支付 1已支付 2已退款 3已关闭 4已关闭有退款）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_bom
-- ----------------------------
DROP TABLE IF EXISTS `dw_bom`;
CREATE TABLE `dw_bom`  (
                           `id` bigint(20) NOT NULL COMMENT '主键id',
                           `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父项产品id',
                           `subitem_id` bigint(20) NULL DEFAULT NULL COMMENT '子项产品id',
                           `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                           `unit_con` bigint(255) NULL DEFAULT NULL COMMENT '单位用量',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_german2_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_cus_infor
-- ----------------------------
DROP TABLE IF EXISTS `dw_cus_infor`;
CREATE TABLE `dw_cus_infor`  (
                                 `id` bigint(20) NOT NULL COMMENT '主键id',
                                 `cus_infor_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户资料名称',
                                 `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '社会统一信用代码',
                                 `cus_sort_id` bigint(20) NULL DEFAULT NULL COMMENT '客户分类id',
                                 `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户资料' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_cus_person
-- ----------------------------
DROP TABLE IF EXISTS `dw_cus_person`;
CREATE TABLE `dw_cus_person`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `cus_infor_id` bigint(20) NULL DEFAULT NULL COMMENT '客户资料id',
                                  `cus_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
                                  `cus_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
                                  `maior_cus_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主联系人',
                                  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户联系人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_cus_sort
-- ----------------------------
DROP TABLE IF EXISTS `dw_cus_sort`;
CREATE TABLE `dw_cus_sort`  (
                                `id` bigint(20) NOT NULL COMMENT '主键id',
                                `pid` bigint(20) NULL DEFAULT NULL COMMENT '父类id',
                                `pids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类id集合',
                                `sort_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户分类名称',
                                `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '客户分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_custom_code
-- ----------------------------
DROP TABLE IF EXISTS `dw_custom_code`;
CREATE TABLE `dw_custom_code`  (
                                   `encode` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义编码',
                                   `time_format` int(1) NOT NULL COMMENT '时间选项',
                                   `serial_num` int(1) NOT NULL COMMENT '流水号位数',
                                   `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '目标表单名称',
                                   `id` bigint(20) NOT NULL COMMENT '主键id',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                   `table_re_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名称',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '编号配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_field_config
-- ----------------------------
DROP TABLE IF EXISTS `dw_field_config`;
CREATE TABLE `dw_field_config`  (
                                    `id` bigint(20) NOT NULL COMMENT '主键',
                                    `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名称',
                                    `field_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段索引',
                                    `field_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名称',
                                    `field_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段类型',
                                    `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                    `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
                                    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                    `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字段配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_inv
-- ----------------------------
DROP TABLE IF EXISTS `dw_inv`;
CREATE TABLE `dw_inv`  (
                           `id` bigint(20) NOT NULL COMMENT '主键id',
                           `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号，唯一性校验',
                           `category` int(11) NOT NULL COMMENT '-1为出库，1为入库',
                           `out_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出库类型',
                           `in_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入库类型',
                           `time` datetime(0) NOT NULL COMMENT '出入库时间',
                           `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `war_hou_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                           PRIMARY KEY (`id`) USING BTREE,
                           INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出入库表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_inv_detail
-- ----------------------------
DROP TABLE IF EXISTS `dw_inv_detail`;
CREATE TABLE `dw_inv_detail`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `inv_id` bigint(20) NOT NULL COMMENT '库单ID',
                                  `pro_id` bigint(20) NOT NULL COMMENT '产品ID',
                                  `war_hou_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
                                  `num` bigint(20) NOT NULL COMMENT '数量',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE,
                                  INDEX `inv_id_code`(`inv_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出入库明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_plan_rel
-- ----------------------------
DROP TABLE IF EXISTS `dw_plan_rel`;
CREATE TABLE `dw_plan_rel`  (
                                `id` bigint(20) NOT NULL COMMENT '主键id',
                                `pro_plan_id` bigint(20) NOT NULL COMMENT '生产计划id',
                                `pro_id` bigint(20) NOT NULL COMMENT '产品id',
                                `work_order_id` bigint(20) NOT NULL COMMENT '工单id',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '生产计划关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_pro
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro`;
CREATE TABLE `dw_pro`  (
                           `id` bigint(20) NOT NULL COMMENT '主键',
                           `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称',
                           `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品编码',
                           `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型',
                           `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                           `unit` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '库存位置',
                           `josn` json NULL COMMENT '配置字段值',
                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                           `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                           `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                           `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                           `work_route_id` bigint(20) NULL DEFAULT NULL COMMENT '工艺路线id',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_pro_model
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro_model`;
CREATE TABLE `dw_pro_model`  (
                                 `id` bigint(20) NOT NULL COMMENT '产品型号主键',
                                 `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品型号名称',
                                 `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品型号编码',
                                 `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品ID',
                                 `unit` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库存单位',
                                 `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型ID',
                                 `remarks` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品型号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_pro_plan
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro_plan`;
CREATE TABLE `dw_pro_plan`  (
                                `id` bigint(20) NOT NULL COMMENT '主键id',
                                `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号，唯一性校验',
                                `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '生产计划' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_pro_type
-- ----------------------------
DROP TABLE IF EXISTS `dw_pro_type`;
CREATE TABLE `dw_pro_type`  (
                                `id` bigint(20) NOT NULL COMMENT '主键',
                                `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
                                `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码',
                                `pid` bigint(20) NULL DEFAULT NULL COMMENT '父级',
                                `pids` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所有父级',
                                `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                `sort` int(11) NULL DEFAULT NULL COMMENT '排序号',
                                `josn` json NULL COMMENT '配置字段值',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_puor_detail
-- ----------------------------
DROP TABLE IF EXISTS `dw_puor_detail`;
CREATE TABLE `dw_puor_detail`  (
                                   `id` bigint(20) NOT NULL COMMENT '主键',
                                   `purchase_order_id` bigint(20) NOT NULL COMMENT '采购订单id',
                                   `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购明细编号',
                                   `pro_id` bigint(20) NOT NULL COMMENT '产品id',
                                   `purchase_num` int(11) NOT NULL COMMENT '数量',
                                   `unit_price` double(10, 2) NOT NULL COMMENT '单价',
  `total_price` double(10, 2) NULL DEFAULT NULL COMMENT '总价',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购细明' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_puor_order
-- ----------------------------
DROP TABLE IF EXISTS `dw_puor_order`;
CREATE TABLE `dw_puor_order`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '采购订单编码',
                                  `arrival_time` datetime(0) NOT NULL COMMENT '到货时间',
                                  `order_source` bigint(20) NOT NULL COMMENT '订单来源',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_sale_order
-- ----------------------------
DROP TABLE IF EXISTS `dw_sale_order`;
CREATE TABLE `dw_sale_order`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单编号',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '销售订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_stock_balance
-- ----------------------------
DROP TABLE IF EXISTS `dw_stock_balance`;
CREATE TABLE `dw_stock_balance`  (
                                     `id` bigint(20) NOT NULL COMMENT '主键id',
                                     `war_hou_id` bigint(20) NULL DEFAULT NULL COMMENT '仓库id',
                                     `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品id',
                                     `sto_num` bigint(20) NULL DEFAULT NULL COMMENT '库存数量',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                     `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                     `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                     `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '库存余额' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_supp_data
-- ----------------------------
DROP TABLE IF EXISTS `dw_supp_data`;
CREATE TABLE `dw_supp_data`  (
                                 `id` bigint(20) NOT NULL COMMENT '主键id',
                                 `supp_data_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商资料名称',
                                 `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '社会统一信用代码',
                                 `supp_sort_id` bigint(20) NULL DEFAULT NULL COMMENT '供应商分类id',
                                 `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = ' 供应商资料' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_supp_person
-- ----------------------------
DROP TABLE IF EXISTS `dw_supp_person`;
CREATE TABLE `dw_supp_person`  (
                                   `id` bigint(20) NOT NULL COMMENT '主键id',
                                   `supp_data_id` bigint(20) NULL DEFAULT NULL COMMENT '供应商资料id',
                                   `supp_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
                                   `supp_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人电话',
                                   `chief_supp_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主联系人',
                                   `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商联系人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_supp_sort
-- ----------------------------
DROP TABLE IF EXISTS `dw_supp_sort`;
CREATE TABLE `dw_supp_sort`  (
                                 `id` bigint(20) NOT NULL COMMENT '主键id',
                                 `pid` bigint(20) NULL DEFAULT NULL COMMENT '父类id',
                                 `pids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类id集合',
                                 `sort_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商分类名称',
                                 `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_task
-- ----------------------------
DROP TABLE IF EXISTS `dw_task`;
CREATE TABLE `dw_task`  (
                            `id` bigint(20) NOT NULL COMMENT 'id',
                            `sort_num` int(2) NULL DEFAULT NULL COMMENT '序号',
                            `code` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
                            `work_order_id` bigint(20) NULL DEFAULT NULL COMMENT '工单id',
                            `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品id',
                            `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型id',
                            `work_step_id` bigint(20) NULL DEFAULT NULL COMMENT '工序id',
                            `good_num` int(20) NULL DEFAULT NULL COMMENT '良品数',
                            `bad_num` int(20) NULL DEFAULT NULL COMMENT '不良品数',
                            `pla_num` int(20) NULL DEFAULT NULL COMMENT '计划数',
                            `pla_start_time` datetime(0) NULL DEFAULT NULL COMMENT '计划开始时间',
                            `pla_end_time` datetime(0) NULL DEFAULT NULL COMMENT '计划结束时间',
                            `fact_sta_time` datetime(0) NULL DEFAULT NULL COMMENT '实际开始时间',
                            `fact_end_time` datetime(0) NULL DEFAULT NULL COMMENT '实际结束时间',
                            `status` int(20) NULL DEFAULT NULL COMMENT '状态(未开始:-1;执行中:0;已结束:1;)',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL,
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                            `report_right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报工权限',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_ware_house
-- ----------------------------
DROP TABLE IF EXISTS `dw_ware_house`;
CREATE TABLE `dw_ware_house`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `pid` bigint(20) NULL DEFAULT NULL COMMENT '父类id',
                                  `pids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类id集合',
                                  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
                                  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
                                  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL,
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_work_order
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_order`;
CREATE TABLE `dw_work_order`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `type` int(2) NULL DEFAULT NULL COMMENT '工单类型：1顺序工单；2流程工单',
                                  `work_order_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工单编号',
                                  `pla_num` int(11) NULL DEFAULT NULL COMMENT '计划数',
                                  `pla_start_time` datetime(0) NULL DEFAULT NULL COMMENT '计划开始时间',
                                  `pla_end_time` datetime(0) NULL DEFAULT NULL COMMENT '计划结束时间',
                                  `fact_sta_time` datetime(0) NULL DEFAULT NULL COMMENT '实际开始时间',
                                  `fact_end_time` datetime(0) NULL DEFAULT NULL COMMENT '实际结束时间',
                                  `pro_id` bigint(20) NULL DEFAULT NULL COMMENT '产品id',
                                  `pro_model_id` bigint(20) NULL DEFAULT NULL COMMENT '产品型号id',
                                  `sale_order_id` bigint(20) NULL DEFAULT NULL COMMENT '销售订单id',
                                  `pro_type_id` bigint(20) NULL DEFAULT NULL COMMENT '产品类型id',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `status` int(11) NULL DEFAULT NULL COMMENT '状态(未开始:-1;执行中:0;已结束:1;已取消:-2)',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_work_order_task
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_order_task`;
CREATE TABLE `dw_work_order_task`  (
                                       `id` bigint(20) NOT NULL COMMENT '主键id',
                                       `work_order_id` bigint(20) NULL DEFAULT NULL COMMENT '工单id',
                                       `source_task_id` bigint(20) NULL DEFAULT NULL COMMENT '源节点任务',
                                       `target_task_id` bigint(20) NULL DEFAULT NULL COMMENT '目标节点任务',
                                       `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                       `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                       `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                       `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工单任务关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_work_report
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_report`;
CREATE TABLE `dw_work_report`  (
                                   `id` bigint(20) NOT NULL COMMENT '报工表主键Id',
                                   `work_order_id` bigint(20) NULL DEFAULT NULL COMMENT '工单id',
                                   `task_id` bigint(20) NULL DEFAULT NULL COMMENT '任务id',
                                   `production_user` bigint(20) NULL DEFAULT NULL COMMENT '生产人员',
                                   `report_num` int(20) NULL DEFAULT NULL COMMENT '报工数',
                                   `good_num` int(20) NULL DEFAULT NULL COMMENT '良品数',
                                   `bad_num` int(20) NULL DEFAULT NULL COMMENT '不良品数',
                                   `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
                                   `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
                                   `work_time` int(11) NULL DEFAULT NULL COMMENT '工作时长',
                                   `distinction` int(1) NULL DEFAULT NULL COMMENT '区分该报工从哪里添加(1为从报工添加,0为从任务添加)',
                                   `approval` int(1) NULL DEFAULT NULL COMMENT '审批(1为已审批,0为未审批)',
                                   `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '报工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_work_route
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_route`;
CREATE TABLE `dw_work_route`  (
                                  `id` bigint(20) NOT NULL COMMENT 'id',
                                  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
                                  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工艺路线名称',
                                  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工艺路线' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_work_step
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_step`;
CREATE TABLE `dw_work_step`  (
                                 `id` bigint(20) NOT NULL COMMENT 'id\r\n',
                                 `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工序编号',
                                 `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工序名称',
                                 `mul_bad_item` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '不良品项列表（多选）',
                                 `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                 `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                 `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                 `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                 `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                 `report_right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报工权限',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '工序' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dw_work_step_route
-- ----------------------------
DROP TABLE IF EXISTS `dw_work_step_route`;
CREATE TABLE `dw_work_step_route`  (
                                       `id` bigint(20) NOT NULL COMMENT '工序路线关系表主键id',
                                       `work_route_Id` bigint(20) NULL DEFAULT NULL COMMENT '工艺路线id',
                                       `work_step_Id` bigint(20) NULL DEFAULT NULL COMMENT '工序id',
                                       `sort_num` int(11) NULL DEFAULT NULL COMMENT '排序号',
                                       `source` bigint(20) NULL DEFAULT NULL COMMENT '源节点',
                                       `target` bigint(20) NULL DEFAULT NULL COMMENT '目标节点',
                                       `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                       `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                       `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                       `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工序路线关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app`  (
                            `id` bigint(20) NOT NULL COMMENT '主键id',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用名称',
                            `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                            `active` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否默认激活（Y-是，N-否）',
                            `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统应用表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_app
-- ----------------------------
INSERT INTO `sys_app` VALUES (1265476890672672821, '系统应用', 'system', 'N', 0, '2029-12-30 19:07:00', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_app` VALUES (1342445032647098369, '系统工具', 'system_tool', 'N', 0, '2030-01-01 20:20:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_app` VALUES (1527495028319117314, '生产管理', 'manufacturing', 'N', 0, '2022-05-20 11:42:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_app` VALUES (1549011192304033793, '客户关系', 'crm', 'N', 0, '2019-07-18 20:40:22', 1265476890672672808, '2022-07-18 21:32:40', 1265476890672672808);
INSERT INTO `sys_app` VALUES (1549019665616003074, '协同办公', 'teamwork', 'Y', 0, '2019-06-18 21:14:02', 1265476890672672808, '2022-07-18 21:25:01', 1265476890672672808);
INSERT INTO `sys_app` VALUES (1549021450137165826, '人力资源', 'hr', 'N', 0, '2022-07-18 21:21:08', 1265476890672672808, '2022-07-18 21:24:09', 1265476890672672808);
INSERT INTO `sys_app` VALUES (1557615934372339714, '高级体验', 'ht', 'N', 0, '2022-08-11 14:32:33', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area`  (
                             `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                             `level_code` tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '层级',
                             `parent_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父级行政代码',
                             `area_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '行政代码',
                             `zip_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '邮政编码',
                             `city_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '区号',
                             `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '名称',
                             `short_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '简称',
                             `merger_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '组合名',
                             `pinyin` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音',
                             `lng` decimal(10, 6) NULL DEFAULT NULL COMMENT '经度',
                             `lat` decimal(10, 6) NULL DEFAULT NULL COMMENT '纬度',
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `uk_code`(`area_code`) USING BTREE,
                             INDEX `idx_parent_code`(`parent_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '中国行政地区表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_area
-- ----------------------------

-- ----------------------------
-- Table structure for sys_code_generate
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate`;
CREATE TABLE `sys_code_generate`  (
                                      `id` bigint(20) NOT NULL COMMENT '主键',
                                      `author_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者姓名',
                                      `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类名',
                                      `table_prefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否移除表前缀',
                                      `generate_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成位置类型',
                                      `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据库表名',
                                      `package_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '包名称',
                                      `bus_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '业务名',
                                      `table_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '功能名',
                                      `app_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属应用',
                                      `menu_pid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单上级',
                                      `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                      `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                      `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                      `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成基础配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_code_generate_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_generate_config`;
CREATE TABLE `sys_code_generate_config`  (
                                             `id` bigint(20) NOT NULL COMMENT '主键',
                                             `code_gen_id` bigint(20) NULL DEFAULT NULL COMMENT '代码生成主表ID',
                                             `column_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库字段名',
                                             `java_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'java类字段名',
                                             `data_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物理类型',
                                             `column_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段描述',
                                             `java_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'java类型',
                                             `effect_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作用类型（字典）',
                                             `dict_type_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典code',
                                             `whether_table` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列表展示',
                                             `whether_add_update` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '增改',
                                             `whether_retract` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列表是否缩进（字典）',
                                             `whether_required` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（字典）',
                                             `query_whether` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否是查询条件',
                                             `query_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询方式',
                                             `column_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主键',
                                             `column_key_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主外键名称',
                                             `whether_common` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否是通用字段',
                                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成详细配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_code_generate_config
-- ----------------------------
INSERT INTO `sys_code_generate_config` VALUES (1527495983397613569, 1527495981841526785, 'code', 'code', 'varchar', '产品编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495984014176258, 1527495981841526785, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495984668487682, 1527495981841526785, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495985381519361, 1527495981841526785, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495986098745346, 1527495981841526785, 'name', 'name', 'varchar', '产品名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495986753056769, 1527495981841526785, 'pro_type_id', 'proTypeId', 'bigint', '产品类型', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProTypeId', 'N', '2022-05-20 11:46:37', 1265476890672672808, '2022-05-20 11:50:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495987461894145, 1527495981841526785, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remarks', 'N', '2022-05-20 11:46:38', 1265476890672672808, '2022-05-20 11:50:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495988116205570, 1527495981841526785, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 11:46:38', 1265476890672672808, '2022-05-20 11:50:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527495988757934082, 1527495981841526785, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 11:46:38', 1265476890672672808, '2022-05-20 11:50:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498140268146689, 1527498138657533954, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:36', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498140905680897, 1527498138657533954, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:36', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498141547409410, 1527498138657533954, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:36', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498142184943617, 1527498138657533954, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498142818283522, 1527498138657533954, 'name', 'name', 'varchar', '名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 11:55:11', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498143460012034, 1527498138657533954, 'pro_id', 'proId', 'bigint', '产品型号ID', 'Long', 'select', 'code_gen_query_type', 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498144097546241, 1527498138657533954, 'remarks', 'remarks', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remarks', 'N', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498144739274753, 1527498138657533954, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527498145318088706, 1527498138657533954, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 11:55:12', 1265476890672672808, '2022-05-20 14:59:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527499129060777986, 1527499124988108802, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499129757032450, 1527499124988108802, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499130465869826, 1527499124988108802, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499131170512898, 1527499124988108802, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499131891933186, 1527499124988108802, 'pro_model_id', 'proModelId', 'bigint', '产品型号id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProModelId', 'N', '2022-05-20 11:59:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499132852428801, 1527499124988108802, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 11:59:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527499133573849089, 1527499124988108802, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 11:59:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1527523767136477186, 1527523765366480897, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:39', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523767841120258, 1527523765366480897, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523768533180418, 1527523765366480897, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523769187491842, 1527523765366480897, 'pro_inst_id', 'proInstId', 'bigint', '产品实例ID', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProInstId', 'N', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523769896329218, 1527523765366480897, 'pro_step_id', 'proStepId', 'bigint', '产品步骤ID', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProStepId', 'N', '2022-05-20 13:37:01', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523770613555202, 1527523765366480897, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 13:37:02', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527523771330781186, 1527523765366480897, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 13:37:02', 1265476890672672808, '2022-05-20 13:37:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528596401373185, 1527528594438438913, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-05-20 13:56:12', 1265476890672672808, '2022-05-20 14:01:34', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528597173125121, 1527528594438438913, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 13:56:12', 1265476890672672808, '2022-05-20 14:01:34', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528598083289090, 1527528594438438913, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:34', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528598800515073, 1527528594438438913, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528599580655617, 1527528594438438913, 'name', 'name', 'varchar', '名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528600302075905, 1527528594438438913, 'pid', 'pid', 'bigint', '父级', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Pid', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528601015107585, 1527528594438438913, 'pids', 'pids', 'varchar', '所有父级', 'String', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'like', '', 'Pids', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528601795248129, 1527528594438438913, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remarks', 'N', '2022-05-20 13:56:13', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528602516668417, 1527528594438438913, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 13:56:14', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527528603233894402, 1527528594438438913, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 13:56:14', 1265476890672672808, '2022-05-20 14:01:35', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560391012384769, 1527560389183668225, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560391675084802, 1527560389183668225, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560392312619010, 1527560389183668225, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560392971124738, 1527560389183668225, 'name', 'name', 'varchar', '步骤名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560393642213377, 1527560389183668225, 'pro_model_id', 'proModelId', 'bigint', '产品型号id', 'Long', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProModelId', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560394292330497, 1527560389183668225, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Remarks', 'N', '2022-05-20 16:02:33', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560394967613442, 1527560389183668225, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-20 16:02:34', 1265476890672672808, '2022-05-20 16:08:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1527560395592564738, 1527560389183668225, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-20 16:02:34', 1265476890672672808, '2022-05-20 16:08:11', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914713331683330, 1528914711771402242, 'code', 'code', 'bigint', '不良品编号', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-05-24 09:44:08', 1265476890672672808, '2022-05-24 10:05:27', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914713939857410, 1528914711771402242, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-24 09:44:08', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914714556420098, 1528914711771402242, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-24 09:44:08', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914715164594177, 1528914711771402242, 'id', 'id', 'bigint', '不良品项主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914715776962562, 1528914711771402242, 'name', 'name', 'varchar', '不良品名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914716406108162, 1528914711771402242, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914717014282241, 1528914711771402242, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528914717630844930, 1528914711771402242, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-24 09:44:09', 1265476890672672808, '2022-05-24 10:05:28', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918378670989314, 1528918376926158850, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-05-24 09:58:42', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918379371438082, 1528918376926158850, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-05-24 09:58:42', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918380038332417, 1528918376926158850, 'id', 'id', 'bigint', 'id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-24 09:58:42', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918380709421058, 1528918376926158850, 'name', 'name', 'varchar', '产品单位名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918381405675521, 1528918376926158850, 'remarks', 'remarks', 'varchar', '产品单位备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918382047404033, 1528918376926158850, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:12', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528918382747852801, 1528918376926158850, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-05-24 09:58:43', 1265476890672672808, '2022-05-24 10:02:13', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923116980441089, 1528923115369828353, 'code', 'code', 'varchar', '工序编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923117617975298, 1528923115369828353, 'id', 'id', 'int', 'id\r\n', 'Integer', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923118528139265, 1528923115369828353, 'mul_bad_item', 'mulBadItem', 'varchar', '不良品项列表（多选）', 'String', 'checkbox', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'MulBadItem', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923119199227905, 1528923115369828353, 'name', 'name', 'varchar', '工序名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:15', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1528923119845150721, 1528923115369828353, 'remarks', 'remarks', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Remarks', 'N', '2022-05-24 10:17:32', 1265476890672672808, '2022-05-24 10:24:16', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534372927078776834, 1534372926302830594, 'id', 'id', 'bigint', '报工表主键Id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372927376572418, 1534372926302830594, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372927527567362, 1534372926302830594, 'task_id', 'taskId', 'bigint', '工序id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TaskId', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372927917637633, 1534372926302830594, 'step_status', 'stepStatus', 'int', '工序状态', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StepStatus', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372928165101569, 1534372926302830594, 'production_name', 'productionName', 'varchar', '生产人员', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProductionName', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372928462897154, 1534372926302830594, 'report_num', 'reportNum', 'bigint', '报工数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportNum', 'N', '2022-06-08 11:13:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372930614575106, 1534372926302830594, 'good_num', 'goodNum', 'bigint', '良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372930857844737, 1534372926302830594, 'bad_num', 'badNum', 'bigint', '不良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931063365633, 1534372926302830594, 'start_time', 'startTime', 'datetime', '开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StartTime', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931344384001, 1534372926302830594, 'end_time', 'endTime', 'datetime', '结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'EndTime', 'N', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931486990337, 1534372926302830594, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931759620097, 1534372926302830594, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372931919003649, 1534372926302830594, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534372932187439106, 1534372926302830594, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 11:13:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534373506890862594, 1534373506437877761, 'id', 'id', 'bigint', 'id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373506983137281, 1534373506437877761, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507293515778, 1534373506437877761, 'name', 'name', 'varchar', '工艺路线名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Name', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507616477185, 1534373506437877761, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Remarks', 'N', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507683586050, 1534373506437877761, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373507826192386, 1534373506437877761, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373508207874049, 1534373506437877761, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534373508337897474, 1534373506437877761, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 11:15:26', 1265476890672672808, '2022-06-08 11:16:08', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1534376273143435265, 1534376271981613057, 'id', 'id', 'bigint', '工序路线关系表主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376273642557442, 1534376271981613057, 'work_route_Id', 'workRouteId', 'bigint', '工艺路线id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkRouteId', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376273759997953, 1534376271981613057, 'source', 'source', 'bigint', '源节点', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Source', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274003267585, 1534376271981613057, 'target', 'target', 'bigint', '目标节点', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Target', 'N', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274187816962, 1534376271981613057, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274431086593, 1534376271981613057, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274590470145, 1534376271981613057, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534376274938597378, 1534376271981613057, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 11:26:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799176060929, 1534421798261702658, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799436107778, 1534421798261702658, 'inv_id', 'invId', 'varchar', '库单编号，外键', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', 'MUL', 'InvId', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799566131202, 1534421798261702658, 'pro_id', 'proId', 'varchar', '产品编号，外键', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421799889092609, 1534421798261702658, 'num', 'num', 'bigint', '数量', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Num', 'N', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421800153333762, 1534421798261702658, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421801541648385, 1534421798261702658, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421801889775618, 1534421798261702658, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-08 14:27:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1534421802485366786, 1534421798261702658, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-08 14:27:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1536185610979614721, 1536185610023313410, 'id', 'id', 'bigint', '报工表主键Id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-13 11:16:05', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185611801698306, 1536185610023313410, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-13 11:16:05', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185612128854018, 1536185610023313410, 'task_id', 'taskId', 'bigint', '任务id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TaskId', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185612401483777, 1536185610023313410, 'step_status', 'stepStatus', 'int', '工序状态', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StepStatus', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185612716056577, 1536185610023313410, 'production_name', 'productionName', 'varchar', '生产人员', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProductionName', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185614083399682, 1536185610023313410, 'report_num', 'reportNum', 'bigint', '报工数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportNum', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185614393778177, 1536185610023313410, 'good_num', 'goodNum', 'bigint', '良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185614741905409, 1536185610023313410, 'bad_num', 'badNum', 'bigint', '不良品数', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185615052283905, 1536185610023313410, 'start_time', 'startTime', 'datetime', '开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StartTime', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185615333302273, 1536185610023313410, 'end_time', 'endTime', 'datetime', '结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'EndTime', 'N', '2022-06-13 11:16:06', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185616419627009, 1536185610023313410, 'remarks', 'remarks', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-06-13 11:16:07', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185620983029761, 1536185610023313410, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-13 11:16:08', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185622300041217, 1536185610023313410, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-13 11:16:08', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185623403143170, 1536185610023313410, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-13 11:16:08', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1536185624711766018, 1536185610023313410, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-13 11:16:09', 1265476890672672808, '2022-06-13 11:16:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809981879037953, 1539809981665128449, 'id', 'id', 'bigint', '报工表主键Id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809981971312641, 1539809981665128449, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982055198722, 1539809981665128449, 'task_id', 'taskId', 'bigint', '任务id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TaskId', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982134890498, 1539809981665128449, 'production_user', 'productionUser', 'bigint', '生产人员', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProductionUser', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982218776577, 1539809981665128449, 'report_num', 'reportNum', 'int', '报工数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportNum', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982302662658, 1539809981665128449, 'good_num', 'goodNum', 'int', '良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982361382914, 1539809981665128449, 'bad_num', 'badNum', 'int', '不良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982415908865, 1539809981665128449, 'start_time', 'startTime', 'datetime', '开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'StartTime', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982470434817, 1539809981665128449, 'end_time', 'endTime', 'datetime', '结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'EndTime', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982520766466, 1539809981665128449, 'work_time', 'workTime', 'int', '工作时长', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkTime', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982579486721, 1539809981665128449, 'distinction', 'distinction', 'int', '区分该报工从哪里添加(1为从报工添加,0为从任务添加)', 'Integer', 'input', NULL, 'N', 'N', 'N', 'N', 'Y', 'eq', '', 'Distinction', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982646595585, 1539809981665128449, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remarks', 'N', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982696927234, 1539809981665128449, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982755647490, 1539809981665128449, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982826950657, 1539809981665128449, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539809982898253826, 1539809981665128449, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-23 11:18:03', 1265476890672672808, '2022-06-23 11:19:01', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1539852516655755265, 1539852515879809026, 'id', 'id', 'bigint', 'id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852516781584385, 1539852515879809026, 'sort_num', 'sortNum', 'int', '序号', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SortNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517310066690, 1539852515879809026, 'code', 'code', 'varchar', '编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517372981249, 1539852515879809026, 'work_order_id', 'workOrderId', 'bigint', '工单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkOrderId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517440090113, 1539852515879809026, 'pro_id', 'proId', 'bigint', '产品id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517503004674, 1539852515879809026, 'pro_type_id', 'proTypeId', 'bigint', '产品类型id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProTypeId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517565919233, 1539852515879809026, 'work_step_id', 'workStepId', 'bigint', '工序id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'WorkStepId', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517565919234, 1539852515879809026, 'good_num', 'goodNum', 'int', '良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'GoodNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517633028098, 1539852515879809026, 'bad_num', 'badNum', 'int', '不良品数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'BadNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517695942657, 1539852515879809026, 'pla_num', 'plaNum', 'int', '计划数', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PlaNum', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517695942658, 1539852515879809026, 'pla_start_time', 'plaStartTime', 'datetime', '计划开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PlaStartTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517763051522, 1539852515879809026, 'pla_end_time', 'plaEndTime', 'datetime', '计划结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PlaEndTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517825966082, 1539852515879809026, 'fact_sta_time', 'factStaTime', 'datetime', '实际开始时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'FactStaTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517825966083, 1539852515879809026, 'fact_end_time', 'factEndTime', 'datetime', '实际结束时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'FactEndTime', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517893074946, 1539852515879809026, 'status', 'status', 'int', '状态(未开始:-1;执行中:0;已结束:1;)', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Status', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517955989505, 1539852515879809026, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852517955989506, 1539852515879809026, 'create_user', 'createUser', 'bigint', '', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852518018904066, 1539852515879809026, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852518086012930, 1539852515879809026, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1539852518148927490, 1539852515879809026, 'report_right', 'reportRight', 'bigint', '报工权限', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ReportRight', 'N', '2022-06-23 14:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1541591753163268098, 1541591752827723778, 'encode', 'encode', 'varchar', '自定义编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Encode', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753242959874, 1541591752827723778, 'time_format', 'timeFormat', 'int', '时间选项', 'Integer', 'radio', 'time_type', 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'TimeFormat', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753301680129, 1541591752827723778, 'serial_num', 'serialNum', 'int', '流水号位数', 'Integer', 'inputnumber', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'SerialNum', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753372983297, 1541591752827723778, 'table_name', 'tableName', 'varchar', '目标表单名称', 'String', 'select', 'cus_table_name', 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'TableName', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753431703554, 1541591752827723778, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753494618113, 1541591752827723778, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753553338369, 1541591752827723778, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753616252929, 1541591752827723778, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1541591753674973186, 1541591752827723778, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-06-28 09:18:10', 1265476890672672808, '2022-06-28 09:31:48', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569610708279298, 1549569608074256386, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-20 09:39:19', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569610897022977, 1549569608074256386, 'pid', 'pid', 'bigint', '父类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'Pid', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611056406530, 1549569608074256386, 'pids', 'pids', 'varchar', '父类id集合', 'String', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'Pids', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611245150210, 1549569608074256386, 'sort_name', 'sortName', 'varchar', '客户分类名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'SortName', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611442282497, 1549569608074256386, 'remark', 'remark', 'varchar', '备注信息', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611651997697, 1549569608074256386, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611786215426, 1549569608074256386, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569611916238849, 1549569608074256386, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549569612058845186, 1549569608074256386, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-20 09:39:20', 1265476890672672808, '2022-07-20 10:24:21', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586211662794753, 1549586211230781441, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-20 10:45:17', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586211755069442, 1549586211230781441, 'cus_infor_name', 'cusInforName', 'varchar', '客户资料名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'CusInforName', 'N', '2022-07-20 10:45:17', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586214217125889, 1549586211230781441, 'code', 'code', 'varchar', '社会统一信用代码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-07-20 10:45:18', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586214397480962, 1549586211230781441, 'cus_sort_id', 'cusSortId', 'bigint', '客户分类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'CusSortId', 'N', '2022-07-20 10:45:18', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586216750485506, 1549586211230781441, 'remark', 'remark', 'varchar', '备注信息', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586216851148802, 1549586211230781441, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586217035698177, 1549586211230781441, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586217203470338, 1549586211230781441, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549586217283162114, 1549586211230781441, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-20 10:45:19', 1265476890672672808, '2022-07-20 10:47:29', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605376654086146, 1549605375643258881, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:39', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605377371312129, 1549605375643258881, 'cus_infor_id', 'cusInforId', 'bigint', '客户资料id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'CusInforId', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:39', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605378172424194, 1549605375643258881, 'cus_person', 'cusPerson', 'varchar', '联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'CusPerson', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605378839318530, 1549605375643258881, 'cus_phone', 'cusPhone', 'varchar', '联系电话', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'eq', '', 'CusPhone', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605379799814145, 1549605375643258881, 'maior_cus_person', 'maiorCusPerson', 'varchar', '主联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'N', 'eq', '', 'MaiorCusPerson', 'N', '2022-07-20 12:01:27', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605380781281281, 1549605375643258881, 'remark', 'remark', 'varchar', '备注信息', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605381037133826, 1549605375643258881, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605381162962945, 1549605375643258881, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605382148624385, 1549605375643258881, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:40', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1549605383645990913, 1549605375643258881, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-20 12:01:28', 1265476890672672808, '2022-07-20 12:02:41', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192508581863426, 1552192507873026049, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192508837715970, 1552192507873026049, 'code', 'code', 'varchar', '采购订单编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192508963545089, 1552192507873026049, 'arrival_time', 'arrivalTime', 'datetime', '到货时间', 'Date', 'datepicker', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'ArrivalTime', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509219397633, 1552192507873026049, 'order_source', 'orderSource', 'bigint', '订单来源', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'OrderSource', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509483638785, 1552192507873026049, 'remarks', 'remarks', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Remarks', 'N', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509613662209, 1552192507873026049, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192509814988801, 1552192507873026049, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192510003732481, 1552192507873026049, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552192510196670465, 1552192507873026049, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-27 15:21:47', 1265476890672672808, '2022-07-27 15:23:14', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194970470539266, 1552194969950445570, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194970793500674, 1552194969950445570, 'purchase_order_id', 'purchaseOrderId', 'bigint', '采购订单编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'PurchaseOrderId', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194970931912705, 1552194969950445570, 'code', 'code', 'varchar', '采购明细编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Code', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971208736769, 1552194969950445570, 'pro_id', 'proId', 'bigint', '产品名称', 'String', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'ProId', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971401674753, 1552194969950445570, 'purchase_num', 'purchaseNum', 'int', '数量', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'PurchaseNum', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971670110209, 1552194969950445570, 'unit_price', 'unitPrice', 'double', '单价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'UnitPrice', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194971863048194, 1552194969950445570, 'total_price', 'totalPrice', 'double', '总价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'TotalPrice', 'N', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972060180481, 1552194969950445570, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972324421633, 1552194969950445570, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972517359617, 1552194969950445570, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-27 15:31:34', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552194972710297602, 1552194969950445570, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-27 15:31:35', 1265476890672672808, '2022-07-27 15:57:24', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701358616578, 1552208700809162754, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701488640001, 1552208700809162754, 'code', 'code', 'varchar', '编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'eq', '', 'Code', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701643829249, 1552208700809162754, 'name', 'name', 'varchar', '名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208701832572930, 1552208700809162754, 'remark', 'remark', 'varchar', '备注', 'String', 'textarea', NULL, 'Y', 'Y', 'N', 'N', 'Y', 'like', '', 'Remark', 'N', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702038093825, 1552208700809162754, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702247809026, 1552208700809162754, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702407192578, 1552208700809162754, 'update_time', 'updateTime', 'datetime', '修改时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552208702612713473, 1552208700809162754, 'update_user', 'updateUser', 'bigint', '修改人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-27 16:26:08', 1265476890672672808, '2022-07-27 16:28:10', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552474304292737026, 1552474302514352130, 'id', 'id', 'bigint', '主键', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-28 10:01:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474304687001601, 1552474302514352130, 'purchase_order_id', 'purchaseOrderId', 'bigint', '采购订单id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PurchaseOrderId', 'N', '2022-07-28 10:01:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474304947048450, 1552474302514352130, 'code', 'code', 'varchar', '采购明细编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305202900993, 1552474302514352130, 'pro_id', 'proId', 'bigint', '产品id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProId', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305404227586, 1552474302514352130, 'purchase_num', 'purchaseNum', 'int', '数量', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'PurchaseNum', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305664274434, 1552474302514352130, 'unit_price', 'unitPrice', 'double', '单价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'UnitPrice', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474305861406721, 1552474302514352130, 'total_price', 'totalPrice', 'double', '总价', 'Double', 'Double', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'TotalPrice', 'N', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306121453570, 1552474302514352130, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306314391553, 1552474302514352130, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306633158657, 1552474302514352130, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552474306901594114, 1552474302514352130, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-28 10:01:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1552574101191671810, 1552574100327645186, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574101447524354, 1552574100327645186, 'pro_code', 'proCode', 'varchar', '产品编号', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'ProCode', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574101694988290, 1552574100327645186, 'pro_name', 'proName', 'varchar', '产品名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'ProName', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574101921480706, 1552574100327645186, 'sto_num', 'stoNum', 'int', '库存数量', 'Integer', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'N', 'eq', '', 'StoNum', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574102219276290, 1552574100327645186, 'war_hou_name', 'warHouName', 'varchar', '仓库名称', 'String', 'select', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'WarHouName', 'N', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574102588375041, 1552574100327645186, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574102852616194, 1552574100327645186, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574103104274433, 1552574100327645186, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1552574103326572546, 1552574100327645186, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-07-28 16:38:06', 1265476890672672808, '2022-07-28 16:41:32', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1554352652243931137, 1554352651656728578, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-02 14:25:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352652441063426, 1554352651656728578, 'pid', 'pid', 'bigint', '父类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Pid', 'N', '2022-08-02 14:25:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352652634001410, 1554352651656728578, 'pids', 'pids', 'varchar', '父类id集合', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Pids', 'N', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352652826939394, 1554352651656728578, 'sort_name', 'sortName', 'varchar', '供应商分类名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SortName', 'N', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653019877377, 1554352651656728578, 'remark', 'remark', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remark', 'N', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653208621058, 1554352651656728578, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653342838786, 1554352651656728578, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653472862210, 1554352651656728578, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352653674188802, 1554352651656728578, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-02 14:25:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777196441602, 1554352776881868802, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777330659329, 1554352776881868802, 'supp_data_id', 'suppDataId', 'bigint', '供应商资料id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppDataId', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777464877057, 1554352776881868802, 'supp_person', 'suppPerson', 'varchar', '联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppPerson', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777590706177, 1554352776881868802, 'supp_phone', 'suppPhone', 'varchar', '联系人电话', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppPhone', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777724923906, 1554352776881868802, 'maior_cus_person', 'maiorCusPerson', 'varchar', '主联系人', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'MaiorCusPerson', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777787838465, 1554352776881868802, 'remark', 'remark', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remark', 'N', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352777913667586, 1554352776881868802, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352778039496706, 1554352776881868802, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352778169520130, 1554352776881868802, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352778303737857, 1554352776881868802, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-02 14:25:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829176451073, 1554352828849295362, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829302280194, 1554352828849295362, 'supp_data_name', 'suppDataName', 'varchar', '供应商资料名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppDataName', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829432303618, 1554352828849295362, 'code', 'code', 'varchar', '社会统一信用代码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829562327042, 1554352828849295362, 'supp_sort_id', 'suppSortId', 'bigint', '客户分类id', 'Long', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'SuppSortId', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829755265025, 1554352828849295362, 'remark', 'remark', 'varchar', '备注', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Remark', 'N', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352829948203009, 1554352828849295362, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352830011117570, 1554352828849295362, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352830208249857, 1554352828849295362, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1554352830275358721, 1554352828849295362, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-02 14:26:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_code_generate_config` VALUES (1557621802560151554, 1557621802300104706, 'id', 'id', 'bigint', '主键id', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', 'PRI', 'Id', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802627260417, 1557621802300104706, 'name', 'name', 'varchar', '租户名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'like', '', 'Name', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802627260418, 1557621802300104706, 'code', 'code', 'varchar', '租户的编码', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'Code', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802690174978, 1557621802300104706, 'db_name', 'dbName', 'varchar', '关联的数据库名称', 'String', 'input', NULL, 'Y', 'Y', 'N', 'Y', 'Y', 'eq', '', 'DbName', 'N', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802757283842, 1557621802300104706, 'create_time', 'createTime', 'datetime', '创建时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateTime', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802824392705, 1557621802300104706, 'create_user', 'createUser', 'bigint', '创建人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'CreateUser', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621802891501570, 1557621802300104706, 'update_time', 'updateTime', 'datetime', '更新时间', 'Date', 'datepicker', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateTime', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);
INSERT INTO `sys_code_generate_config` VALUES (1557621803025719297, 1557621802300104706, 'update_user', 'updateUser', 'bigint', '更新人', 'Long', 'input', NULL, 'N', 'N', 'N', 'N', 'N', 'eq', '', 'UpdateUser', 'Y', '2022-08-11 14:55:52', 1265476890672672808, '2022-08-11 14:58:37', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `id` bigint(20) NOT NULL COMMENT '主键',
                               `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                               `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                               `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
                               `sys_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否是系统参数（Y-是，N-否）',
                               `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                               `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                               `group_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '常量所属分类的编码，来自于“常量的分类”字典',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统参数配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1265117443880853506, 'jwt密钥', 'SNOWY_JWT_SECRET', 'snowy', 'Y', '（重要）jwt密钥，默认为空，自行设置', 0, 'DEFAULT', '2020-05-26 06:35:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853507, '默认密码', 'SNOWY_DEFAULT_PASSWORD', '123456', 'Y', '默认密码', 0, 'DEFAULT', '2020-05-26 06:37:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853508, 'token过期时间', 'SNOWY_TOKEN_EXPIRE', '86400', 'Y', 'token过期时间（单位：秒）', 0, 'DEFAULT', '2020-05-27 11:54:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853509, 'session会话过期时间', 'SNOWY_SESSION_EXPIRE', '7200', 'Y', 'session会话过期时间（单位：秒）', 0, 'DEFAULT', '2020-05-27 11:54:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1265117443880853519, '阿里云短信keyId', 'SNOWY_ALIYUN_SMS_ACCESSKEY_ID', '你的keyId', 'Y', '阿里云短信keyId', 0, 'ALIYUN_SMS', '2020-06-07 16:27:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547042242371585, '阿里云短信secret', 'SNOWY_ALIYUN_SMS_ACCESSKEY_SECRET', '你的secret', 'Y', '阿里云短信secret', 0, 'ALIYUN_SMS', '2020-06-07 16:29:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547130041737217, '阿里云短信签名', 'SNOWY_ALIYUN_SMS_SIGN_NAME', 'Snowy快速开发平台', 'Y', '阿里云短信签名', 0, 'ALIYUN_SMS', '2020-06-07 16:29:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547279530926081, '阿里云短信-登录模板号', 'SNOWY_ALIYUN_SMS_LOGIN_TEMPLATE_CODE', 'SMS_1877123456', 'Y', '阿里云短信-登录模板号', 0, 'ALIYUN_SMS', '2020-06-07 16:30:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269547410879750145, '阿里云短信默认失效时间', 'SNOWY_ALIYUN_SMS_INVALIDATE_MINUTES', '5', 'Y', '阿里云短信默认失效时间（单位：分钟）', 0, 'ALIYUN_SMS', '2020-06-07 16:31:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269575927357071361, '腾讯云短信secretId', 'SNOWY_TENCENT_SMS_SECRET_ID', '你的secretId', 'Y', '腾讯云短信secretId', 0, 'TENCENT_SMS', '2020-06-07 18:24:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269575991693500418, '腾讯云短信secretKey', 'SNOWY_TENCENT_SMS_SECRET_KEY', '你的secretkey', 'Y', '腾讯云短信secretKey', 0, 'TENCENT_SMS', '2020-06-07 18:24:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269576044084551682, '腾讯云短信sdkAppId', 'SNOWY_TENCENT_SMS_SDK_APP_ID', '1400375123', 'Y', '腾讯云短信sdkAppId', 0, 'TENCENT_SMS', '2020-06-07 18:24:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1269576089294954497, '腾讯云短信签名', 'SNOWY_TENCENT_SMS_SIGN', 'Snowy快速开发平台', 'Y', '腾讯云短信签名', 0, 'TENCENT_SMS', '2020-06-07 18:25:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378172860403713, '邮箱host', 'SNOWY_EMAIL_HOST', 'smtp.126.com', 'Y', '邮箱host', 0, 'EMAIL', '2020-06-09 23:32:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378295543795714, '邮箱用户名', 'SNOWY_EMAIL_USERNAME', 'test@126.com', 'Y', '邮箱用户名', 0, 'EMAIL', '2020-06-09 23:32:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378340510928897, '邮箱密码', 'SNOWY_EMAIL_PASSWORD', '你的邮箱密码', 'Y', '邮箱密码', 0, 'EMAIL', '2020-06-09 23:32:54', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378527358783489, '邮箱端口', 'SNOWY_EMAIL_PORT', '465', 'Y', '邮箱端口', 0, 'EMAIL', '2020-06-09 23:33:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270378790035460097, '邮箱是否开启ssl', 'SNOWY_EMAIL_SSL', 'true', 'Y', '邮箱是否开启ssl', 0, 'EMAIL', '2020-06-09 23:34:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649972737, '邮箱发件人', 'SNOWY_EMAIL_FROM', 'test@126.com', 'Y', '邮箱发件人', 0, 'EMAIL', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649972738, 'win本地上传文件路径', 'SNOWY_FILE_UPLOAD_PATH_FOR_WINDOWS', 'd:/tmp', 'Y', 'win本地上传文件路径', 0, 'FILE_PATH', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649972739, 'linux/mac本地上传文件路径', 'SNOWY_FILE_UPLOAD_PATH_FOR_LINUX', '/tmp', 'Y', 'linux/mac本地上传文件路径', 0, 'FILE_PATH', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649982740, 'Snowy演示环境', 'SNOWY_DEMO_ENV_FLAG', 'false', 'Y', 'Snowy演示环境的开关，true-打开，false-关闭，如果演示环境开启，则只能读数据不能写数据', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, '2020-09-03 14:38:17', 1265476890672672808);
INSERT INTO `sys_config` VALUES (1270380786649982741, 'Snowy放开XSS过滤的接口', 'SNOWY_UN_XSS_FILTER_URL', '/demo/xssfilter,/demo/unxss', 'Y', '多个url可以用英文逗号隔开', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649982742, '单用户登陆的开关', 'SNOWY_ENABLE_SINGLE_LOGIN', 'false', 'Y', '单用户登陆的开关，true-打开，false-关闭，如果一个人登录两次，就会将上一次登陆挤下去', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1270380786649982743, '登录验证码的开关', 'SNOWY_CAPTCHA_OPEN', 'true', 'Y', '登录验证码的开关，true-打开，false-关闭', 0, 'DEFAULT', '2020-06-09 23:42:37', 1265476890672672808, '2021-12-16 19:43:29', 1265476890672672808);
INSERT INTO `sys_config` VALUES (1280694281648070659, '阿里云定位api接口地址', 'SNOWY_IP_GEO_API', 'http://api01.aliyun.venuscn.com/ip?ip=%s', 'Y', '阿里云定位api接口地址', 0, 'DEFAULT', '2020-07-20 10:44:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1280694281648070660, '阿里云定位appCode', 'SNOWY_IP_GEO_APP_CODE', '461535aabeae4f34861884d392f5d452', 'Y', '阿里云定位appCode', 0, 'DEFAULT', '2020-07-20 10:44:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288309751255412737, 'Oauth用户登录的开关', 'SNOWY_ENABLE_OAUTH_LOGIN', 'true', 'Y', 'Oauth用户登录的开关', 0, 'OAUTH', '2020-07-29 11:05:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288310043346743297, 'Oauth码云登录ClientId', 'SNOWY_OAUTH_GITEE_CLIENT_ID', '你的clientId', 'Y', 'Oauth码云登录ClientId', 0, 'OAUTH', '2020-07-29 11:07:05', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288310157876408321, 'Oauth码云登录ClientSecret', 'SNOWY_OAUTH_GITEE_CLIENT_SECRET', '你的clientSecret', 'Y', 'Oauth码云登录ClientSecret', 0, 'OAUTH', '2020-07-29 11:07:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1288310280056483841, 'Oauth码云登录回调地址', 'SNOWY_OAUTH_GITEE_REDIRECT_URI', 'http://localhost:83/oauth/callback/gitee', 'Y', 'Oauth码云登录回调地址', 0, 'OAUTH', '2020-07-29 11:08:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1410861340761313282, '在线文档地址', 'SNOWY_ONLY_OFFICE_SERVICE_URL', 'https://xiaonuo.vip/', 'N', 'beizhu', 0, 'DEFAULT', '2021-07-02 15:22:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471107941140008961, 'token是否加解密', 'SNOWY_TOKEN_ENCRYPTION_OPEN', 'true', 'Y', 'token是否加解密', 0, 'CRYPTOGRAM', '2021-12-15 21:20:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471108086246150145, '操作日志是否加密', 'SNOWY_VISLOG_ENCRYPTION_OPEN', 'true', 'Y', '操作日志是否加密，默认开启', 0, 'CRYPTOGRAM', '2021-12-15 21:21:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471108334985154562, '登录登出日志是否加密', 'SNOWY_OPLOG_ENCRYPTION_OPEN', 'true', 'Y', '登录登出日志是否加密', 0, 'CRYPTOGRAM', '2021-12-15 21:22:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471108457198784514, '铭感字段值是否加解密', 'SNOWY_FIELD_ENC_DEC_OPEN', 'true', 'Y', '铭感字段值是否加解密，默认开启', 0, 'CRYPTOGRAM', '2021-12-15 21:22:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_config` VALUES (1471109091222482946, '是否开启租户功能', 'SNOWY_TENANT_OPEN', 'false', 'Y', '是否开启租户功能，默认关闭', 0, 'DEFAULT', '2021-12-15 21:25:14', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_database_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_database_info`;
CREATE TABLE `sys_database_info`  (
                                      `id` bigint(20) NOT NULL COMMENT '主键id',
                                      `db_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据库名称（英文名称）',
                                      `jdbc_driver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'jdbc的驱动类型',
                                      `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据库连接的账号',
                                      `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据库连接密码',
                                      `jdbc_url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'jdbc的url',
                                      `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注，摘要',
                                      `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                      PRIMARY KEY (`id`) USING BTREE,
                                      UNIQUE INDEX `NAME_UNIQUE`(`db_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据库信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_database_info
-- ----------------------------
INSERT INTO `sys_database_info` VALUES (1557925201876897794, 'master', 'com.mysql.cj.jdbc.Driver', 'root', 'yjy@123456', 'jdbc:mysql://172.16.1.35:3306/digital_workshop?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=CTT&nullCatalogMeansCurrent=true', '主数据源，项目启动数据源！', '2022-08-12 11:01:27');
INSERT INTO `sys_database_info` VALUES (1557925272743858178, 'snowy_tenant_db_default', 'com.mysql.cj.jdbc.Driver', 'root', 'yjy@123456', 'jdbc:mysql://172.16.1.35:3306/snowy_tenant_db_default?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=CTT&nullCatalogMeansCurrent=true', NULL, NULL);
INSERT INTO `sys_database_info` VALUES (1557925964795629570, 'snowy_tenant_db_768', 'com.mysql.cj.jdbc.Driver', 'root', 'yjy@123456', 'jdbc:mysql://172.16.1.35:3306/snowy_tenant_db_768?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=CTT&nullCatalogMeansCurrent=true', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `type_id` bigint(20) NOT NULL COMMENT '字典类型id',
                                  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
                                  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                                  `sort` int(11) NOT NULL COMMENT '排序',
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统字典值表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1265216536659087357, 1265216211667636234, '男', '1', 100, '男性', 0, '2020-04-01 10:23:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087358, 1265216211667636234, '女', '2', 100, '女性', 0, '2020-04-01 10:23:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087359, 1265216211667636234, '未知', '3', 100, '未知性别', 0, '2020-04-01 10:24:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087361, 1265216211667636235, '默认常量', 'DEFAULT', 100, '默认常量，都以SNOWY_开头的', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087363, 1265216211667636235, '阿里云短信', 'ALIYUN_SMS', 100, '阿里云短信配置', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087364, 1265216211667636235, '腾讯云短信', 'TENCENT_SMS', 100, '腾讯云短信', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087365, 1265216211667636235, '邮件配置', 'EMAIL', 100, '邮箱配置', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087366, 1265216211667636235, '文件上传路径', 'FILE_PATH', 100, '文件上传路径', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216536659087367, 1265216211667636235, 'Oauth配置', 'OAUTH', 100, 'Oauth配置', 0, '2020-04-14 23:25:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216617500102656, 1265216211667636226, '正常', '0', 100, '正常', 0, '2020-05-26 17:41:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216617500102657, 1265216211667636226, '停用', '1', 100, '停用', 0, '2020-05-26 17:42:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265216938389524482, 1265216211667636226, '删除', '2', 100, '删除', 0, '2020-05-26 17:43:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265217669028892673, 1265217074079453185, '否', 'N', 100, '否', 0, '2020-05-26 17:46:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265217706584690689, 1265217074079453185, '是', 'Y', 100, '是', 0, '2020-05-26 17:46:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265220776437731330, 1265217846770913282, '登录', '1', 100, '登录', 0, '2020-05-26 17:58:34', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265220806070489090, 1265217846770913282, '登出', '2', 100, '登出', 0, '2020-05-26 17:58:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265221129564573697, 1265221049302372354, '目录', '0', 100, '目录', 0, '2020-05-26 17:59:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265221163119005697, 1265221049302372354, '菜单', '1', 100, '菜单', 0, '2020-05-26 18:00:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265221188091891713, 1265221049302372354, '按钮', '2', 100, '按钮', 0, '2020-05-26 18:00:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466389204967426, 1265466149622128641, '未发送', '0', 100, '未发送', 0, '2020-05-27 10:14:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466432670539778, 1265466149622128641, '发送成功', '1', 100, '发送成功', 0, '2020-05-27 10:14:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466486097584130, 1265466149622128641, '发送失败', '2', 100, '发送失败', 0, '2020-05-27 10:14:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466530477514754, 1265466149622128641, '失效', '3', 100, '失效', 0, '2020-05-27 10:15:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466835009150978, 1265466752209395713, '无', '0', 100, '无', 0, '2020-05-27 10:16:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466874758569986, 1265466752209395713, '组件', '1', 100, '组件', 0, '2020-05-27 10:16:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466925476093953, 1265466752209395713, '内链', '2', 100, '内链', 0, '2020-05-27 10:16:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265466962209808385, 1265466752209395713, '外链', '3', 100, '外链', 0, '2020-05-27 10:16:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265467428423475202, 1265467337566461954, '系统权重', '1', 100, '系统权重', 0, '2020-05-27 10:18:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265467503090475009, 1265467337566461954, '业务权重', '2', 100, '业务权重', 0, '2020-05-27 10:18:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468138431062018, 1265468028632571905, '全部数据', '1', 100, '全部数据', 0, '2020-05-27 10:21:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468194928336897, 1265468028632571905, '本部门及以下数据', '2', 100, '本部门及以下数据', 0, '2020-05-27 10:21:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468241992622082, 1265468028632571905, '本部门数据', '3', 100, '本部门数据', 0, '2020-05-27 10:21:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468273634451457, 1265468028632571905, '仅本人数据', '4', 100, '仅本人数据', 0, '2020-05-27 10:22:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468302046666753, 1265468028632571905, '自定义数据', '5', 100, '自定义数据', 0, '2020-05-27 10:22:09', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468508100239362, 1265468437904367618, 'app', '1', 100, 'app', 0, '2020-05-27 10:22:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468543433056258, 1265468437904367618, 'pc', '2', 100, 'pc', 0, '2020-05-27 10:23:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1265468576874242050, 1265468437904367618, '其他', '3', 100, '其他', 0, '2020-05-27 10:23:15', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617233011335170, 1275617093517172738, '其它', '0', 100, '其它', 0, '2020-06-24 10:30:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617295355469826, 1275617093517172738, '增加', '1', 100, '增加', 0, '2020-06-24 10:30:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617348610547714, 1275617093517172738, '删除', '2', 100, '删除', 0, '2020-06-24 10:30:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617395515449346, 1275617093517172738, '编辑', '3', 100, '编辑', 0, '2020-06-24 10:31:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617433612312577, 1275617093517172738, '更新', '4', 100, '更新', 0, '2020-06-24 10:31:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617472707420161, 1275617093517172738, '查询', '5', 100, '查询', 0, '2020-06-24 10:31:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617502973517826, 1275617093517172738, '详情', '6', 100, '详情', 0, '2020-06-24 10:31:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617536959963137, 1275617093517172738, '树', '7', 100, '树', 0, '2020-06-24 10:31:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617619524837377, 1275617093517172738, '导入', '8', 100, '导入', 0, '2020-06-24 10:31:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617651816783873, 1275617093517172738, '导出', '9', 100, '导出', 0, '2020-06-24 10:32:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617683475390465, 1275617093517172738, '授权', '10', 100, '授权', 0, '2020-06-24 10:32:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617709928865793, 1275617093517172738, '强退', '11', 100, '强退', 0, '2020-06-24 10:32:17', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617739091861505, 1275617093517172738, '清空', '12', 100, '清空', 0, '2020-06-24 10:32:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1275617788601425921, 1275617093517172738, '修改状态', '13', 100, '修改状态', 0, '2020-06-24 10:32:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774590944317441, 1277774529430654977, '阿里云', '1', 100, '阿里云', 0, '2020-06-30 09:22:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774666055913474, 1277774529430654977, '腾讯云', '2', 100, '腾讯云', 0, '2020-06-30 09:23:15', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774695168577538, 1277774529430654977, 'minio', '3', 100, 'minio', 0, '2020-06-30 09:23:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1277774726835572737, 1277774529430654977, '本地', '4', 100, '本地', 0, '2020-06-30 09:23:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278607123583868929, 1278606951432855553, '运行', '1', 100, '运行', 0, '2020-07-02 16:31:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278607162943217666, 1278606951432855553, '停止', '2', 100, '停止', 0, '2020-07-02 16:31:18', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939265862004738, 1278911800547147777, '通知', '1', 100, '通知', 0, '2020-07-03 14:30:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939319922388994, 1278911800547147777, '公告', '2', 100, '公告', 0, '2020-07-03 14:31:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939399001796609, 1278911952657776642, '草稿', '0', 100, '草稿', 0, '2020-07-03 14:31:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939432686252034, 1278911952657776642, '发布', '1', 100, '发布', 0, '2020-07-03 14:31:37', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939458804183041, 1278911952657776642, '撤回', '2', 100, '撤回', 0, '2020-07-03 14:31:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1278939485878415362, 1278911952657776642, '删除', '3', 100, '删除', 0, '2020-07-03 14:31:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1291390260160299009, 1291390159941599233, '是', 'true', 100, '是', 2, '2020-08-06 23:06:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1291390315437031426, 1291390159941599233, '否', 'false', 100, '否', 2, '2020-08-06 23:06:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1342446007168466945, 1342445962104864770, '下载压缩包', '1', 100, '下载压缩包', 0, '2020-12-25 20:24:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1342446035433881601, 1342445962104864770, '生成到本项目', '2', 100, '生成到本项目', 0, '2020-12-25 20:24:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358094655567454210, 1358094419419750401, '输入框', 'input', 100, '输入框', 0, '2021-02-07 00:46:13', 1265476890672672808, '2021-02-08 01:01:28', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358094740510498817, 1358094419419750401, '时间选择', 'datepicker', 100, '时间选择', 0, '2021-02-07 00:46:33', 1265476890672672808, '2021-02-08 01:04:07', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358094793149014017, 1358094419419750401, '下拉框', 'select', 100, '下拉框', 0, '2021-02-07 00:46:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358095496009506817, 1358094419419750401, '单选框', 'radio', 100, '单选框', 0, '2021-02-07 00:49:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358095673084633090, 1358094419419750401, '开关', 'switch', 100, '开关', 2, '2021-02-07 00:50:15', 1265476890672672808, '2021-02-11 19:07:18', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358458689433190402, 1358457818733428737, '等于', 'eq', 1, '等于', 0, '2021-02-08 00:52:45', 1265476890672672808, '2021-02-13 23:35:36', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358458785168179202, 1358457818733428737, '模糊', 'like', 2, '模糊', 0, '2021-02-08 00:53:08', 1265476890672672808, '2021-02-13 23:35:46', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358460475682406401, 1358094419419750401, '多选框', 'checkbox', 100, '多选框', 0, '2021-02-08 00:59:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358460819019743233, 1358094419419750401, '数字输入框', 'inputnumber', 100, '数字输入框', 0, '2021-02-08 01:01:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470210267725826, 1358470065111252994, 'Long', 'Long', 100, 'Long', 0, '2021-02-08 01:38:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470239351029762, 1358470065111252994, 'String', 'String', 100, 'String', 0, '2021-02-08 01:38:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470265640927233, 1358470065111252994, 'Date', 'Date', 100, 'Date', 0, '2021-02-08 01:38:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470300168437761, 1358470065111252994, 'Integer', 'Integer', 100, 'Integer', 0, '2021-02-08 01:38:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358470697377415169, 1358470065111252994, 'boolean', 'boolean', 100, 'boolean', 0, '2021-02-08 01:40:28', 1265476890672672808, '2021-02-08 01:40:47', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358471133434036226, 1358470065111252994, 'int', 'int', 100, 'int', 0, '2021-02-08 01:42:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358471188291338241, 1358470065111252994, 'double', 'double', 100, 'double', 0, '2021-02-08 01:42:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1358756511688761346, 1358457818733428737, '大于', 'gt', 3, '大于', 0, '2021-02-08 20:36:12', 1265476890672672808, '2021-02-13 23:45:24', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756547159990274, 1358457818733428737, '小于', 'lt', 4, '大于', 0, '2021-02-08 20:36:20', 1265476890672672808, '2021-02-13 23:45:29', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756609990664193, 1358457818733428737, '不等于', 'ne', 7, '不等于', 0, '2021-02-08 20:36:35', 1265476890672672808, '2021-02-13 23:45:46', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756685030957057, 1358457818733428737, '大于等于', 'ge', 5, '大于等于', 0, '2021-02-08 20:36:53', 1265476890672672808, '2021-02-13 23:45:35', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1358756800525312001, 1358457818733428737, '小于等于', 'le', 6, '小于等于', 0, '2021-02-08 20:37:20', 1265476890672672808, '2021-02-13 23:45:40', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1360529773814083586, 1358094419419750401, '文本域', 'textarea', 100, '文本域', 0, '2021-02-13 18:02:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1360606105914732545, 1358457818733428737, '不为空', 'isNotNull', 8, '不为空', 0, '2021-02-13 23:05:49', 1265476890672672808, '2021-02-13 23:45:50', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1471107569499508737, 1265216211667636235, '加密配置', 'CRYPTOGRAM', 100, '加密配置，默认全开', 0, '2021-12-15 21:19:11', 1265476890672672808, '2021-12-15 21:19:24', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534370624561029121, 1534370298088988673, '其他出库', 'other_out', 100, '其他出库', 0, '2022-06-08 11:03:59', 1265476890672672808, '2022-06-08 11:07:00', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534370774654197762, 1534370298088988673, '采购退货出库', 'pur_out', 100, '采购退货出库', 0, '2022-06-08 11:04:35', 1265476890672672808, '2022-06-08 11:08:38', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371162073669634, 1534370298088988673, '普通出库', 'ord_out', 100, '普通出库', 0, '2022-06-08 11:06:07', 1265476890672672808, '2022-06-08 11:07:18', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371664297603073, 1534371575181225985, '字符串', 'string', 100, NULL, 0, '2022-06-08 11:08:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534371775545139202, 1534371402801537025, '模具缺失', 'Diemissing', 100, '模具缺失', 0, '2022-06-08 11:08:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534371910538829825, 1534370205881409537, '普通入库', 'ord_in', 100, '普通入库', 0, '2022-06-08 11:09:05', 1265476890672672808, '2022-06-08 11:10:17', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534371910769500162, 1534371402801537025, 'other', '其它', 100, '其他', 0, '2022-06-08 11:09:05', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534371989345607681, 1534370205881409537, '其他入库', 'other_in', 100, '其他入库', 0, '2022-06-08 11:09:24', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534372101077671938, 1534370205881409537, '采购入库', 'pur_in', 100, '采购入库', 0, '2022-06-08 11:09:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534372255730032641, 1534371402801537025, '破损', 'damaged', 100, '破损', 0, '2022-06-08 11:10:28', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534372356875673601, 1534371402801537025, '测试数据', 'ceshi1', 100, '11', 0, '2022-06-08 11:10:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1534375984302686209, 1534375649047773185, '百', 'hundred', 100, '百', 2, '2022-06-08 11:25:17', 1265476890672672808, '2022-06-22 11:02:03', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534376071464517633, 1534375649047773185, '千', 'thousand', 100, '千', 2, '2022-06-08 11:25:37', 1265476890672672808, '2022-06-22 11:02:05', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1534376187206336514, 1534375649047773185, '万', 'ten_thousand', 100, '万', 2, '2022-06-08 11:26:05', 1265476890672672808, '2022-06-22 11:02:07', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1539443353173667842, 1534375649047773185, '个', 'indivual', 100, NULL, 0, '2022-06-22 11:01:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539443446530486274, 1534375649047773185, '件', 'piece', 100, NULL, 0, '2022-06-22 11:01:34', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444094273630209, 1534375649047773185, '套', 'sleeve', 100, NULL, 0, '2022-06-22 11:04:08', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444301958787073, 1534375649047773185, '只', 'single', 100, NULL, 0, '2022-06-22 11:04:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444436134572034, 1534375649047773185, '台', 'desk', 100, NULL, 0, '2022-06-22 11:05:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444541944279042, 1534375649047773185, '米', 'meter', 100, NULL, 0, '2022-06-22 11:05:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444621967405058, 1534375649047773185, '条', 'strip', 100, NULL, 0, '2022-06-22 11:06:14', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444768386363394, 1534375649047773185, '根', 'base', 100, NULL, 0, '2022-06-22 11:06:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444884954460162, 1534375649047773185, '张', 'leaf', 100, NULL, 0, '2022-06-22 11:07:17', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539444960623898625, 1534375649047773185, '车', 'car', 100, NULL, 0, '2022-06-22 11:07:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445033567039490, 1534375649047773185, '箱', 'box', 100, NULL, 0, '2022-06-22 11:07:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445116857528321, 1534375649047773185, '立方', 'cube', 100, NULL, 0, '2022-06-22 11:08:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445206221369346, 1534375649047773185, '吨', 'ton', 100, NULL, 0, '2022-06-22 11:08:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445256955670530, 1534375649047773185, 'KG', 'kg', 100, NULL, 0, '2022-06-22 11:08:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445340317462529, 1534375649047773185, '副', 'vice', 100, NULL, 0, '2022-06-22 11:09:05', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445432814448641, 1534375649047773185, '平方', 'square', 100, NULL, 0, '2022-06-22 11:09:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1539445527370838018, 1534375649047773185, '卷', 'roll', 100, NULL, 0, '2022-06-22 11:09:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592159251587074, 1541591973620080641, '年', '0', 100, NULL, 0, '2022-06-28 09:19:47', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592211361619970, 1541591973620080641, '年月', '1', 100, NULL, 0, '2022-06-28 09:19:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592242990866434, 1541591973620080641, '年月日', '2', 100, NULL, 0, '2022-06-28 09:20:07', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541592783582126081, 1541592393646071809, '入库单', 'dw_inv', 100, NULL, 0, '2022-06-28 09:22:16', 1265476890672672808, '2022-06-28 09:26:38', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1541593787136466945, 1541592393646071809, '出库单', ' dw_inv ', 100, NULL, 0, '2022-06-28 09:26:15', 1265476890672672808, '2022-06-28 09:26:30', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1541593988588888066, 1541592393646071809, '产品管理', 'dw_pro', 100, NULL, 0, '2022-06-28 09:27:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594070629474305, 1541592393646071809, '产品类型', 'dw_type', 100, NULL, 0, '2022-06-28 09:27:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594136652013570, 1541592393646071809, '任务', 'dw_task', 100, NULL, 0, '2022-06-28 09:27:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594233825648641, 1541592393646071809, '工单', 'dw_work_order', 100, NULL, 0, '2022-06-28 09:28:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594693559115777, 1541592393646071809, '工艺路线', 'dw_work_route', 100, NULL, 0, '2022-06-28 09:29:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541594900241833985, 1541592393646071809, '工序', 'dw_work_step', 100, NULL, 0, '2022-06-28 09:30:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541700530479316994, 1534371402801537025, '出现划痕', '20220628', 100, NULL, 0, '2022-06-28 16:30:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541700792732368897, 1534371402801537025, '起皱凹陷', '65399', 100, NULL, 0, '2022-06-28 16:31:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (1541700927822606337, 1534375649047773185, '个', 'indal', 100, NULL, 2, '2022-06-28 16:31:59', 1265476890672672808, '2022-06-28 16:32:11', 1265476890672672808);
INSERT INTO `sys_dict_data` VALUES (1541704912394629121, 1534375649047773185, '片', 'slice', 100, NULL, 0, '2022-06-28 16:47:49', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                                  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                                  `sort` int(11) NOT NULL COMMENT '排序',
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `status` tinyint(4) NOT NULL COMMENT '状态（字典 0正常 1停用 2删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统字典类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1265216211667636226, '通用状态', 'common_status', 100, '通用状态', 0, '2020-05-26 17:40:26', 1265476890672672808, '2020-06-08 11:31:47', 1265476890672672808);
INSERT INTO `sys_dict_type` VALUES (1265216211667636234, '性别', 'sex', 100, '性别字典', 0, '2020-04-01 10:12:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265216211667636235, '常量的分类', 'consts_type', 100, '常量的分类，用于区别一组配置', 0, '2020-04-14 23:24:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265217074079453185, '是否', 'yes_or_no', 100, '是否', 0, '2020-05-26 17:43:52', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265217846770913282, '访问类型', 'vis_type', 100, '访问类型', 0, '2020-05-26 17:46:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265221049302372354, '菜单类型', 'menu_type', 100, '菜单类型', 0, '2020-05-26 17:59:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265466149622128641, '发送类型', 'send_type', 100, '发送类型', 0, '2020-05-27 10:13:36', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265466752209395713, '打开方式', 'open_type', 100, '打开方式', 0, '2020-05-27 10:16:00', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265467337566461954, '菜单权重', 'menu_weight', 100, '菜单权重', 0, '2020-05-27 10:18:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265468028632571905, '数据范围类型', 'data_scope_type', 100, '数据范围类型', 0, '2020-05-27 10:21:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1265468437904367618, '短信发送来源', 'sms_send_source', 100, '短信发送来源', 0, '2020-05-27 10:22:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1275617093517172738, '操作类型', 'op_type', 100, '操作类型', 0, '2020-06-24 10:29:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1277774529430654977, '文件存储位置', 'file_storage_location', 100, '文件存储位置', 0, '2020-06-30 09:22:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1278606951432855553, '运行状态', 'run_status', 100, '定时任务运行状态', 0, '2020-07-02 16:30:27', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1278911800547147777, '通知公告类型', 'notice_type', 100, '通知公告类型', 0, '2020-07-03 12:41:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1278911952657776642, '通知公告状态', 'notice_status', 100, '通知公告状态', 0, '2020-07-03 12:42:25', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1291390159941599233, '是否boolean', 'yes_true_false', 100, '是否boolean', 2, '2020-08-06 23:06:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1342445962104864770, '代码生成方式', 'code_gen_create_type', 100, '代码生成方式', 0, '2020-12-25 20:23:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1358094419419750401, '代码生成作用类型', 'code_gen_effect_type', 100, '代码生成作用类型', 0, '2021-02-07 00:45:16', 1265476890672672808, '2021-02-08 00:47:48', 1265476890672672808);
INSERT INTO `sys_dict_type` VALUES (1358457818733428737, '代码生成查询类型', 'code_gen_query_type', 100, '代码生成查询类型', 0, '2021-02-08 00:49:18', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1358470065111252994, '代码生成java类型', 'code_gen_java_type', 100, '代码生成java类型', 0, '2021-02-08 01:37:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534370205881409537, '入库类型', 'in_type', 100, '入库类型', 0, '2022-06-08 11:02:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534370298088988673, '出库类型', 'out_type', 100, '出库类型', 0, '2022-06-08 11:02:41', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534371402801537025, '不良品项', 'bad_item', 100, '不良品项', 0, '2022-06-08 11:07:04', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534371575181225985, '字段类型', 'field_type', 100, NULL, 0, '2022-06-08 11:07:45', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1534375649047773185, '库存单位', 'unit', 100, '库存单位', 0, '2022-06-08 11:23:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1541591973620080641, '时间类型单选', 'time_type', 100, NULL, 0, '2022-06-28 09:19:03', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (1541592393646071809, '自定义表名', 'cus_table_name', 100, NULL, 0, '2022-06-28 09:20:43', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_emp
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp`;
CREATE TABLE `sys_emp`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `job_num` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '工号',
                            `org_id` bigint(20) NOT NULL COMMENT '所属机构id',
                            `org_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属机构名称',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_emp
-- ----------------------------
INSERT INTO `sys_emp` VALUES (1275735541155614721, '001', 1265476890672672769, '华夏集团新疆分公司');
INSERT INTO `sys_emp` VALUES (1280700700074041345, '110', 1265476890672672771, '研发部');
INSERT INTO `sys_emp` VALUES (1280709549107552257, NULL, 1265476890672672770, '华夏集团成都分公司');
INSERT INTO `sys_emp` VALUES (1471457941179072513, NULL, 1265476890672672770, '华夏集团成都分公司');
INSERT INTO `sys_emp` VALUES (1551457178297200641, NULL, 1265476890651701250, '华夏集团');
INSERT INTO `sys_emp` VALUES (1551462379850723329, NULL, 1265476890651701250, '华夏集团');

-- ----------------------------
-- Table structure for sys_emp_ext_org_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_ext_org_pos`;
CREATE TABLE `sys_emp_ext_org_pos`  (
                                        `id` bigint(20) NOT NULL COMMENT '主键',
                                        `emp_id` bigint(20) NOT NULL COMMENT '员工id',
                                        `org_id` bigint(20) NOT NULL COMMENT '机构id',
                                        `pos_id` bigint(20) NOT NULL COMMENT '岗位id',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工附属机构岗位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_emp_ext_org_pos
-- ----------------------------

-- ----------------------------
-- Table structure for sys_emp_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_emp_pos`;
CREATE TABLE `sys_emp_pos`  (
                                `id` bigint(20) NOT NULL COMMENT '主键',
                                `emp_id` bigint(20) NOT NULL COMMENT '员工id',
                                `pos_id` bigint(20) NOT NULL COMMENT '职位id',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '员工职位关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_emp_pos
-- ----------------------------
INSERT INTO `sys_emp_pos` VALUES (1280710828479324161, 1280700700074041345, 1265476890672672790);
INSERT INTO `sys_emp_pos` VALUES (1471457757112041474, 1280709549107552257, 1265476890672672788);
INSERT INTO `sys_emp_pos` VALUES (1471457941179072514, 1471457941179072513, 1265476890672672788);
INSERT INTO `sys_emp_pos` VALUES (1528561867151138818, 1275735541155614721, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1551457178603384834, 1551457178297200641, 1265476890672672787);
INSERT INTO `sys_emp_pos` VALUES (1551462380186267650, 1551462379850723329, 1265476890672672787);

-- ----------------------------
-- Table structure for sys_file_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_info`;
CREATE TABLE `sys_file_info`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键id',
                                  `file_location` tinyint(4) NOT NULL COMMENT '文件存储位置（1:阿里云，2:腾讯云，3:minio，4:本地）',
                                  `file_bucket` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件仓库',
                                  `file_origin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名称（上传时候的文件名）',
                                  `file_suffix` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件后缀',
                                  `file_size_kb` bigint(20) NULL DEFAULT NULL COMMENT '文件大小kb',
                                  `file_size_info` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件大小信息，计算后的',
                                  `file_object_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '存储到bucket的名称（文件唯一标识id）',
                                  `file_path` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存储路径',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改用户',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_file_info
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `id` bigint(20) NOT NULL COMMENT '主键',
                             `pid` bigint(20) NOT NULL COMMENT '父id',
                             `pids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父ids',
                             `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                             `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                             `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '菜单类型（字典 0目录 1菜单 2按钮）',
                             `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
                             `router` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由地址',
                             `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件地址',
                             `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                             `application` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用分类（应用编码）',
                             `open_type` tinyint(4) NOT NULL COMMENT '打开方式（字典 0无 1组件 2内链 3外链）',
                             `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否可见（Y-是，N-否）',
                             `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接地址',
                             `redirect` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '重定向地址',
                             `weight` tinyint(4) NULL DEFAULT NULL COMMENT '权重（字典 1系统权重 2业务权重）',
                             `sort` int(11) NOT NULL COMMENT '排序',
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1264622039642255311, 0, '[0],', '主控面板', 'system_index', 0, 'home', '/', 'RouteView', NULL, 'system', 0, 'Y', NULL, '/analysis', 1, 1, NULL, 0, '2020-05-25 02:19:24', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255321, 1264622039642255311, '[0],[1264622039642255311],', '分析页', 'system_index_dashboard', 1, NULL, 'analysis', 'system/dashboard/Analysis', NULL, 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-25 02:21:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255331, 1264622039642255311, '[0],[1264622039642255311],', '工作台', 'system_index_workplace', 1, NULL, 'workplace', 'system/dashboard/Workplace', NULL, 'system', 0, 'Y', NULL, NULL, 1, 2, NULL, 0, '2020-05-25 02:23:48', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255341, 0, '[0],', '组织架构', 'sys_mgr', 0, 'team', '/sys', 'PageView', NULL, 'hr', 0, 'Y', NULL, NULL, 1, 2, NULL, 0, '2020-03-27 15:58:16', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255351, 1264622039642255341, '[0],[1264622039642255341],', '用户管理', 'sys_user_mgr', 1, NULL, '/mgr_user', 'system/user/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, '2020-03-27 16:10:21', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255361, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户查询', 'sys_user_mgr_page', 2, NULL, NULL, NULL, 'sysUser:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:36:49', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255371, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户编辑', 'sys_user_mgr_edit', 2, NULL, NULL, NULL, 'sysUser:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:20:23', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255381, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户增加', 'sys_user_mgr_add', 2, NULL, NULL, NULL, 'sysUser:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:37:35', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255391, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户删除', 'sys_user_mgr_delete', 2, NULL, NULL, NULL, 'sysUser:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:37:58', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255401, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户详情', 'sys_user_mgr_detail', 2, NULL, NULL, NULL, 'sysUser:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:38:25', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255411, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户导出', 'sys_user_mgr_export', 2, NULL, NULL, NULL, 'sysUser:export', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:21:59', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255421, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户选择器', 'sys_user_mgr_selector', 2, NULL, NULL, NULL, 'sysUser:selector', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-03 13:30:14', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255431, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户授权角色', 'sys_user_mgr_grant_role', 2, NULL, NULL, NULL, 'sysUser:grantRole', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:22:01', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255441, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户拥有角色', 'sys_user_mgr_own_role', 2, NULL, NULL, NULL, 'sysUser:ownRole', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:27:22', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255451, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户授权数据', 'sys_user_mgr_grant_data', 2, NULL, NULL, NULL, 'sysUser:grantData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:22:13', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255461, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户拥有数据', 'sys_user_mgr_own_data', 2, NULL, NULL, NULL, 'sysUser:ownData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:27:41', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255471, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户更新信息', 'sys_user_mgr_update_info', 2, NULL, NULL, NULL, 'sysUser:updateInfo', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 16:19:32', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255481, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户修改密码', 'sys_user_mgr_update_pwd', 2, NULL, NULL, NULL, 'sysUser:updatePwd', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 16:20:25', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255491, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户修改状态', 'sys_user_mgr_change_status', 2, NULL, NULL, NULL, 'sysUser:changeStatus', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:13:14', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255501, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户修改头像', 'sys_user_mgr_update_avatar', 2, NULL, NULL, NULL, 'sysUser:updateAvatar', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:21:42', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255511, 1264622039642255351, '[0],[1264622039642255341],[1264622039642255351],', '用户重置密码', 'sys_user_mgr_reset_pwd', 2, NULL, NULL, NULL, 'sysUser:resetPwd', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:01:51', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255521, 1264622039642255341, '[0],[1264622039642255341],', '机构管理', 'sys_org_mgr', 1, NULL, '/org', 'system/org/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, '2020-03-27 17:15:39', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255531, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构查询', 'sys_org_mgr_page', 2, NULL, NULL, NULL, 'sysOrg:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:17:37', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255541, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构列表', 'sys_org_mgr_list', 2, NULL, NULL, NULL, 'sysOrg:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:54:26', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255551, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构增加', 'sys_org_mgr_add', 2, NULL, NULL, NULL, 'sysOrg:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:19:53', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255561, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构编辑', 'sys_org_mgr_edit', 2, NULL, NULL, NULL, 'sysOrg:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:54:37', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255571, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构删除', 'sys_org_mgr_delete', 2, NULL, NULL, NULL, 'sysOrg:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:20:48', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255581, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构详情', 'sys_org_mgr_detail', 2, NULL, NULL, NULL, 'sysOrg:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:21:15', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255591, 1264622039642255521, '[0],[1264622039642255341],[1264622039642255521]', '机构树', 'sys_org_mgr_tree', 2, NULL, NULL, NULL, 'sysOrg:tree', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:21:58', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255601, 1264622039642255341, '[0],[1264622039642255341],', '职位管理', 'sys_pos_mgr', 1, NULL, '/pos', 'system/pos/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 5, NULL, 0, '2020-03-27 18:38:31', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255611, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位查询', 'sys_pos_mgr_page', 2, NULL, NULL, NULL, 'sysPos:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:41:48', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255621, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位列表', 'sys_pos_mgr_list', 2, NULL, NULL, NULL, 'sysPos:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:55:57', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255631, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位增加', 'sys_pos_mgr_add', 2, NULL, NULL, NULL, 'sysPos:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:42:20', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255641, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位编辑', 'sys_pos_mgr_edit', 2, NULL, NULL, NULL, 'sysPos:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:56:08', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255651, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位删除', 'sys_pos_mgr_delete', 2, NULL, NULL, NULL, 'sysPos:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:42:39', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255661, 1264622039642255601, '[0],[1264622039642255341],[1264622039642255601],', '职位详情', 'sys_pos_mgr_detail', 2, NULL, NULL, NULL, 'sysPos:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:43:00', 1265476890672672808, '2022-07-18 21:22:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255671, 0, '[0],', '权限管理', 'auth_manager', 0, 'safety-certificate', '/auth', 'PageView', NULL, 'hr', 0, 'Y', NULL, NULL, 1, 3, NULL, 0, '2020-07-15 15:51:57', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255681, 1264622039642255671, '[0],[1264622039642255671],', '应用管理', 'sys_app_mgr', 1, NULL, '/app', 'system/app/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 6, NULL, 0, '2020-03-27 16:40:21', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255691, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用查询', 'sys_app_mgr_page', 2, NULL, NULL, NULL, 'sysApp:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:41:58', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255701, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用列表', 'sys_app_mgr_list', 2, NULL, NULL, NULL, 'sysApp:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 10:04:59', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255711, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用增加', 'sys_app_mgr_add', 2, NULL, NULL, NULL, 'sysApp:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 16:44:10', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255721, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用编辑', 'sys_app_mgr_edit', 2, NULL, NULL, NULL, 'sysApp:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 10:04:34', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255731, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用删除', 'sys_app_mgr_delete', 2, NULL, NULL, NULL, 'sysApp:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:29', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255741, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '应用详情', 'sys_app_mgr_detail', 2, NULL, NULL, NULL, 'sysApp:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255751, 1264622039642255681, '[0],[1264622039642255671],[1264622039642255681],', '设为默认应用', 'sys_app_mgr_set_as_default', 2, NULL, NULL, NULL, 'sysApp:setAsDefault', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 17:14:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255761, 1264622039642255671, '[0],[1264622039642255671],', '菜单管理', 'sys_menu_mgr', 1, NULL, '/menu', 'system/menu/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 7, NULL, 0, '2020-03-27 18:44:35', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255771, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单列表', 'sys_menu_mgr_list', 2, NULL, NULL, NULL, 'sysMenu:list', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:45:20', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255781, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单增加', 'sys_menu_mgr_add', 2, NULL, NULL, NULL, 'sysMenu:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:45:37', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255791, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单编辑', 'sys_menu_mgr_edit', 2, NULL, NULL, NULL, 'sysMenu:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:52:00', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255801, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单删除', 'sys_menu_mgr_delete', 2, NULL, NULL, NULL, 'sysMenu:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:46:01', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255811, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单详情', 'sys_menu_mgr_detail', 2, NULL, NULL, NULL, 'sysMenu:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:46:22', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255821, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单授权树', 'sys_menu_mgr_grant_tree', 2, NULL, NULL, NULL, 'sysMenu:treeForGrant', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-03 09:50:31', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255831, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单树', 'sys_menu_mgr_tree', 2, NULL, NULL, NULL, 'sysMenu:tree', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-27 18:47:50', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255841, 1264622039642255761, '[0],[1264622039642255671],[1264622039642255761],', '菜单切换', 'sys_menu_mgr_change', 2, NULL, NULL, NULL, 'sysMenu:change', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-03 09:51:43', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255851, 1264622039642255671, '[0],[1264622039642255671],', '角色管理', 'sys_role_mgr', 1, NULL, '/role', 'system/role/index', NULL, 'hr', 1, 'Y', NULL, NULL, 1, 8, NULL, 0, '2020-03-28 16:01:09', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255861, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色查询', 'sys_role_mgr_page', 2, NULL, NULL, NULL, 'sysRole:page', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:09', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255871, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色增加', 'sys_role_mgr_add', 2, NULL, NULL, NULL, 'sysRole:add', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255881, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色编辑', 'sys_role_mgr_edit', 2, NULL, NULL, NULL, 'sysRole:edit', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:57:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255891, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色删除', 'sys_role_mgr_delete', 2, NULL, NULL, NULL, 'sysRole:delete', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:02:46', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255901, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色详情', 'sys_role_mgr_detail', 2, NULL, NULL, NULL, 'sysRole:detail', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-03-28 16:03:01', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255911, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色下拉', 'sys_role_mgr_drop_down', 2, NULL, NULL, NULL, 'sysRole:dropDown', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:45:39', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255921, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色授权菜单', 'sys_role_mgr_grant_menu', 2, NULL, NULL, NULL, 'sysRole:grantMenu', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:16:27', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255931, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色拥有菜单', 'sys_role_mgr_own_menu', 2, NULL, NULL, NULL, 'sysRole:ownMenu', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:21:54', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255941, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色授权数据', 'sys_role_mgr_grant_data', 2, NULL, NULL, NULL, 'sysRole:grantData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 09:16:56', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255951, 1264622039642255851, '[0],[1264622039642255671],[1264622039642255851],', '角色拥有数据', 'sys_role_mgr_own_data', 2, NULL, NULL, NULL, 'sysRole:ownData', 'hr', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 14:23:08', 1265476890672672808, '2022-07-18 21:22:50', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642255961, 0, '[0],', '开发管理', 'system_tools', 0, 'euro', '/tools', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, '2020-05-25 02:10:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255971, 1264622039642255961, '[0],[1264622039642255961],', '系统配置', 'system_tools_config', 1, NULL, '/config', 'system/config/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 9, NULL, 0, '2020-05-25 02:12:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255981, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置查询', 'system_tools_config_page', 2, NULL, NULL, NULL, 'sysConfig:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642255991, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置列表', 'system_tools_config_list', 2, NULL, NULL, NULL, 'sysConfig:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256001, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置增加', 'system_tools_config_add', 2, NULL, NULL, NULL, 'sysConfig:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256011, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置编辑', 'system_tools_config_edit', 2, NULL, NULL, NULL, 'sysConfig:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256021, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置删除', 'system_tools_config_delete', 2, NULL, NULL, NULL, 'sysConfig:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:03:44', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256031, 1264622039642255971, '[0],[1264622039642255961],[1264622039642255971],', '配置详情', 'system_tools_config_detail', 2, NULL, NULL, NULL, 'sysConfig:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-27 17:02:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256041, 1264622039642255961, '[0],[1264622039642255961],', '邮件发送', 'sys_email_mgr', 1, NULL, '/email', 'system/email/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 10, NULL, 0, '2020-07-02 11:44:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256051, 1264622039642256041, '[0],[1264622039642255961],[1264622039642256041],', '发送文本邮件', 'sys_email_mgr_send_email', 2, NULL, NULL, NULL, 'email:sendEmail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:45:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256061, 1264622039642256041, '[0],[1264622039642255961],[1264622039642256041],', '发送html邮件', 'sys_email_mgr_send_email_html', 2, NULL, NULL, NULL, 'email:sendEmailHtml', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 11:45:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256071, 1264622039642255961, '[0],[1264622039642255961],', '短信管理', 'sys_sms_mgr', 1, NULL, '/sms', 'system/sms/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 11, NULL, 0, '2020-07-02 12:00:12', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256081, 1264622039642256071, '[0],[1264622039642255961],[1264622039642256071],', '短信发送查询', 'sys_sms_mgr_page', 2, NULL, NULL, NULL, 'sms:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:16:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256091, 1264622039642256071, '[0],[1264622039642255961],[1264622039642256071],', '发送验证码短信', 'sys_sms_mgr_send_login_message', 2, NULL, NULL, NULL, 'sms:sendLoginMessage', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:02:31', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256101, 1264622039642256071, '[0],[1264622039642255961],[1264622039642256071],', '验证短信验证码', 'sys_sms_mgr_validate_message', 2, NULL, NULL, NULL, 'sms:validateMessage', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 12:02:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256111, 1264622039642255961, '[0],[1264622039642255961],', '字典管理', 'sys_dict_mgr', 1, NULL, '/dict', 'system/dict/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 12, NULL, 0, '2020-04-01 11:17:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256121, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型查询', 'sys_dict_mgr_dict_type_page', 2, NULL, NULL, NULL, 'sysDictType:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:20:22', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256131, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型列表', 'sys_dict_mgr_dict_type_list', 2, NULL, NULL, NULL, 'sysDictType:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-05-29 15:12:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256141, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型增加', 'sys_dict_mgr_dict_type_add', 2, NULL, NULL, NULL, 'sysDictType:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:19:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256151, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型删除', 'sys_dict_mgr_dict_type_delete', 2, NULL, NULL, NULL, 'sysDictType:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:21:30', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256161, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型编辑', 'sys_dict_mgr_dict_type_edit', 2, NULL, NULL, NULL, 'sysDictType:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:21:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256171, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型详情', 'sys_dict_mgr_dict_type_detail', 2, NULL, NULL, NULL, 'sysDictType:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:06', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256181, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型下拉', 'sys_dict_mgr_dict_type_drop_down', 2, NULL, NULL, NULL, 'sysDictType:dropDown', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256191, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典类型修改状态', 'sys_dict_mgr_dict_type_change_status', 2, NULL, NULL, NULL, 'sysDictType:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:15:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256201, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值查询', 'sys_dict_mgr_dict_page', 2, NULL, NULL, NULL, 'sysDictData:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:23:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256211, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值列表', 'sys_dict_mgr_dict_list', 2, NULL, NULL, NULL, 'sysDictData:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:58', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256221, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值增加', 'sys_dict_mgr_dict_add', 2, NULL, NULL, NULL, 'sysDictData:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:22:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256231, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值删除', 'sys_dict_mgr_dict_delete', 2, NULL, NULL, NULL, 'sysDictData:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:23:26', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256241, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值编辑', 'sys_dict_mgr_dict_edit', 2, NULL, NULL, NULL, 'sysDictData:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:21', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256251, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值详情', 'sys_dict_mgr_dict_detail', 2, NULL, NULL, NULL, 'sysDictData:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-04-01 11:24:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256261, 1264622039642256111, '[0],[1264622039642255961],[1264622039642256111],', '字典值修改状态', 'sys_dict_mgr_dict_change_status', 2, NULL, NULL, NULL, 'sysDictData:changeStatus', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-23 11:17:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256271, 1264622039642255961, '[0],[1264622039642255961],', '接口文档', 'sys_swagger_mgr', 1, NULL, '/swagger', 'Iframe', NULL, 'system', 2, 'Y', 'http://localhost:82/doc.html', NULL, 1, 13, NULL, 0, '2020-07-02 12:16:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256281, 0, '[0],', '日志管理', 'sys_log_mgr', 0, 'read', '/log', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 5, NULL, 0, '2020-04-01 09:25:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256291, 1264622039642256281, '[0],[1264622039642256281],', '访问日志', 'sys_log_mgr_vis_log', 1, NULL, '/vislog', 'system/log/vislog/index', NULL, 'system', 0, 'Y', NULL, NULL, 1, 14, NULL, 0, '2020-04-01 09:26:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256301, 1264622039642256291, '[0],[1264622039642256281],[1264622039642256291],', '访问日志查询', 'sys_log_mgr_vis_log_page', 2, NULL, NULL, NULL, 'sysVisLog:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:55:51', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256311, 1264622039642256291, '[0],[1264622039642256281],[1264622039642256291],', '访问日志清空', 'sys_log_mgr_vis_log_delete', 2, NULL, NULL, NULL, 'sysVisLog:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:56:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256321, 1264622039642256281, '[0],[1264622039642256281],', '操作日志', 'sys_log_mgr_op_log', 1, NULL, '/oplog', 'system/log/oplog/index', NULL, 'system', 0, 'Y', NULL, NULL, 1, 15, NULL, 0, '2020-04-01 09:26:59', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256331, 1264622039642256321, '[0],[1264622039642256281],[1264622039642256321],', '操作日志查询', 'sys_log_mgr_op_log_page', 2, NULL, NULL, NULL, 'sysOpLog:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:57:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256341, 1264622039642256321, '[0],[1264622039642256281],[1264622039642256321],', '操作日志清空', 'sys_log_mgr_op_log_delete', 2, NULL, NULL, NULL, 'sysOpLog:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-02 09:58:13', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256351, 0, '[0],', '系统监控', 'sys_monitor_mgr', 0, 'deployment-unit', '/monitor', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 6, NULL, 0, '2020-06-05 16:00:50', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256361, 1264622039642256351, '[0],[1264622039642256351],', '服务监控', 'sys_monitor_mgr_machine_monitor', 1, NULL, '/machine', 'system/machine/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 16, NULL, 0, '2020-06-05 16:02:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256371, 1264622039642256361, '[0],[1264622039642256351],[1264622039642256361],', '服务监控查询', 'sys_monitor_mgr_machine_monitor_query', 2, NULL, NULL, NULL, 'sysMachine:query', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:05:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256381, 1264622039642256351, '[0],[1264622039642256351],', '在线用户', 'sys_monitor_mgr_online_user', 1, NULL, '/onlineUser', 'system/onlineUser/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 17, NULL, 0, '2020-06-05 16:01:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256391, 1264622039642256381, '[0],[1264622039642256351],[1264622039642256381],', '在线用户列表', 'sys_monitor_mgr_online_user_list', 2, NULL, NULL, NULL, 'sysOnlineUser:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:03:46', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256401, 1264622039642256381, '[0],[1264622039642256351],[1264622039642256381],', '在线用户强退', 'sys_monitor_mgr_online_user_force_exist', 2, NULL, NULL, NULL, 'sysOnlineUser:forceExist', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-05 16:04:16', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256411, 1264622039642256351, '[0],[1264622039642256351],', '数据监控', 'sys_monitor_mgr_druid', 1, NULL, '/druid', 'Iframe', NULL, 'system', 2, 'Y', 'http://localhost:82/druid', NULL, 1, 18, NULL, 0, '2020-06-28 16:15:07', 1265476890672672808, '2020-09-13 09:39:10', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256421, 0, '[0],', '通知公告', 'sys_notice', 0, 'sound', '/notice', 'PageView', NULL, 'teamwork', 1, 'Y', NULL, NULL, 1, 7, NULL, 0, '2020-06-29 15:41:53', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256431, 1264622039642256421, '[0],[1264622039642256421],', '公告管理', 'sys_notice_mgr', 1, NULL, '/notice', 'system/notice/index', NULL, 'teamwork', 1, 'Y', NULL, NULL, 1, 19, NULL, 0, '2020-06-29 15:44:24', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256441, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告查询', 'sys_notice_mgr_page', 2, NULL, NULL, NULL, 'sysNotice:page', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:45:30', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256451, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告增加', 'sys_notice_mgr_add', 2, NULL, NULL, NULL, 'sysNotice:add', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:45:57', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256461, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告编辑', 'sys_notice_mgr_edit', 2, NULL, NULL, NULL, 'sysNotice:edit', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:22', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256471, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告删除', 'sys_notice_mgr_delete', 2, NULL, NULL, NULL, 'sysNotice:delete', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:11', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256481, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告查看', 'sys_notice_mgr_detail', 2, NULL, NULL, NULL, 'sysNotice:detail', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:33', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256491, 1264622039642256431, '[0],[1264622039642256421],[1264622039642256431],', '公告修改状态', 'sys_notice_mgr_changeStatus', 2, NULL, NULL, NULL, 'sysNotice:changeStatus', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 15:46:50', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256501, 1264622039642256421, '[0],[1264622039642256421],', '已收公告', 'sys_notice_mgr_received', 1, NULL, '/noticeReceived', 'system/noticeReceived/index', NULL, 'teamwork', 1, 'Y', NULL, NULL, 1, 20, NULL, 0, '2020-06-29 16:32:53', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256511, 1264622039642256501, '[0],[1264622039642256421],[1264622039642256501],', '已收公告查询', 'sys_notice_mgr_received_page', 2, NULL, NULL, NULL, 'sysNotice:received', 'teamwork', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-29 16:33:43', 1265476890672672808, '2022-07-18 21:17:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1264622039642256521, 0, '[0],', '文件管理', 'sys_file_mgr', 0, 'file', '/file', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 8, NULL, 0, '2020-06-24 17:31:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256531, 1264622039642256521, '[0],[1264622039642256521],', '系统文件', 'sys_file_mgr_sys_file', 1, NULL, '/file', 'system/file/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 21, NULL, 0, '2020-06-24 17:32:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256541, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件查询', 'sys_file_mgr_sys_file_page', 2, NULL, NULL, NULL, 'sysFileInfo:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:38', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256551, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件列表', 'sys_file_mgr_sys_file_list', 2, NULL, NULL, NULL, 'sysFileInfo:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256561, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件删除', 'sys_file_mgr_sys_file_delete', 2, NULL, NULL, NULL, 'sysFileInfo:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:36:11', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256571, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件详情', 'sys_file_mgr_sys_file_detail', 2, NULL, NULL, NULL, 'sysFileInfo:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:36:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256581, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件上传', 'sys_file_mgr_sys_file_upload', 2, NULL, NULL, NULL, 'sysFileInfo:upload', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:34:29', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256591, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '文件下载', 'sys_file_mgr_sys_file_download', 2, NULL, NULL, NULL, 'sysFileInfo:download', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:34:55', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256601, 1264622039642256531, '[0],[1264622039642256521],[1264622039642256531],', '图片预览', 'sys_file_mgr_sys_file_preview', 2, NULL, NULL, NULL, 'sysFileInfo:preview', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-06-24 17:35:19', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256611, 0, '[0],', '定时任务', 'sys_timers', 0, 'dashboard', '/timers', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:17:20', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256621, 1264622039642256611, '[0],[1264622039642256611],', '任务管理', 'sys_timers_mgr', 1, NULL, '/timers', 'system/timers/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 22, NULL, 0, '2020-07-01 17:18:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256631, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务查询', 'sys_timers_mgr_page', 2, NULL, NULL, NULL, 'sysTimers:page', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:19:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256641, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务列表', 'sys_timers_mgr_list', 2, NULL, NULL, NULL, 'sysTimers:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:19:56', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256651, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务详情', 'sys_timers_mgr_detail', 2, NULL, NULL, NULL, 'sysTimers:detail', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:10', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256661, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务增加', 'sys_timers_mgr_add', 2, NULL, NULL, NULL, 'sysTimers:add', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:23', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256671, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务删除', 'sys_timers_mgr_delete', 2, NULL, NULL, NULL, 'sysTimers:delete', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:33', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256681, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务编辑', 'sys_timers_mgr_edit', 2, NULL, NULL, NULL, 'sysTimers:edit', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:20:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256691, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务可执行列表', 'sys_timers_mgr_get_action_classes', 2, NULL, NULL, NULL, 'sysTimers:getActionClasses', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:16', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256701, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务启动', 'sys_timers_mgr_start', 2, NULL, NULL, NULL, 'sysTimers:start', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:32', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256711, 1264622039642256621, '[0],[1264622039642256611],[1264622039642256621],', '定时任务关闭', 'sys_timers_mgr_stop', 2, NULL, NULL, NULL, 'sysTimers:stop', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-07-01 17:22:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256721, 0, '[0],', '区域管理', 'sys_area', 0, 'environment', '/area', 'PageView', NULL, 'system', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 13:55:40', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256731, 1264622039642256721, '[0],[1264622039642256721],', '系统区域', 'sys_area_mgr', 1, NULL, '/area', 'system/area/index', NULL, 'system', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 13:57:42', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1264622039642256741, 1264622039642256731, '[0],[1264622039642256721],[1264622039642256731],', '系统区域列表', 'sys_area_mgr_list', 2, NULL, NULL, NULL, 'sysArea:list', 'system', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, '2021-05-19 14:01:39', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1342445437296771074, 0, '[0],', '代码生成', 'code_gen', 1, 'thunderbolt', '/codeGenerate/index', 'gen/codeGenerate/index', NULL, 'system_tool', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, '2020-12-25 20:21:48', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1410859007809736705, 1264622039642256521, '[0],[1264622039642256521],', '在线文档', 'file_oline', 1, '', '/file_oline', 'system/fileOnline/index', '', 'system', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2021-07-02 15:12:55', 1265476890672672808, '2021-08-25 20:02:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1534384317233524738, 0, '[0],', '基础数据', 'basic_data', 0, 'setting', '/basicData', 'PageView', '', 'manufacturing', 1, 'Y', NULL, '', 1, 11, NULL, 0, '2022-06-08 11:58:23', 1265476890672672808, '2022-06-08 13:42:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1534410297847214081, 0, '[0],', '生产管理', 'prod_manage', 0, 'hdd', '/prodManage', 'PageView', '', 'manufacturing', 1, 'Y', NULL, '', 1, 12, NULL, 0, '2022-06-08 13:41:38', 1265476890672672808, '2022-06-08 13:42:54', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1534411830716350466, 0, '[0],', '库存管理', 'inv_mgt', 0, 'desktop', '/inv/mgt', 'PageView', '', 'manufacturing', 0, 'Y', NULL, '', 1, 100, NULL, 0, '2022-06-08 13:47:43', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1538776469361139714, 0, '[0],', '大屏展示', 'bigScreen', 1, 'radar-chart', '/bigScreen/index', 'main/bigScreen/index', '', 'manufacturing', 1, 'Y', NULL, '', 1, 1, NULL, 0, '2022-06-20 14:51:14', 1265476890672672808, '2022-06-20 14:52:41', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1542409869531873282, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工审批', 'work_report_approval', 2, '', '', '', 'workReport:approval', 'manufacturing', 0, 'Y', '', '', 1, 100, NULL, 0, '2022-06-30 15:29:04', 1265476890672672808, '2022-06-30 15:29:19', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1551480636561321985, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型树', 'protype_index_tree', 2, '', '', '', 'proType:tree', 'manufacturing', 0, 'Y', '', '', 2, 100, NULL, 0, '2022-07-25 16:13:04', 1265476890672672808, '2022-07-25 16:13:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1552209067446579201, 0, '[0],', '采购管理', 'purc_manage', 0, 'schedule', '/purcManage', 'PageView', '', 'manufacturing', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2022-07-27 16:27:35', 1265476890672672808, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1557930917148868610, 0, '[0],', '供应商管理', 'pro_supplier', 0, 'file-search', '/proSupplier', 'PageView', '', 'crm', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2022-08-12 11:24:10', 1265476890672672808, '2022-08-12 11:36:21', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (1557931401653895170, 0, '[0],', '客户管理', 'customer', 0, 'file-done', '/customer', 'PageView', '', 'crm', 1, 'Y', NULL, '', 1, 100, NULL, 0, '2022-08-12 11:26:06', 1265476890672672808, '2022-08-12 11:37:06', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4621333945127042674, 8957317986837088422, '[0],[8957317986837088422],', '字段配置新增', 'fieldconfig_index_add', 2, NULL, NULL, NULL, 'fieldConfig:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4674816360932725215, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类查询', 'cussort_index_page', 2, NULL, NULL, NULL, 'cusSort:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4678695510847297372, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类新增', 'suppsort_index_add', 2, NULL, NULL, NULL, 'suppSort:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4700141466883779130, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明导出', 'puordetail_index_export', 2, NULL, NULL, NULL, 'puorDetail:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4719172401435180793, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单编辑', 'bom_index_edit', 2, NULL, NULL, NULL, 'bom:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4757443910107701590, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划编辑', 'proplan_index_edit', 2, NULL, NULL, NULL, 'proPlan:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4770899845682577376, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则查询', 'customcode_index_page', 2, NULL, NULL, NULL, 'customCode:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4794640277308881312, 1557930917148868610, '[0],[1557930917148868610],', '供应商分类', 'suppsort_index', 1, 'ordered-list', '/suppSort', 'main/suppsort/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4830986669641223230, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表新增', 'workorder_index_add', 2, NULL, NULL, NULL, 'workOrder:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4832107133629024589, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表列表', 'pro_index_list', 2, NULL, NULL, NULL, 'pro:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4834264129878073409, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明删除', 'puordetail_index_delete', 2, NULL, NULL, NULL, 'puorDetail:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4835110481928644952, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单列表', 'bom_index_list', 2, NULL, NULL, NULL, 'bom:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4890328064515383248, 0, '[0],', 'SaaS租户', 'tenantinfo_index', 1, NULL, '/tenantInfo', 'main/tenantinfo/index', NULL, 'ht', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (4903707486851837034, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类导出', 'cussort_index_export', 2, NULL, NULL, NULL, 'cusSort:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4967796364303223216, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单列表', 'invOut_index_list', 2, NULL, NULL, NULL, 'invOut:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (4998978684524443692, 1534384317233524738, '[0],[1534384317233524738],', '工序', 'workstep_index', 1, 'deployment-unit', '/workStep', 'main/workstep/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2022-08-12 11:15:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5000566847705099773, 1534411830716350466, '[0],[1534411830716350466],', '仓库管理', 'warehouse_index', 1, 'hdd', '/wareHouse', 'main/warehouse/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:49:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5002055413507195458, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细新增', 'invdetail_index_add', 2, NULL, NULL, NULL, 'invDetail:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5012839659883217263, 1534411830716350466, '[0],[1534411830716350466],', '入库单', 'invIn_index', 1, 'login', '/invIn', 'main/invIn/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5014700545593967407, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务列表', 'task_index_list', 2, NULL, NULL, NULL, 'task:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5016248121790626902, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划导出', 'proplan_index_export', 2, NULL, NULL, NULL, 'proPlan:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5027224879967170377, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人查询', 'suppperson_index_page', 2, NULL, NULL, NULL, 'suppPerson:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5079108238558434584, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线列表', 'workroute_index_list', 2, NULL, NULL, NULL, 'workRoute:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5091294346539048091, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户删除', 'tenantinfo_index_delete', 2, NULL, NULL, NULL, 'tenantInfo:delete', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5101364812197334235, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类新增', 'suppsort_index_add', 2, NULL, NULL, NULL, 'suppSort:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5142735407151148234, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表列表', 'protype_index_list', 2, NULL, NULL, NULL, 'proType:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5162865219757254273, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明查询', 'puordetail_index_page', 2, NULL, NULL, NULL, 'puorDetail:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5196743176498588333, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料查询', 'suppdata_index_page', 2, NULL, NULL, NULL, 'suppData:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5197311839117522988, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人新增', 'suppperson_index_add', 2, NULL, NULL, NULL, 'suppPerson:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5206868747918117433, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划新增', 'proplan_index_add', 2, NULL, NULL, NULL, 'proPlan:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5243613315131080710, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线查询', 'workroute_index_page', 2, NULL, NULL, NULL, 'workRoute:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5274675770695528925, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单删除', 'puororder_index_delete', 2, NULL, NULL, NULL, 'puorOrder:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5276828216703973340, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料新增', 'cusinfor_index_add', 2, NULL, NULL, NULL, 'cusInfor:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5279583233581044419, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表编辑', 'pro_index_edit', 2, NULL, NULL, NULL, 'pro:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5327083612717101630, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表删除', 'workordertask_index_delete', 2, NULL, NULL, NULL, 'workOrderTask:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5346549595871021092, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人查看', 'cusperson_index_detail', 2, NULL, NULL, NULL, 'cusPerson:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5378908852735764919, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工编辑', 'workreport_index_edit', 2, NULL, NULL, NULL, 'workReport:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5382528335658667107, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单编辑', 'puororder_index_edit', 2, NULL, NULL, NULL, 'puorOrder:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5448185299220002627, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料删除', 'cusinfor_index_delete', 2, NULL, NULL, NULL, 'cusInfor:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5457126757845470309, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单删除', 'saleorder_index_delete', 2, NULL, NULL, NULL, 'saleOrder:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5459011313540319790, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额导出', 'stockbalance_index_export', 2, NULL, NULL, NULL, 'stockBalance:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5465134665687105949, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人查看', 'suppperson_index_detail', 2, NULL, NULL, NULL, 'suppPerson:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5469043739505193743, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额查询', 'stockbalance_index_page', 2, NULL, NULL, NULL, 'stockBalance:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5474507168841280952, 0, '[0],', '编号规则', 'customcode_index', 1, 'setting', '/customCode', 'main/customcode/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5480200211927228567, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则新增', 'customcode_index_add', 2, NULL, NULL, NULL, 'customCode:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5523506981641034218, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划查询', 'proplan_index_page', 2, NULL, NULL, NULL, 'proPlan:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5553078347948343545, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表导出', 'protype_index_export', 2, NULL, NULL, NULL, 'proType:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5555524248707858432, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理查询', 'warehouse_index_page', 2, NULL, NULL, NULL, 'wareHouse:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5587279777867961498, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人删除', 'cusperson_index_delete', 2, NULL, NULL, NULL, 'cusPerson:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5608609939719113803, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类查看', 'suppsort_index_detail', 2, NULL, NULL, NULL, 'suppSort:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5624458298951752771, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单导出', 'invIn_index_export', 2, NULL, NULL, NULL, 'invIn:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5642803668063215805, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务编辑', 'task_index_edit', 2, NULL, NULL, NULL, 'task:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5653092812632926489, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则删除', 'customcode_index_delete', 2, NULL, NULL, NULL, 'customCode:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5668705457330168859, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表查询', 'workordertask_index_page', 2, NULL, NULL, NULL, 'workOrderTask:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5707993274387362828, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序新增', 'workstep_index_add', 2, NULL, NULL, NULL, 'workStep:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5714641428045333341, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理导出', 'warehouse_index_export', 2, NULL, NULL, NULL, 'wareHouse:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5721174223820977430, 1534411830716350466, '[0],[1534411830716350466],', '库存余额', 'stockbalance_index', 1, 'gold', '/stockBalance', 'main/stockbalance/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5753930915061776016, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工列表', 'workreport_index_list', 2, NULL, NULL, NULL, 'workReport:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5788080164048630938, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线编辑', 'workroute_index_edit', 2, NULL, NULL, NULL, 'workRoute:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5801879553212456867, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理新增', 'warehouse_index_add', 2, NULL, NULL, NULL, 'wareHouse:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5803272132980049305, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细查询', 'invdetail_index_page', 2, NULL, NULL, NULL, 'invDetail:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5820472386138045917, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表查询', 'worksteproute_index_page', 2, NULL, NULL, NULL, 'workStepRoute:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5846194751422568093, 8957317986837088422, '[0],[8957317986837088422],', '字段配置导出', 'fieldconfig_index_export', 2, NULL, NULL, NULL, 'fieldConfig:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5871841993475141582, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料列表', 'cusinfor_index_list', 2, NULL, NULL, NULL, 'cusInfor:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5886355131876064420, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表编辑', 'workordertask_index_edit', 2, NULL, NULL, NULL, 'workOrderTask:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5886900280768283007, 8957317986837088422, '[0],[8957317986837088422],', '字段配置编辑', 'fieldconfig_index_edit', 2, NULL, NULL, NULL, 'fieldConfig:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (5893520734687245059, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单删除', 'invOut_index_delete', 2, NULL, NULL, NULL, 'invOut:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5901981891345615661, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理列表', 'warehouse_index_list', 2, NULL, NULL, NULL, 'wareHouse:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5934688735467584877, 1534410297847214081, '[0],[1534410297847214081],', '销售订单', 'saleorder_index', 1, 'file-ppt', '/saleOrder', 'main/saleorder/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-08-12 11:21:12', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (5999927780812383399, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单列表', 'puororder_index_list', 2, NULL, NULL, NULL, 'puorOrder:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6015462925549607928, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单导出', 'bom_index_export', 2, NULL, NULL, NULL, 'bom:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6020421073902938241, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表导出', 'workordertask_index_export', 2, NULL, NULL, NULL, 'workOrderTask:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6037995103127006961, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户导出', 'tenantinfo_index_export', 2, NULL, NULL, NULL, 'tenantInfo:export', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6045763991836034103, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单新增', 'saleorder_index_add', 2, NULL, NULL, NULL, 'saleOrder:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6046397975825687693, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单新增', 'puororder_index_add', 2, NULL, NULL, NULL, 'puorOrder:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6103813152661113624, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表列表', 'worksteproute_index_list', 2, NULL, NULL, NULL, 'workStepRoute:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6116805160244737678, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表删除', 'worksteproute_index_delete', 2, NULL, NULL, NULL, 'workStepRoute:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6122595225588020183, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表编辑', 'workorder_index_edit', 2, NULL, NULL, NULL, 'workOrder:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6122993178045887049, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料列表', 'suppdata_index_list', 2, NULL, NULL, NULL, 'suppData:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6134354751483903106, 1557930917148868610, '[0],[1557930917148868610],', '供应商资料', 'suppdata_index', 1, 'solution', '/suppData', 'main/suppdata/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6194217070047486945, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线删除', 'workroute_index_delete', 2, NULL, NULL, NULL, 'workRoute:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6201934794874381431, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类删除', 'suppsort_index_delete', 2, NULL, NULL, NULL, 'suppSort:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6204862670753579307, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单新增', 'bom_index_add', 2, NULL, NULL, NULL, 'bom:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6238776219082572290, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人编辑', 'cusperson_index_edit', 2, NULL, NULL, NULL, 'cusPerson:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6249597959063106635, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序删除', 'workstep_index_delete', 2, NULL, NULL, NULL, 'workStep:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6257208246505535137, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单编辑', 'saleorder_index_edit', 2, NULL, NULL, NULL, 'saleOrder:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6280770118494855200, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单列表', 'invIn_index_list', 2, NULL, NULL, NULL, 'invIn:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6290232610677682017, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细导出', 'invdetail_index_export', 2, NULL, NULL, NULL, 'invDetail:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6299920358694835309, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务查看', 'task_index_detail', 2, NULL, NULL, NULL, 'task:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6326961474985796183, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单新增', 'invOut_index_add', 2, NULL, NULL, NULL, 'invOut:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6329171771009724483, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表列表', 'workordertask_index_list', 2, NULL, NULL, NULL, 'workOrderTask:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6347609996552577764, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细查看', 'invdetail_index_detail', 2, NULL, NULL, NULL, 'invDetail:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6354039257511829492, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单查看', 'saleorder_index_detail', 2, NULL, NULL, NULL, 'saleOrder:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6374585520258378360, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表导出', 'worksteproute_index_export', 2, NULL, NULL, NULL, 'workStepRoute:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6385901691378408995, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类导出', 'suppsort_index_export', 2, NULL, NULL, NULL, 'suppSort:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6405406552095053594, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单查询', 'puororder_index_page', 2, NULL, NULL, NULL, 'puorOrder:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6414357916883420801, 1557930917148868610, '[0],[1557930917148868610],', '供应商联系人', 'suppperson_index', 1, 'team', '/suppPerson', 'main/suppperson/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6429021782911745716, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类删除', 'cussort_index_delete', 2, NULL, NULL, NULL, 'cusSort:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6436044181834026359, 1534384317233524738, '[0],[1534384317233524738],', '产品管理', 'pro_index', 1, 'barcode', '/pro', 'main/pro/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, NULL, NULL, '2022-08-12 11:15:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6449912099524079616, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序导出', 'workstep_index_export', 2, NULL, NULL, NULL, 'workStep:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6494634442879742520, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人删除', 'suppperson_index_delete', 2, NULL, NULL, NULL, 'suppPerson:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6507903030541782366, 1534410297847214081, '[0],[1534410297847214081],', '工单', 'workorder_index', 1, 'cloud-download', '/workOrder', 'main/workorder/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-08-12 11:21:44', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6527167616560933135, 1534384317233524738, '[0],[1534384317233524738],', '产品类型', 'protype_index', 1, 'cluster', '/proType', 'main/protype/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-08-12 11:15:16', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6534238167742390015, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类查询', 'suppsort_index_page', 2, NULL, NULL, NULL, 'suppSort:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6535734057645557495, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人列表', 'suppperson_index_list', 2, NULL, NULL, NULL, 'suppPerson:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6547301851685353031, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表查看', 'protype_index_detail', 2, NULL, NULL, NULL, 'proType:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6570253563256899359, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表新增', 'protype_index_add', 2, NULL, NULL, NULL, 'proType:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6591367648784009715, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单查询', 'bom_index_page', 2, NULL, NULL, NULL, 'bom:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6655166482691941995, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料导出', 'cusinfor_index_export', 2, NULL, NULL, NULL, 'cusInfor:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6659298240010835782, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人编辑', 'suppperson_index_edit', 2, NULL, NULL, NULL, 'suppPerson:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6667327679590719676, 1534410297847214081, '[0],[1534410297847214081],', '工单任务关系', 'workordertask_index', 1, 'box-plot', '/workOrderTask', 'main/workordertask/index', NULL, 'manufacturing', 1, 'N', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-06-28 14:11:59', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6696300230304937737, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料查询', 'cusinfor_index_page', 2, NULL, NULL, NULL, 'cusInfor:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6700801806460342017, 0, '[0],', '供应商分类', 'suppsort_index', 1, '', '/suppSort', 'main/suppsort/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6704297887824939633, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类列表', 'suppsort_index_list', 2, NULL, NULL, NULL, 'suppSort:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6709768906758836391, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序查看', 'workstep_index_detail', 2, NULL, NULL, NULL, 'workStep:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6739113753546148532, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理查看', 'warehouse_index_detail', 2, NULL, NULL, NULL, 'wareHouse:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6813707923637624035, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单导出', 'puororder_index_export', 2, NULL, NULL, NULL, 'puorOrder:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6845666717193712873, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表查看', 'pro_index_detail', 2, NULL, NULL, NULL, 'pro:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6860312372874164125, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表查看', 'worksteproute_index_detail', 2, NULL, NULL, NULL, 'workStepRoute:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6879316079334546932, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户查询', 'tenantinfo_index_page', 2, NULL, NULL, NULL, 'tenantInfo:page', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (6900932255841430981, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单查询', 'invOut_index_page', 2, NULL, NULL, NULL, 'invOut:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6905244625723471705, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序查询', 'workstep_index_page', 2, NULL, NULL, NULL, 'workStep:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6947194724669964805, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类删除', 'suppsort_index_delete', 2, NULL, NULL, NULL, 'suppSort:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (6996269839693777638, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类列表', 'suppsort_index_list', 2, NULL, NULL, NULL, 'suppSort:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7030392685370585623, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户查看', 'tenantinfo_index_detail', 2, NULL, NULL, NULL, 'tenantInfo:detail', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7044559182298687818, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理编辑', 'warehouse_index_edit', 2, NULL, NULL, NULL, 'wareHouse:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7047324015553475476, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单查看', 'bom_index_detail', 2, NULL, NULL, NULL, 'bom:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7072750871619746982, 1557931401653895170, '[0],[1557931401653895170],', '客户分类', 'cussort_index', 1, 'folder-open', '/cusSort', 'main/cussort/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 50, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7119232719243003213, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类导出', 'suppsort_index_export', 2, NULL, NULL, NULL, 'suppSort:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7128085356547940830, 1534384317233524738, '[0],[1534384317233524738],', '物料清单', 'bom_index', 1, 'bg-colors', '/bom', 'main/bom/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:37:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7200474546305867050, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则列表', 'customcode_index_list', 2, NULL, NULL, NULL, 'customCode:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7209147716433162075, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料编辑', 'suppdata_index_edit', 2, NULL, NULL, NULL, 'suppData:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7242870236365548592, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则导出', 'customcode_index_export', 2, NULL, NULL, NULL, 'customCode:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7254023492833901715, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单删除', 'invIn_index_delete', 2, NULL, NULL, NULL, 'invIn:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7287765970469918079, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则查看', 'customcode_index_detail', 2, NULL, NULL, NULL, 'customCode:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7288710125399936914, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工新增', 'workreport_index_add', 2, NULL, NULL, NULL, 'workReport:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7294415501551357401, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类查看', 'suppsort_index_detail', 2, NULL, NULL, NULL, 'suppSort:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7318591877639154878, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单编辑', 'invIn_index_edit', 2, NULL, NULL, NULL, 'invIn:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7323225129555267277, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细删除', 'invdetail_index_delete', 2, NULL, NULL, NULL, 'invDetail:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7337865129337018225, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料新增', 'suppdata_index_add', 2, NULL, NULL, NULL, 'suppData:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7346283523793183379, 1534410297847214081, '[0],[1534410297847214081],', '报工', 'workreport_index', 1, 'file', '/workReport', 'main/workreport/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 4, NULL, 0, NULL, NULL, '2022-06-08 13:44:54', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7380411400614516902, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人新增', 'cusperson_index_add', 2, NULL, NULL, NULL, 'cusPerson:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7388361180917918171, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序列表', 'workstep_index_list', 2, NULL, NULL, NULL, 'workStep:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7395287038762629813, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细编辑', 'invdetail_index_edit', 2, NULL, NULL, NULL, 'invDetail:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7455112058491820064, 8957317986837088422, '[0],[8957317986837088422],', '字段配置删除', 'fieldconfig_index_delete', 2, NULL, NULL, NULL, 'fieldConfig:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7516608184768822750, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划删除', 'proplan_index_delete', 2, NULL, NULL, NULL, 'proPlan:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7529678467290942551, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料查看', 'suppdata_index_detail', 2, NULL, NULL, NULL, 'suppData:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7561922286979539034, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单查询', 'saleorder_index_page', 2, NULL, NULL, NULL, 'saleOrder:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7581916369308597479, 7128085356547940830, '[0],[1534384317233524738],[7128085356547940830],', '物料清单删除', 'bom_index_delete', 2, NULL, NULL, NULL, 'bom:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:27:08', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7596329328691560085, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类新增', 'cussort_index_add', 2, NULL, NULL, NULL, 'cusSort:add', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7621956982866257638, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表查询', 'protype_index_page', 2, NULL, NULL, NULL, 'proType:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7629147488562302699, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单查看', 'invOut_index_detail', 2, NULL, NULL, NULL, 'invOut:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7635960329677377171, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类编辑', 'cussort_index_edit', 2, NULL, NULL, NULL, 'cusSort:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7639361031662168624, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表导出', 'workorder_index_export', 2, NULL, NULL, NULL, 'workOrder:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7656571819576826411, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务开始\r\n', 'task_index_add', 2, NULL, NULL, NULL, 'task:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7678659259268274624, 8957317986837088422, '[0],[8957317986837088422],', '字段配置查看', 'fieldconfig_index_detail', 2, NULL, NULL, NULL, 'fieldConfig:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (7682643462888492777, 8016744771368544956, '[0],[1534411830716350466],[8016744771368544956],', '出入库明细列表', 'invdetail_index_list', 2, NULL, NULL, NULL, 'invDetail:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7702400310012060990, 1534384317233524738, '[0],[1534384317233524738],', '工艺路线', 'workroute_index', 1, 'gateway', '/workRoute', 'main/workroute/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-08-12 11:15:55', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7721498005843290304, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人查询', 'cusperson_index_page', 2, NULL, NULL, NULL, 'cusPerson:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7723724790018395451, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额查看', 'stockbalance_index_detail', 2, NULL, NULL, NULL, 'stockBalance:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7764860386896651377, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额删除', 'stockbalance_index_delete', 2, NULL, NULL, NULL, 'stockBalance:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7812048842906278623, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单新增', 'invIn_index_add', 2, NULL, NULL, NULL, 'invIn:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7845222658049694795, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明编辑', 'puordetail_index_edit', 2, NULL, NULL, NULL, 'puorDetail:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7882898168203081196, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务查询', 'task_index_page', 2, NULL, NULL, NULL, 'task:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7898061431233899837, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料编辑', 'cusinfor_index_edit', 2, NULL, NULL, NULL, 'cusInfor:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7909153361025684710, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明查看', 'puordetail_index_detail', 2, NULL, NULL, NULL, 'puorDetail:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7936411315868128942, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线导出', 'workroute_index_export', 2, NULL, NULL, NULL, 'workRoute:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7940657177277675482, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线新增', 'workroute_index_add', 2, NULL, NULL, NULL, 'workRoute:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7946795106595000695, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表新增', 'pro_index_add', 2, NULL, NULL, NULL, 'pro:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7950819887425681478, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表删除', 'pro_index_delete', 2, NULL, NULL, NULL, 'pro:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (7974141988632466544, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表删除', 'workorder_index_delete', 2, NULL, NULL, NULL, 'workOrder:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8000986046974593578, 6414357916883420801, '[0],[1557930917148868610],[6414357916883420801],', '供应商联系人导出', 'suppperson_index_export', 2, NULL, NULL, NULL, 'suppPerson:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:24:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8016744771368544956, 1534411830716350466, '[0],[1534411830716350466],', '出入库明细', 'invdetail_index', 1, 'bar-chart', '/invDetail', 'main/invdetail/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 3, NULL, 0, NULL, NULL, '2022-06-10 16:05:47', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8088901678692869692, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务结束', 'task_index_export', 2, NULL, NULL, NULL, 'task:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8099644559700436461, 8957317986837088422, '[0],[8957317986837088422],', '字段配置查询', 'fieldconfig_index_page', 2, NULL, NULL, NULL, 'fieldConfig:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8107628912599125262, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类列表', 'cussort_index_list', 2, NULL, NULL, NULL, 'cusSort:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8175022271486866402, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类查询', 'suppsort_index_page', 2, NULL, NULL, NULL, 'suppSort:page', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8181816557328888214, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人列表', 'cusperson_index_list', 2, NULL, NULL, NULL, 'cusPerson:list', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8181822590852115190, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表编辑', 'worksteproute_index_edit', 2, NULL, NULL, NULL, 'workStepRoute:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8197158197771689059, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表编辑', 'protype_index_edit', 2, NULL, NULL, NULL, 'proType:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8254320371052864398, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额列表', 'stockbalance_index_list', 2, NULL, NULL, NULL, 'stockBalance:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8259333771823873238, 7072750871619746982, '[0],[1557931401653895170],[7072750871619746982],', '客户分类查看', 'cussort_index_detail', 2, NULL, NULL, NULL, 'cusSort:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:17', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8273410905135864011, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额编辑', 'stockbalance_index_edit', 2, NULL, NULL, NULL, 'stockBalance:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8275137972166495794, 4998978684524443692, '[0],[1534384317233524738],[4998978684524443692],', '工序编辑', 'workstep_index_edit', 2, NULL, NULL, NULL, 'workStep:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8287951873357908072, 1552209067446579201, '[0],[1552209067446579201],', '采购细明', 'puordetail_index', 1, NULL, '/puorDetail', 'main/puordetail/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8293588485966810596, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划查看', 'proplan_index_detail', 2, NULL, NULL, NULL, 'proPlan:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8313819116019873398, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表列表', 'workorder_index_list', 2, NULL, NULL, NULL, 'workOrder:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8315301036989682205, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表导出', 'pro_index_export', 2, NULL, NULL, NULL, 'pro:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8342821565086833982, 5000566847705099773, '[0],[1534411830716350466],[5000566847705099773],', '仓库管理删除', 'warehouse_index_delete', 2, NULL, NULL, NULL, 'wareHouse:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 16:37:01', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8381831167975323900, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户列表', 'tenantinfo_index_list', 2, NULL, NULL, NULL, 'tenantInfo:list', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8404132105857398918, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户新增', 'tenantinfo_index_add', 2, NULL, NULL, NULL, 'tenantInfo:add', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8422209827465537355, 1552209067446579201, '[0],[1552209067446579201],', '采购订单', 'puororder_index', 1, NULL, '/puorOrder', 'main/puororder/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8424802293696759825, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料删除', 'suppdata_index_delete', 2, NULL, NULL, NULL, 'suppData:delete', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8426505795261326687, 1534384317233524738, '[0],[1534384317233524738],', '工序路线关系', 'worksteproute_index', 1, 'link', '/workStepRoute', 'main/worksteproute/index', NULL, 'manufacturing', 1, 'N', NULL, NULL, 1, 6, NULL, 0, NULL, NULL, '2022-06-28 14:12:11', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8436218677725644487, 4890328064515383248, '[0],[4890328064515383248],', 'SaaS租户编辑', 'tenantinfo_index_edit', 2, NULL, NULL, NULL, 'tenantInfo:edit', 'ht', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8436831550169549160, 1557931401653895170, '[0],[1557931401653895170],', '客户资料', 'cusinfor_index', 1, 'solution', '/cusInfor', 'main/cusinfor/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8471984269988902242, 6700801806460342017, '[0],[6700801806460342017],', '供应商分类编辑', 'suppsort_index_edit', 2, NULL, NULL, NULL, 'suppSort:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (8477499608364957015, 1534410297847214081, '[0],[1534410297847214081],', '生产计划', 'proplan_index', 1, 'tool', '/proPlan', 'main/proplan/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2022-08-12 11:21:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8494981115107347954, 8477499608364957015, '[0],[1534410297847214081],[8477499608364957015],', '生产计划列表', 'proplan_index_list', 2, NULL, NULL, NULL, 'proPlan:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-21 16:58:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8502207026072268708, 4794640277308881312, '[0],[1557930917148868610],[4794640277308881312],', '供应商分类编辑', 'suppsort_index_edit', 2, NULL, NULL, NULL, 'suppSort:edit', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:13', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8515665128226371022, 5721174223820977430, '[0],[1534411830716350466],[5721174223820977430],', '库存余额新增', 'stockbalance_index_add', 2, NULL, NULL, NULL, 'stockBalance:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-28 16:50:46', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8562547749529140265, 7702400310012060990, '[0],[1534384317233524738],[7702400310012060990],', '工艺路线查看', 'workroute_index_detail', 2, NULL, NULL, NULL, 'workRoute:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:38:34', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8562653372099681846, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表查询', 'workorder_index_page', 2, NULL, NULL, NULL, 'workOrder:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8590002636006568603, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单列表', 'saleorder_index_list', 2, NULL, NULL, NULL, 'saleOrder:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8613714595480779524, 8700710314151240148, '[0],[1557931401653895170],[8700710314151240148],', '客户联系人导出', 'cusperson_index_export', 2, NULL, NULL, NULL, 'cusPerson:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8618504262472492350, 6436044181834026359, '[0],[1534384317233524738],[6436044181834026359],', '产品表查询', 'pro_index_page', 2, NULL, NULL, NULL, 'pro:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:36:37', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8621765388717143178, 8426505795261326687, '[0],[1534384317233524738],[8426505795261326687],', '工序路线关系表新增', 'worksteproute_index_add', 2, NULL, NULL, NULL, 'workStepRoute:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:39:09', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8633823152794346306, 1534411830716350466, '[0],[1534411830716350466],', '出库单', 'invOut_index', 1, 'logout', '/invOut', 'main/invOut/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8655981106868069232, 6134354751483903106, '[0],[1557930917148868610],[6134354751483903106],', ' 供应商资料导出', 'suppdata_index_export', 2, NULL, NULL, NULL, 'suppData:export', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:25:05', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8700710314151240148, 1557931401653895170, '[0],[1557931401653895170],', '联系人', 'cusperson_index', 1, 'team', '/cusPerson', 'main/cusperson/index', NULL, 'crm', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:57', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8775511535879974972, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单查询', 'invIn_index_page', 2, NULL, NULL, NULL, 'invIn:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8833258242949762178, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单编辑', 'invOut_index_edit', 2, NULL, NULL, NULL, 'invOut:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8834719374925492199, 9202857208449608897, '[0],[1534410297847214081],[9202857208449608897],', '任务删除', 'task_index_delete', 2, NULL, NULL, NULL, 'task:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:30', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8868921547741003555, 8633823152794346306, '[0],[1534411830716350466],[8633823152794346306],', '出库单导出', 'invOut_index_export', 2, NULL, NULL, NULL, 'invOut:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:48:27', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8903136733033804117, 8422209827465537355, '[0],[1552209067446579201],[8422209827465537355],', '采购订单查看', 'puororder_index_detail', 2, NULL, NULL, NULL, 'puorOrder:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8931538668430408569, 6507903030541782366, '[0],[1534410297847214081],[6507903030541782366],', '工单表查看', 'workorder_index_detail', 2, NULL, NULL, NULL, 'workOrder:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:18', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8946168826263694974, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明新增', 'puordetail_index_add', 2, NULL, NULL, NULL, 'puorDetail:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8957317986837088422, 0, '[0],', '字段配置', 'fieldconfig_index', 1, 'setting', '/fieldConfig', 'main/fieldconfig/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:46:32', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8959792232643188225, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表查看', 'workordertask_index_detail', 2, NULL, NULL, NULL, 'workOrderTask:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (8973112451110891359, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工导出', 'workreport_index_export', 2, NULL, NULL, NULL, 'workReport:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9022874385071881655, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工查看', 'workreport_index_detail', 2, NULL, NULL, NULL, 'workReport:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9090782745729954273, 8287951873357908072, '[0],[1552209067446579201],[8287951873357908072],', '采购细明列表', 'puordetail_index_list', 2, NULL, NULL, NULL, 'puorDetail:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-01 08:57:53', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9118159149629415380, 5934688735467584877, '[0],[1534410297847214081],[5934688735467584877],', '销售订单导出', 'saleorder_index_export', 2, NULL, NULL, NULL, 'saleOrder:export', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-07-27 11:17:42', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9135927743117002076, 8436831550169549160, '[0],[1557931401653895170],[8436831550169549160],', '客户资料查看', 'cusinfor_index_detail', 2, NULL, NULL, NULL, 'cusInfor:detail', 'crm', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-08-12 11:26:25', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9150731911657480775, 5474507168841280952, '[0],[5474507168841280952],', '自定义编号规则编辑', 'customcode_index_edit', 2, NULL, NULL, NULL, 'customCode:edit', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (9152465645130034198, 5012839659883217263, '[0],[1534411830716350466],[5012839659883217263],', '入库单查看', 'invIn_index_detail', 2, NULL, NULL, NULL, 'invIn:detail', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:49:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9156093698218665129, 8957317986837088422, '[0],[8957317986837088422],', '字段配置列表', 'fieldconfig_index_list', 2, NULL, NULL, NULL, 'fieldConfig:list', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (9173957282399717676, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工删除', 'workreport_index_delete', 2, NULL, NULL, NULL, 'workReport:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9180800170765745954, 6667327679590719676, '[0],[1534410297847214081],[6667327679590719676],', '工单任务关系表新增', 'workordertask_index_add', 2, NULL, NULL, NULL, 'workOrderTask:add', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:44:03', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9202857208449608897, 1534410297847214081, '[0],[1534410297847214081],', '任务', 'task_index', 1, 'share-alt', '/task', 'main/task/index', NULL, 'manufacturing', 1, 'Y', NULL, NULL, 1, 2, NULL, 0, NULL, NULL, '2022-06-08 13:45:54', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9208275275751949837, 7346283523793183379, '[0],[1534410297847214081],[7346283523793183379],', '报工查询', 'workreport_index_page', 2, NULL, NULL, NULL, 'workReport:page', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 13:43:49', 1265476890672672808);
INSERT INTO `sys_menu` VALUES (9222389351831596218, 6527167616560933135, '[0],[1534384317233524738],[6527167616560933135],', '产品类型表删除', 'protype_index_delete', 2, NULL, NULL, NULL, 'proType:delete', 'manufacturing', 0, 'Y', NULL, NULL, 1, 100, NULL, 0, NULL, NULL, '2022-06-08 12:00:45', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
                               `id` bigint(20) NOT NULL COMMENT '主键',
                               `title` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
                               `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
                               `type` tinyint(4) NOT NULL COMMENT '类型（字典 1通知 2公告）',
                               `public_user_id` bigint(20) NOT NULL COMMENT '发布人id',
                               `public_user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发布人姓名',
                               `public_org_id` bigint(20) NULL DEFAULT NULL COMMENT '发布机构id',
                               `public_org_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布机构名称',
                               `public_time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
                               `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '撤回时间',
                               `status` tinyint(4) NOT NULL COMMENT '状态（字典 0草稿 1发布 2撤回 3删除）',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                               `update_user` bigint(20) NULL DEFAULT NULL COMMENT '修改人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_notice_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice_user`;
CREATE TABLE `sys_notice_user`  (
                                    `id` bigint(20) NOT NULL COMMENT '主键',
                                    `notice_id` bigint(20) NOT NULL COMMENT '通知公告id',
                                    `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                    `status` tinyint(4) NOT NULL COMMENT '状态（字典 0未读 1已读）',
                                    `read_time` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户数据范围表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_oauth_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_user`;
CREATE TABLE `sys_oauth_user`  (
                                   `id` bigint(20) NOT NULL COMMENT '主键',
                                   `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '第三方平台的用户唯一id',
                                   `access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户授权的token',
                                   `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
                                   `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
                                   `blog` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户网址',
                                   `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所在公司',
                                   `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '位置',
                                   `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                                   `gender` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
                                   `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户来源',
                                   `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户备注（各平台中的用户个人介绍）',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                   `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
                                   `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                   `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '第三方认证用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_oauth_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_op_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_op_log`;
CREATE TABLE `sys_op_log`  (
                               `id` bigint(20) NOT NULL COMMENT '主键',
                               `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
                               `op_type` tinyint(4) NULL DEFAULT NULL COMMENT '操作类型',
                               `success` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否执行成功（Y-是，N-否）',
                               `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '具体消息',
                               `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip',
                               `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
                               `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
                               `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
                               `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求地址',
                               `class_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类名称',
                               `method_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法名称',
                               `req_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式（GET POST PUT DELETE)',
                               `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
                               `result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回结果',
                               `op_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
                               `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作账号',
                               `sign_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签名数据（除ID外）',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统操作日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `pid` bigint(20) NOT NULL COMMENT '父id',
                            `pids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父ids',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                            `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                            `sort` int(11) NOT NULL COMMENT '排序',
                            `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
                            `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统组织机构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO `sys_org` VALUES (1265476890651701250, 0, '[0],', '华夏集团', 'hxjt', 100, '华夏集团总公司', 0, '2020-03-26 16:50:53', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672769, 1265476890651701250, '[0],[1265476890651701250],', '华夏集团乌鲁木齐分公司', 'hxjt_wlmq', 100, '华夏集团乌鲁木齐分公司', 0, '2020-03-26 16:55:42', 1265476890672672808, '2021-12-16 20:29:38', 1265476890672672808);
INSERT INTO `sys_org` VALUES (1265476890672672770, 1265476890651701250, '[0],[1265476890651701250],', '华夏集团成都分公司', 'hxjt_cd', 100, '华夏集团成都分公司', 0, '2020-03-26 16:56:02', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672771, 1265476890672672769, '[0],[1265476890651701250],[1265476890672672769],', '研发部', 'hxjt_wlmq_yfb', 100, '华夏集团乌鲁木齐分公司研发部', 0, '2020-03-26 16:56:36', 1265476890672672808, '2021-12-16 20:29:38', 1265476890672672808);
INSERT INTO `sys_org` VALUES (1265476890672672772, 1265476890672672769, '[0],[1265476890651701250],[1265476890672672769],', '企划部', 'hxjt_wlmq_qhb', 100, '华夏集团乌鲁木齐分公司企划部', 0, '2020-03-26 16:57:06', 1265476890672672808, '2021-12-16 20:29:38', 1265476890672672808);
INSERT INTO `sys_org` VALUES (1265476890672672773, 1265476890672672770, '[0],[1265476890651701250],[1265476890672672770],', '市场部', 'hxjt_cd_scb', 100, '华夏集团成都分公司市场部', 0, '2020-03-26 16:57:35', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672774, 1265476890672672770, '[0],[1265476890651701250],[1265476890672672770],', '财务部', 'hxjt_cd_cwb', 100, '华夏集团成都分公司财务部', 0, '2020-03-26 16:58:01', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_org` VALUES (1265476890672672775, 1265476890672672773, '[0],[1265476890651701250],[1265476890672672770],[1265476890672672773],', '市场部二部', 'hxjt_cd_scb_2b', 100, '华夏集团成都分公司市场部二部', 0, '2020-04-06 15:36:50', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_pos
-- ----------------------------
DROP TABLE IF EXISTS `sys_pos`;
CREATE TABLE `sys_pos`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                            `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                            `sort` int(11) NOT NULL COMMENT '排序',
                            `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                            `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                            PRIMARY KEY (`id`) USING BTREE,
                            UNIQUE INDEX `CODE_UNI`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统职位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_pos
-- ----------------------------
INSERT INTO `sys_pos` VALUES (1265476890672672787, '总经理', 'zjl', 100, '总经理职位', 0, '2020-03-26 19:28:54', 1265476890672672808, '2020-06-02 21:01:04', 1265476890672672808);
INSERT INTO `sys_pos` VALUES (1265476890672672788, '副总经理', 'fzjl', 100, '副总经理职位', 0, '2020-03-26 19:29:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_pos` VALUES (1265476890672672789, '部门经理', 'bmjl', 100, '部门经理职位', 0, '2020-03-26 19:31:49', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_pos` VALUES (1265476890672672790, '工作人员', 'gzry', 100, '工作人员职位', 0, '2020-05-27 11:32:00', 1265476890672672808, '2020-06-01 10:51:35', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` bigint(20) NOT NULL COMMENT '主键id',
                             `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                             `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
                             `sort` int(11) NOT NULL COMMENT '序号',
                             `data_scope_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '数据范围类型（字典 1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据）',
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1停用 2删除）',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1265476890672672817, '组织架构管理员', 'ent_manager_role', 100, 1, '组织架构管理员', 0, '2020-04-02 19:27:26', 1265476890672672808, '2022-06-30 15:16:03', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1265476890672672818, '权限管理员', 'auth_role', 101, 1, '权限管理员', 0, '2020-04-02 19:28:40', 1265476890672672808, '2022-06-30 15:16:26', 1265476890672672808);
INSERT INTO `sys_role` VALUES (1551458068714385410, '测试用户', 'test_user', 100, 1, '测试用户', 0, '2022-07-25 14:43:23', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_data_scope`;
CREATE TABLE `sys_role_data_scope`  (
                                        `id` bigint(20) NOT NULL COMMENT '主键',
                                        `role_id` bigint(20) NOT NULL COMMENT '角色id',
                                        `org_id` bigint(20) NOT NULL COMMENT '机构id',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色数据范围表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_data_scope
-- ----------------------------
INSERT INTO `sys_role_data_scope` VALUES (1292060127645429762, 1265476890672672819, 1265476890672672774);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `id` bigint(20) NOT NULL COMMENT '主键',
                                  `role_id` bigint(20) NOT NULL COMMENT '角色id',
                                  `menu_id` bigint(20) NOT NULL COMMENT '菜单id',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1542410592785072129, 1265476890672672817, 1264622039642255331);
INSERT INTO `sys_role_menu` VALUES (1542410592818626561, 1265476890672672817, 1264622039642255311);
INSERT INTO `sys_role_menu` VALUES (1542410592839598081, 1265476890672672817, 1264622039642255321);
INSERT INTO `sys_role_menu` VALUES (1542410592868958210, 1265476890672672817, 1264622039642255361);
INSERT INTO `sys_role_menu` VALUES (1542410592885735425, 1265476890672672817, 1264622039642255351);
INSERT INTO `sys_role_menu` VALUES (1542410592919289857, 1265476890672672817, 1264622039642255341);
INSERT INTO `sys_role_menu` VALUES (1542410592936067073, 1265476890672672817, 1264622039642255371);
INSERT INTO `sys_role_menu` VALUES (1542410592961232898, 1265476890672672817, 1264622039642255381);
INSERT INTO `sys_role_menu` VALUES (1542410592982204418, 1265476890672672817, 1264622039642255391);
INSERT INTO `sys_role_menu` VALUES (1542410593007370242, 1265476890672672817, 1264622039642255401);
INSERT INTO `sys_role_menu` VALUES (1542410593032536065, 1265476890672672817, 1264622039642255411);
INSERT INTO `sys_role_menu` VALUES (1542410593053507586, 1265476890672672817, 1264622039642255421);
INSERT INTO `sys_role_menu` VALUES (1542410593103839234, 1265476890672672817, 1264622039642255431);
INSERT INTO `sys_role_menu` VALUES (1542410593129005058, 1265476890672672817, 1264622039642255441);
INSERT INTO `sys_role_menu` VALUES (1542410593154170882, 1265476890672672817, 1264622039642255451);
INSERT INTO `sys_role_menu` VALUES (1542410593179336706, 1265476890672672817, 1264622039642255461);
INSERT INTO `sys_role_menu` VALUES (1542410593204502529, 1265476890672672817, 1264622039642255471);
INSERT INTO `sys_role_menu` VALUES (1542410593225474049, 1265476890672672817, 1264622039642255481);
INSERT INTO `sys_role_menu` VALUES (1542410593246445570, 1265476890672672817, 1264622039642255491);
INSERT INTO `sys_role_menu` VALUES (1542410593271611394, 1265476890672672817, 1264622039642255501);
INSERT INTO `sys_role_menu` VALUES (1542410593296777217, 1265476890672672817, 1264622039642255511);
INSERT INTO `sys_role_menu` VALUES (1542410593309360129, 1265476890672672817, 1264622039642255531);
INSERT INTO `sys_role_menu` VALUES (1542410593342914561, 1265476890672672817, 1264622039642255521);
INSERT INTO `sys_role_menu` VALUES (1542410593363886082, 1265476890672672817, 1264622039642255541);
INSERT INTO `sys_role_menu` VALUES (1542410593389051906, 1265476890672672817, 1264622039642255551);
INSERT INTO `sys_role_menu` VALUES (1542410593414217730, 1265476890672672817, 1264622039642255561);
INSERT INTO `sys_role_menu` VALUES (1542410593439383553, 1265476890672672817, 1264622039642255571);
INSERT INTO `sys_role_menu` VALUES (1542410593456160769, 1265476890672672817, 1264622039642255581);
INSERT INTO `sys_role_menu` VALUES (1542410593502298114, 1265476890672672817, 1264622039642255591);
INSERT INTO `sys_role_menu` VALUES (1542410593519075329, 1265476890672672817, 1264622039642255621);
INSERT INTO `sys_role_menu` VALUES (1542410593544241154, 1265476890672672817, 1264622039642255601);
INSERT INTO `sys_role_menu` VALUES (1542410593590378497, 1265476890672672817, 1264622039642255631);
INSERT INTO `sys_role_menu` VALUES (1542410593611350017, 1265476890672672817, 1264622039642255641);
INSERT INTO `sys_role_menu` VALUES (1542410593636515842, 1265476890672672817, 1264622039642255651);
INSERT INTO `sys_role_menu` VALUES (1542410593661681666, 1265476890672672817, 1264622039642255661);
INSERT INTO `sys_role_menu` VALUES (1542410593682653186, 1265476890672672817, 1264622039642255611);
INSERT INTO `sys_role_menu` VALUES (1542410593699430401, 1265476890672672817, 1264622039642255911);
INSERT INTO `sys_role_menu` VALUES (1542410593720401921, 1265476890672672817, 7639361031662168624);
INSERT INTO `sys_role_menu` VALUES (1542410593737179138, 1265476890672672817, 6507903030541782366);
INSERT INTO `sys_role_menu` VALUES (1542410593770733569, 1265476890672672817, 8562653372099681846);
INSERT INTO `sys_role_menu` VALUES (1542410593787510785, 1265476890672672817, 4830986669641223230);
INSERT INTO `sys_role_menu` VALUES (1542410593812676609, 1265476890672672817, 7974141988632466544);
INSERT INTO `sys_role_menu` VALUES (1542410593829453825, 1265476890672672817, 8313819116019873398);
INSERT INTO `sys_role_menu` VALUES (1542410593854619650, 1265476890672672817, 8931538668430408569);
INSERT INTO `sys_role_menu` VALUES (1542410593879785473, 1265476890672672817, 6122595225588020183);
INSERT INTO `sys_role_menu` VALUES (1542410593900756993, 1265476890672672817, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1542410593934311426, 1265476890672672817, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1542410593951088642, 1265476890672672817, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1542410593972060161, 1265476890672672817, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1542410593997225985, 1265476890672672817, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1542410594022391809, 1265476890672672817, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1542410594039169025, 1265476890672672817, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1542410594064334850, 1265476890672672817, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1542410594081112066, 1265476890672672817, 8959792232643188225);
INSERT INTO `sys_role_menu` VALUES (1542410594102083585, 1265476890672672817, 6667327679590719676);
INSERT INTO `sys_role_menu` VALUES (1542410594123055105, 1265476890672672817, 5668705457330168859);
INSERT INTO `sys_role_menu` VALUES (1542410594139832321, 1265476890672672817, 9180800170765745954);
INSERT INTO `sys_role_menu` VALUES (1542410594169192449, 1265476890672672817, 5327083612717101630);
INSERT INTO `sys_role_menu` VALUES (1542410594248884225, 1265476890672672817, 6329171771009724483);
INSERT INTO `sys_role_menu` VALUES (1542410594282438657, 1265476890672672817, 6020421073902938241);
INSERT INTO `sys_role_menu` VALUES (1542410594311798786, 1265476890672672817, 5886355131876064420);
INSERT INTO `sys_role_menu` VALUES (1542410594378907649, 1265476890672672817, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1542410594412462081, 1265476890672672817, 9173957282399717676);
INSERT INTO `sys_role_menu` VALUES (1542410594429239297, 1265476890672672817, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1542410594454405122, 1265476890672672817, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1542410594471182338, 1265476890672672817, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1542410594496348162, 1265476890672672817, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1542410594513125377, 1265476890672672817, 5378908852735764919);
INSERT INTO `sys_role_menu` VALUES (1542410594559262722, 1265476890672672817, 6449912099524079616);
INSERT INTO `sys_role_menu` VALUES (1542410594584428546, 1265476890672672817, 4998978684524443692);
INSERT INTO `sys_role_menu` VALUES (1542410594609594370, 1265476890672672817, 1534384317233524738);
INSERT INTO `sys_role_menu` VALUES (1542410594634760194, 1265476890672672817, 5707993274387362828);
INSERT INTO `sys_role_menu` VALUES (1542410594651537410, 1265476890672672817, 8275137972166495794);
INSERT INTO `sys_role_menu` VALUES (1542410594672508929, 1265476890672672817, 6249597959063106635);
INSERT INTO `sys_role_menu` VALUES (1542410594685091842, 1265476890672672817, 6905244625723471705);
INSERT INTO `sys_role_menu` VALUES (1542410594710257665, 1265476890672672817, 6709768906758836391);
INSERT INTO `sys_role_menu` VALUES (1542410594731229186, 1265476890672672817, 7388361180917918171);
INSERT INTO `sys_role_menu` VALUES (1542410594748006401, 1265476890672672817, 6570253563256899359);
INSERT INTO `sys_role_menu` VALUES (1542410594781560833, 1265476890672672817, 6527167616560933135);
INSERT INTO `sys_role_menu` VALUES (1542410594806726658, 1265476890672672817, 6547301851685353031);
INSERT INTO `sys_role_menu` VALUES (1542410594836086786, 1265476890672672817, 8197158197771689059);
INSERT INTO `sys_role_menu` VALUES (1542410594848669698, 1265476890672672817, 9222389351831596218);
INSERT INTO `sys_role_menu` VALUES (1542410594882224130, 1265476890672672817, 5142735407151148234);
INSERT INTO `sys_role_menu` VALUES (1542410594899001345, 1265476890672672817, 7621956982866257638);
INSERT INTO `sys_role_menu` VALUES (1542410594932555777, 1265476890672672817, 5553078347948343545);
INSERT INTO `sys_role_menu` VALUES (1542410594957721602, 1265476890672672817, 8315301036989682205);
INSERT INTO `sys_role_menu` VALUES (1542410594982887426, 1265476890672672817, 6436044181834026359);
INSERT INTO `sys_role_menu` VALUES (1542410595008053250, 1265476890672672817, 8618504262472492350);
INSERT INTO `sys_role_menu` VALUES (1542410595024830465, 1265476890672672817, 7950819887425681478);
INSERT INTO `sys_role_menu` VALUES (1542410595049996289, 1265476890672672817, 4832107133629024589);
INSERT INTO `sys_role_menu` VALUES (1542410595075162114, 1265476890672672817, 7946795106595000695);
INSERT INTO `sys_role_menu` VALUES (1542410595091939329, 1265476890672672817, 5279583233581044419);
INSERT INTO `sys_role_menu` VALUES (1542410595108716546, 1265476890672672817, 6845666717193712873);
INSERT INTO `sys_role_menu` VALUES (1542410595125493762, 1265476890672672817, 5243613315131080710);
INSERT INTO `sys_role_menu` VALUES (1542410595142270977, 1265476890672672817, 7702400310012060990);
INSERT INTO `sys_role_menu` VALUES (1542410595167436802, 1265476890672672817, 5079108238558434584);
INSERT INTO `sys_role_menu` VALUES (1542410595184214017, 1265476890672672817, 8562547749529140265);
INSERT INTO `sys_role_menu` VALUES (1542410595217768450, 1265476890672672817, 5788080164048630938);
INSERT INTO `sys_role_menu` VALUES (1542410595234545665, 1265476890672672817, 7936411315868128942);
INSERT INTO `sys_role_menu` VALUES (1542410595259711489, 1265476890672672817, 7940657177277675482);
INSERT INTO `sys_role_menu` VALUES (1542410595276488706, 1265476890672672817, 6194217070047486945);
INSERT INTO `sys_role_menu` VALUES (1542410595301654530, 1265476890672672817, 6103813152661113624);
INSERT INTO `sys_role_menu` VALUES (1542410595331014658, 1265476890672672817, 8426505795261326687);
INSERT INTO `sys_role_menu` VALUES (1542410595368763394, 1265476890672672817, 6374585520258378360);
INSERT INTO `sys_role_menu` VALUES (1542410595389734913, 1265476890672672817, 8621765388717143178);
INSERT INTO `sys_role_menu` VALUES (1542410595414900737, 1265476890672672817, 6116805160244737678);
INSERT INTO `sys_role_menu` VALUES (1542410595440066561, 1265476890672672817, 6860312372874164125);
INSERT INTO `sys_role_menu` VALUES (1542410595473620994, 1265476890672672817, 5820472386138045917);
INSERT INTO `sys_role_menu` VALUES (1542410595498786818, 1265476890672672817, 8181822590852115190);
INSERT INTO `sys_role_menu` VALUES (1542410595523952642, 1265476890672672817, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1542410595540729858, 1265476890672672817, 1542409869531873282);
INSERT INTO `sys_role_menu` VALUES (1542410595570089985, 1265476890672672817, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1542410595599450114, 1265476890672672817, 1264622039642255851);
INSERT INTO `sys_role_menu` VALUES (1542410595624615937, 1265476890672672817, 1264622039642255671);
INSERT INTO `sys_role_menu` VALUES (1549008510768009218, 1265476890672672818, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1549008510768009219, 1265476890672672818, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1549008510768009220, 1265476890672672818, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1549008510768009221, 1265476890672672818, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1549008510768009222, 1265476890672672818, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1549008510768009223, 1265476890672672818, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1549008510768009224, 1265476890672672818, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1549008510768009225, 1265476890672672818, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1549008510768009226, 1265476890672672818, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1551481638618296321, 1551458068714385410, 1538776469361139714);
INSERT INTO `sys_role_menu` VALUES (1551481638618296322, 1551458068714385410, 1264622039642256441);
INSERT INTO `sys_role_menu` VALUES (1551481638689599489, 1551458068714385410, 1264622039642256431);
INSERT INTO `sys_role_menu` VALUES (1551481638760902657, 1551458068714385410, 1264622039642256421);
INSERT INTO `sys_role_menu` VALUES (1551481638760902658, 1551458068714385410, 1264622039642256451);
INSERT INTO `sys_role_menu` VALUES (1551481638832205825, 1551458068714385410, 1264622039642256461);
INSERT INTO `sys_role_menu` VALUES (1551481638899314690, 1551458068714385410, 1264622039642256471);
INSERT INTO `sys_role_menu` VALUES (1551481638970617857, 1551458068714385410, 1264622039642256481);
INSERT INTO `sys_role_menu` VALUES (1551481639037726722, 1551458068714385410, 1264622039642256491);
INSERT INTO `sys_role_menu` VALUES (1551481639037726723, 1551458068714385410, 1264622039642256501);
INSERT INTO `sys_role_menu` VALUES (1551481639180333058, 1551458068714385410, 1264622039642256511);
INSERT INTO `sys_role_menu` VALUES (1551481639180333059, 1551458068714385410, 1264622039642256541);
INSERT INTO `sys_role_menu` VALUES (1551481639251636226, 1551458068714385410, 1264622039642256531);
INSERT INTO `sys_role_menu` VALUES (1551481639322939394, 1551458068714385410, 1264622039642256521);
INSERT INTO `sys_role_menu` VALUES (1551481639390048257, 1551458068714385410, 1264622039642256551);
INSERT INTO `sys_role_menu` VALUES (1551481639390048258, 1551458068714385410, 1264622039642256561);
INSERT INTO `sys_role_menu` VALUES (1551481639461351426, 1551458068714385410, 1264622039642256571);
INSERT INTO `sys_role_menu` VALUES (1551481639528460289, 1551458068714385410, 1264622039642256581);
INSERT INTO `sys_role_menu` VALUES (1551481639666872321, 1551458068714385410, 1264622039642256591);
INSERT INTO `sys_role_menu` VALUES (1551481639733981185, 1551458068714385410, 1264622039642256601);
INSERT INTO `sys_role_menu` VALUES (1551481639801090050, 1551458068714385410, 1410859007809736705);
INSERT INTO `sys_role_menu` VALUES (1551481639872393218, 1551458068714385410, 6570253563256899359);
INSERT INTO `sys_role_menu` VALUES (1551481639914336257, 1551458068714385410, 6527167616560933135);
INSERT INTO `sys_role_menu` VALUES (1551481640014999554, 1551458068714385410, 1534384317233524738);
INSERT INTO `sys_role_menu` VALUES (1551481640082108417, 1551458068714385410, 1551480636561321985);
INSERT INTO `sys_role_menu` VALUES (1551481640149217281, 1551458068714385410, 6547301851685353031);
INSERT INTO `sys_role_menu` VALUES (1551481640220520449, 1551458068714385410, 8197158197771689059);
INSERT INTO `sys_role_menu` VALUES (1551481640287629314, 1551458068714385410, 9222389351831596218);
INSERT INTO `sys_role_menu` VALUES (1551481640358932482, 1551458068714385410, 5142735407151148234);
INSERT INTO `sys_role_menu` VALUES (1551481640426041346, 1551458068714385410, 7621956982866257638);
INSERT INTO `sys_role_menu` VALUES (1551481640497344513, 1551458068714385410, 5553078347948343545);
INSERT INTO `sys_role_menu` VALUES (1551481640497344514, 1551458068714385410, 6436044181834026359);
INSERT INTO `sys_role_menu` VALUES (1551481640497344515, 1551458068714385410, 8315301036989682205);
INSERT INTO `sys_role_menu` VALUES (1551481640564453378, 1551458068714385410, 8618504262472492350);
INSERT INTO `sys_role_menu` VALUES (1551481640635756546, 1551458068714385410, 7950819887425681478);
INSERT INTO `sys_role_menu` VALUES (1551481640635756547, 1551458068714385410, 4832107133629024589);
INSERT INTO `sys_role_menu` VALUES (1551481640702865409, 1551458068714385410, 7946795106595000695);
INSERT INTO `sys_role_menu` VALUES (1551481640702865410, 1551458068714385410, 5279583233581044419);
INSERT INTO `sys_role_menu` VALUES (1551481640845471745, 1551458068714385410, 6845666717193712873);
INSERT INTO `sys_role_menu` VALUES (1551481640916774914, 1551458068714385410, 4998978684524443692);
INSERT INTO `sys_role_menu` VALUES (1551481640916774915, 1551458068714385410, 6449912099524079616);
INSERT INTO `sys_role_menu` VALUES (1551481640988078082, 1551458068714385410, 5707993274387362828);
INSERT INTO `sys_role_menu` VALUES (1551481641055186946, 1551458068714385410, 8275137972166495794);
INSERT INTO `sys_role_menu` VALUES (1551481641130684417, 1551458068714385410, 6249597959063106635);
INSERT INTO `sys_role_menu` VALUES (1551481641193598978, 1551458068714385410, 6905244625723471705);
INSERT INTO `sys_role_menu` VALUES (1551481641269096449, 1551458068714385410, 6709768906758836391);
INSERT INTO `sys_role_menu` VALUES (1551481641336205313, 1551458068714385410, 7388361180917918171);
INSERT INTO `sys_role_menu` VALUES (1551481641336205314, 1551458068714385410, 7702400310012060990);
INSERT INTO `sys_role_menu` VALUES (1551481641407508482, 1551458068714385410, 5243613315131080710);
INSERT INTO `sys_role_menu` VALUES (1551481641474617346, 1551458068714385410, 5079108238558434584);
INSERT INTO `sys_role_menu` VALUES (1551481641474617347, 1551458068714385410, 8562547749529140265);
INSERT INTO `sys_role_menu` VALUES (1551481641545920514, 1551458068714385410, 5788080164048630938);
INSERT INTO `sys_role_menu` VALUES (1551481641613029378, 1551458068714385410, 7936411315868128942);
INSERT INTO `sys_role_menu` VALUES (1551481641613029379, 1551458068714385410, 7940657177277675482);
INSERT INTO `sys_role_menu` VALUES (1551481641688526849, 1551458068714385410, 6194217070047486945);
INSERT INTO `sys_role_menu` VALUES (1551481641751441410, 1551458068714385410, 8426505795261326687);
INSERT INTO `sys_role_menu` VALUES (1551481641826938881, 1551458068714385410, 6103813152661113624);
INSERT INTO `sys_role_menu` VALUES (1551481641826938882, 1551458068714385410, 6374585520258378360);
INSERT INTO `sys_role_menu` VALUES (1551481641894047745, 1551458068714385410, 8621765388717143178);
INSERT INTO `sys_role_menu` VALUES (1551481641965350913, 1551458068714385410, 6116805160244737678);
INSERT INTO `sys_role_menu` VALUES (1551481641965350914, 1551458068714385410, 6860312372874164125);
INSERT INTO `sys_role_menu` VALUES (1551481642036654082, 1551458068714385410, 5820472386138045917);
INSERT INTO `sys_role_menu` VALUES (1551481642103762946, 1551458068714385410, 8181822590852115190);
INSERT INTO `sys_role_menu` VALUES (1551481642103762947, 1551458068714385410, 7639361031662168624);
INSERT INTO `sys_role_menu` VALUES (1551481642179260418, 1551458068714385410, 6507903030541782366);
INSERT INTO `sys_role_menu` VALUES (1551481642242174977, 1551458068714385410, 1534410297847214081);
INSERT INTO `sys_role_menu` VALUES (1551481642242174978, 1551458068714385410, 8562653372099681846);
INSERT INTO `sys_role_menu` VALUES (1551481642313478146, 1551458068714385410, 4830986669641223230);
INSERT INTO `sys_role_menu` VALUES (1551481642313478147, 1551458068714385410, 7974141988632466544);
INSERT INTO `sys_role_menu` VALUES (1551481642384781313, 1551458068714385410, 8313819116019873398);
INSERT INTO `sys_role_menu` VALUES (1551481642451890178, 1551458068714385410, 8931538668430408569);
INSERT INTO `sys_role_menu` VALUES (1551481642451890179, 1551458068714385410, 6122595225588020183);
INSERT INTO `sys_role_menu` VALUES (1551481642523193345, 1551458068714385410, 9202857208449608897);
INSERT INTO `sys_role_menu` VALUES (1551481642594496513, 1551458068714385410, 7656571819576826411);
INSERT INTO `sys_role_menu` VALUES (1551481642594496514, 1551458068714385410, 5014700545593967407);
INSERT INTO `sys_role_menu` VALUES (1551481642661605377, 1551458068714385410, 8088901678692869692);
INSERT INTO `sys_role_menu` VALUES (1551481642661605378, 1551458068714385410, 6299920358694835309);
INSERT INTO `sys_role_menu` VALUES (1551481642732908545, 1551458068714385410, 5642803668063215805);
INSERT INTO `sys_role_menu` VALUES (1551481642804211714, 1551458068714385410, 8834719374925492199);
INSERT INTO `sys_role_menu` VALUES (1551481642804211715, 1551458068714385410, 7882898168203081196);
INSERT INTO `sys_role_menu` VALUES (1551481642871320578, 1551458068714385410, 6667327679590719676);
INSERT INTO `sys_role_menu` VALUES (1551481642871320579, 1551458068714385410, 8959792232643188225);
INSERT INTO `sys_role_menu` VALUES (1551481642942623746, 1551458068714385410, 5668705457330168859);
INSERT INTO `sys_role_menu` VALUES (1551481642942623747, 1551458068714385410, 9180800170765745954);
INSERT INTO `sys_role_menu` VALUES (1551481643081035777, 1551458068714385410, 5327083612717101630);
INSERT INTO `sys_role_menu` VALUES (1551481643081035778, 1551458068714385410, 6329171771009724483);
INSERT INTO `sys_role_menu` VALUES (1551481643156533250, 1551458068714385410, 6020421073902938241);
INSERT INTO `sys_role_menu` VALUES (1551481643223642113, 1551458068714385410, 5886355131876064420);
INSERT INTO `sys_role_menu` VALUES (1551481643223642114, 1551458068714385410, 7346283523793183379);
INSERT INTO `sys_role_menu` VALUES (1551481643294945281, 1551458068714385410, 1542409869531873282);
INSERT INTO `sys_role_menu` VALUES (1551481643362054146, 1551458068714385410, 9208275275751949837);
INSERT INTO `sys_role_menu` VALUES (1551481643433357313, 1551458068714385410, 9173957282399717676);
INSERT INTO `sys_role_menu` VALUES (1551481643504660482, 1551458068714385410, 8973112451110891359);
INSERT INTO `sys_role_menu` VALUES (1551481643571769345, 1551458068714385410, 5753930915061776016);
INSERT INTO `sys_role_menu` VALUES (1551481643571769346, 1551458068714385410, 7288710125399936914);
INSERT INTO `sys_role_menu` VALUES (1551481643643072513, 1551458068714385410, 9022874385071881655);
INSERT INTO `sys_role_menu` VALUES (1551481643643072514, 1551458068714385410, 5378908852735764919);
INSERT INTO `sys_role_menu` VALUES (1551481643710181377, 1551458068714385410, 8477499608364957015);
INSERT INTO `sys_role_menu` VALUES (1551481643785678849, 1551458068714385410, 5206868747918117433);
INSERT INTO `sys_role_menu` VALUES (1551481643852787714, 1551458068714385410, 4757443910107701590);
INSERT INTO `sys_role_menu` VALUES (1551481643919896578, 1551458068714385410, 5016248121790626902);
INSERT INTO `sys_role_menu` VALUES (1551481643991199745, 1551458068714385410, 7516608184768822750);
INSERT INTO `sys_role_menu` VALUES (1551481644066697218, 1551458068714385410, 8293588485966810596);
INSERT INTO `sys_role_menu` VALUES (1551481644066697219, 1551458068714385410, 5523506981641034218);
INSERT INTO `sys_role_menu` VALUES (1551481644129611778, 1551458068714385410, 8494981115107347954);
INSERT INTO `sys_role_menu` VALUES (1551481644200914946, 1551458068714385410, 9152465645130034198);
INSERT INTO `sys_role_menu` VALUES (1551481644268023809, 1551458068714385410, 5012839659883217263);
INSERT INTO `sys_role_menu` VALUES (1551481644268023810, 1551458068714385410, 1534411830716350466);
INSERT INTO `sys_role_menu` VALUES (1551481644347715585, 1551458068714385410, 6280770118494855200);
INSERT INTO `sys_role_menu` VALUES (1551481644414824450, 1551458068714385410, 8775511535879974972);
INSERT INTO `sys_role_menu` VALUES (1551481644481933313, 1551458068714385410, 5624458298951752771);
INSERT INTO `sys_role_menu` VALUES (1551481644549042178, 1551458068714385410, 7254023492833901715);
INSERT INTO `sys_role_menu` VALUES (1551481644620345345, 1551458068714385410, 7318591877639154878);
INSERT INTO `sys_role_menu` VALUES (1551481644691648513, 1551458068714385410, 7812048842906278623);
INSERT INTO `sys_role_menu` VALUES (1551481644758757378, 1551458068714385410, 8633823152794346306);
INSERT INTO `sys_role_menu` VALUES (1551481644834254849, 1551458068714385410, 5893520734687245059);
INSERT INTO `sys_role_menu` VALUES (1551481644901363714, 1551458068714385410, 8868921547741003555);
INSERT INTO `sys_role_menu` VALUES (1551481644901363715, 1551458068714385410, 6326961474985796183);
INSERT INTO `sys_role_menu` VALUES (1551481644972666881, 1551458068714385410, 8833258242949762178);
INSERT INTO `sys_role_menu` VALUES (1551481645039775746, 1551458068714385410, 4967796364303223216);
INSERT INTO `sys_role_menu` VALUES (1551481645039775747, 1551458068714385410, 6900932255841430981);
INSERT INTO `sys_role_menu` VALUES (1551481645182382082, 1551458068714385410, 7629147488562302699);
INSERT INTO `sys_role_menu` VALUES (1551481645249490946, 1551458068714385410, 8016744771368544956);
INSERT INTO `sys_role_menu` VALUES (1551481645249490947, 1551458068714385410, 5002055413507195458);
INSERT INTO `sys_role_menu` VALUES (1551481645320794114, 1551458068714385410, 6290232610677682017);
INSERT INTO `sys_role_menu` VALUES (1551481645392097281, 1551458068714385410, 5803272132980049305);
INSERT INTO `sys_role_menu` VALUES (1551481645392097282, 1551458068714385410, 7395287038762629813);
INSERT INTO `sys_role_menu` VALUES (1551481645463400450, 1551458068714385410, 7323225129555267277);
INSERT INTO `sys_role_menu` VALUES (1551481645530509314, 1551458068714385410, 6347609996552577764);
INSERT INTO `sys_role_menu` VALUES (1551481645530509315, 1551458068714385410, 7682643462888492777);
INSERT INTO `sys_role_menu` VALUES (1551481645597618178, 1551458068714385410, 6696300230304937737);
INSERT INTO `sys_role_menu` VALUES (1551481645673115649, 1551458068714385410, 8436831550169549160);
INSERT INTO `sys_role_menu` VALUES (1551481645740224514, 1551458068714385410, 7898061431233899837);
INSERT INTO `sys_role_menu` VALUES (1551481645811527682, 1551458068714385410, 5448185299220002627);
INSERT INTO `sys_role_menu` VALUES (1551481645811527683, 1551458068714385410, 9135927743117002076);
INSERT INTO `sys_role_menu` VALUES (1551481645878636546, 1551458068714385410, 6655166482691941995);
INSERT INTO `sys_role_menu` VALUES (1551481646000271361, 1551458068714385410, 5871841993475141582);
INSERT INTO `sys_role_menu` VALUES (1551481646000271362, 1551458068714385410, 5276828216703973340);
INSERT INTO `sys_role_menu` VALUES (1551481646046408706, 1551458068714385410, 7455112058491820064);
INSERT INTO `sys_role_menu` VALUES (1551481646092546050, 1551458068714385410, 8957317986837088422);
INSERT INTO `sys_role_menu` VALUES (1551481646159654913, 1551458068714385410, 4621333945127042674);
INSERT INTO `sys_role_menu` VALUES (1551481646159654914, 1551458068714385410, 5886900280768283007);
INSERT INTO `sys_role_menu` VALUES (1551481646226763778, 1551458068714385410, 5846194751422568093);
INSERT INTO `sys_role_menu` VALUES (1551481646298066945, 1551458068714385410, 9156093698218665129);
INSERT INTO `sys_role_menu` VALUES (1551481646298066946, 1551458068714385410, 7678659259268274624);
INSERT INTO `sys_role_menu` VALUES (1551481646369370113, 1551458068714385410, 8099644559700436461);
INSERT INTO `sys_role_menu` VALUES (1551481646407118849, 1551458068714385410, 8107628912599125262);
INSERT INTO `sys_role_menu` VALUES (1551481646474227714, 1551458068714385410, 7072750871619746982);
INSERT INTO `sys_role_menu` VALUES (1551481646537142274, 1551458068714385410, 4903707486851837034);
INSERT INTO `sys_role_menu` VALUES (1551481646574891009, 1551458068714385410, 7635960329677377171);
INSERT INTO `sys_role_menu` VALUES (1551481646612639746, 1551458068714385410, 7596329328691560085);
INSERT INTO `sys_role_menu` VALUES (1551481646675554306, 1551458068714385410, 6429021782911745716);
INSERT INTO `sys_role_menu` VALUES (1551481646721691650, 1551458068714385410, 8259333771823873238);
INSERT INTO `sys_role_menu` VALUES (1551481646788800513, 1551458068714385410, 4674816360932725215);
INSERT INTO `sys_role_menu` VALUES (1551481646834937857, 1551458068714385410, 5653092812632926489);
INSERT INTO `sys_role_menu` VALUES (1551481646872686594, 1551458068714385410, 5474507168841280952);
INSERT INTO `sys_role_menu` VALUES (1551481646931406849, 1551458068714385410, 7200474546305867050);
INSERT INTO `sys_role_menu` VALUES (1551481646998515714, 1551458068714385410, 7242870236365548592);
INSERT INTO `sys_role_menu` VALUES (1551481647065624577, 1551458068714385410, 9150731911657480775);
INSERT INTO `sys_role_menu` VALUES (1551481647136927746, 1551458068714385410, 7287765970469918079);
INSERT INTO `sys_role_menu` VALUES (1551481647187259394, 1551458068714385410, 5480200211927228567);
INSERT INTO `sys_role_menu` VALUES (1551481647187259395, 1551458068714385410, 4770899845682577376);
INSERT INTO `sys_role_menu` VALUES (1551481647346642945, 1551458068714385410, 6238776219082572290);
INSERT INTO `sys_role_menu` VALUES (1551481647346642946, 1551458068714385410, 8700710314151240148);
INSERT INTO `sys_role_menu` VALUES (1551481647417946113, 1551458068714385410, 8613714595480779524);
INSERT INTO `sys_role_menu` VALUES (1551481647485054978, 1551458068714385410, 5346549595871021092);
INSERT INTO `sys_role_menu` VALUES (1551481647556358146, 1551458068714385410, 8181816557328888214);
INSERT INTO `sys_role_menu` VALUES (1551481647627661314, 1551458068714385410, 5587279777867961498);
INSERT INTO `sys_role_menu` VALUES (1551481647698964481, 1551458068714385410, 7380411400614516902);
INSERT INTO `sys_role_menu` VALUES (1551481647766073346, 1551458068714385410, 7721498005843290304);
INSERT INTO `sys_role_menu` VALUES (1551481647766073347, 1551458068714385410, 1264622039642256641);
INSERT INTO `sys_role_menu` VALUES (1551481647908679681, 1551458068714385410, 1264622039642256621);
INSERT INTO `sys_role_menu` VALUES (1551481647908679682, 1551458068714385410, 1264622039642256611);
INSERT INTO `sys_role_menu` VALUES (1551481647975788545, 1551458068714385410, 1264622039642256651);
INSERT INTO `sys_role_menu` VALUES (1551481647975788546, 1551458068714385410, 1264622039642256661);
INSERT INTO `sys_role_menu` VALUES (1551481648047091713, 1551458068714385410, 1264622039642256671);
INSERT INTO `sys_role_menu` VALUES (1551481648114200577, 1551458068714385410, 1264622039642256681);
INSERT INTO `sys_role_menu` VALUES (1551481648185503746, 1551458068714385410, 1264622039642256691);
INSERT INTO `sys_role_menu` VALUES (1551481648261001218, 1551458068714385410, 1264622039642256701);
INSERT INTO `sys_role_menu` VALUES (1551481648261001219, 1551458068714385410, 1264622039642256711);
INSERT INTO `sys_role_menu` VALUES (1551481648328110082, 1551458068714385410, 1264622039642256631);
INSERT INTO `sys_role_menu` VALUES (1551481648395218946, 1551458068714385410, 1264622039642255361);
INSERT INTO `sys_role_menu` VALUES (1551481648395218947, 1551458068714385410, 1264622039642255591);
INSERT INTO `sys_role_menu` VALUES (1551481648466522113, 1551458068714385410, 1264622039642255351);
INSERT INTO `sys_role_menu` VALUES (1551481648466522114, 1551458068714385410, 1264622039642255341);
INSERT INTO `sys_role_menu` VALUES (1551481648533630977, 1551458068714385410, 1264622039642255521);

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms`  (
                            `id` bigint(20) NOT NULL COMMENT '主键',
                            `phone_numbers` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
                            `validate_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信验证码',
                            `template_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信模板ID',
                            `biz_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回执id，可根据该id查询具体的发送状态',
                            `status` tinyint(4) NOT NULL COMMENT '发送状态（字典 0 未发送，1 发送成功，2 发送失败，3 失效）',
                            `source` tinyint(4) NOT NULL COMMENT '来源（字典 1 app， 2 pc， 3 其他）',
                            `invalid_time` datetime(0) NULL DEFAULT NULL COMMENT '失效时间',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信信息发送表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_sms
-- ----------------------------

-- ----------------------------
-- Table structure for sys_tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_info`;
CREATE TABLE `sys_tenant_info`  (
                                    `id` bigint(20) NOT NULL COMMENT '主键id',
                                    `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户名称',
                                    `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户的编码',
                                    `db_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联的数据库名称',
                                    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                    `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                                    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                    `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '租户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant_info
-- ----------------------------
INSERT INTO `sys_tenant_info` VALUES (1557925323436216321, '总公司 (管理单位)', 'default', 'snowy_tenant_db_default', '2022-08-12 11:01:57', 1265476890672672808, NULL, NULL);
INSERT INTO `sys_tenant_info` VALUES (1557926024803536897, '王1', '768', 'snowy_tenant_db_768', '2022-08-12 11:04:44', 1265476890672672808, NULL, NULL);

-- ----------------------------
-- Table structure for sys_timers
-- ----------------------------
DROP TABLE IF EXISTS `sys_timers`;
CREATE TABLE `sys_timers`  (
                               `id` bigint(20) NOT NULL COMMENT '定时器id',
                               `timer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '任务名称',
                               `action_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行任务的class的类名（实现了TimerTaskRunner接口的类的全称）',
                               `cron` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '定时任务表达式',
                               `job_status` tinyint(4) NULL DEFAULT 0 COMMENT '状态（字典 1运行  2停止）',
                               `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_timers
-- ----------------------------
INSERT INTO `sys_timers` VALUES (1288760324837851137, '定时同步缓存常量', 'vip.xiaonuo.sys.modular.timer.tasks.RefreshConstantsTaskRunner', '0 0/1 * * * ?', 1, '定时同步sys_config表的数据到缓存常量中', '2020-07-30 16:56:20', 1265476890672672808, '2020-07-30 16:58:52', 1265476890672672808);
INSERT INTO `sys_timers` VALUES (1304971718170832898, '定时打印一句话', 'vip.xiaonuo.sys.modular.timer.tasks.SystemOutTaskRunner', '0 0 * * * ? *', 2, '定时打印一句话', '2020-09-13 10:34:37', 1265476890672672808, '2020-09-23 20:37:48', 1265476890672672808);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` bigint(20) NOT NULL COMMENT '主键',
                             `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
                             `pwd_hash_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
                             `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
                             `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
                             `avatar` bigint(20) NULL DEFAULT NULL COMMENT '头像',
                             `birthday` date NULL DEFAULT NULL COMMENT '生日',
                             `sex` tinyint(4) NOT NULL COMMENT '性别(字典 1男 2女 3未知)',
                             `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                             `phone` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
                             `tel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
                             `last_login_ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后登陆IP',
                             `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
                             `admin_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '管理员类型（0超级管理员 1非管理员）',
                             `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态（字典 0正常 1冻结 2删除）',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1265476890672672808, 'superAdmin', '9a6dd62cb20ab38571cfa466e8dd67f5893c81f5e823f66c503534766840e3a9', '超级管理员', '超级管理员', NULL, '2020-03-18', 1, 'superAdmin@qq.com', '001757f43bd02871093cd7cbfed021f5', '1234567890', '127.0.0.1', '2022-08-12 11:30:37', 1, 0, '2020-05-29 16:39:28', -1, '2022-08-12 11:30:37', -1);

-- ----------------------------
-- Table structure for sys_user_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_data_scope`;
CREATE TABLE `sys_user_data_scope`  (
                                        `id` bigint(20) NOT NULL COMMENT '主键',
                                        `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                        `org_id` bigint(20) NOT NULL COMMENT '机构id',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户数据范围表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_data_scope
-- ----------------------------
INSERT INTO `sys_user_data_scope` VALUES (1277459951742840834, 1266277099455635457, 1265476890672672770);
INSERT INTO `sys_user_data_scope` VALUES (1277459952577507330, 1266277099455635457, 1265476890672672773);
INSERT INTO `sys_user_data_scope` VALUES (1277459953424756737, 1266277099455635457, 1265476890672672775);
INSERT INTO `sys_user_data_scope` VALUES (1277459954267811841, 1266277099455635457, 1265476890672672774);
INSERT INTO `sys_user_data_scope` VALUES (1542386685185208321, 1275735541155614721, 1265476890651701250);
INSERT INTO `sys_user_data_scope` VALUES (1542386685248122882, 1275735541155614721, 1265476890672672769);
INSERT INTO `sys_user_data_scope` VALUES (1542386685248122883, 1275735541155614721, 1265476890672672771);
INSERT INTO `sys_user_data_scope` VALUES (1542386685248122884, 1275735541155614721, 1265476890672672772);
INSERT INTO `sys_user_data_scope` VALUES (1542386685315231745, 1275735541155614721, 1265476890672672770);
INSERT INTO `sys_user_data_scope` VALUES (1542386685382340609, 1275735541155614721, 1265476890672672773);
INSERT INTO `sys_user_data_scope` VALUES (1542386685382340610, 1275735541155614721, 1265476890672672775);
INSERT INTO `sys_user_data_scope` VALUES (1542386685449449473, 1275735541155614721, 1265476890672672774);
INSERT INTO `sys_user_data_scope` VALUES (1542404712807006209, 1280709549107552257, 1265476890651701250);
INSERT INTO `sys_user_data_scope` VALUES (1542404712874115073, 1280709549107552257, 1265476890672672769);
INSERT INTO `sys_user_data_scope` VALUES (1542404712874115074, 1280709549107552257, 1265476890672672771);
INSERT INTO `sys_user_data_scope` VALUES (1542404712874115075, 1280709549107552257, 1265476890672672772);
INSERT INTO `sys_user_data_scope` VALUES (1542404712941223937, 1280709549107552257, 1265476890672672770);
INSERT INTO `sys_user_data_scope` VALUES (1542404712941223938, 1280709549107552257, 1265476890672672773);
INSERT INTO `sys_user_data_scope` VALUES (1542404712941223939, 1280709549107552257, 1265476890672672775);
INSERT INTO `sys_user_data_scope` VALUES (1542404713008332802, 1280709549107552257, 1265476890672672774);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1542406846898241538, 1280709549107552257, 1265476890672672817);
INSERT INTO `sys_user_role` VALUES (1542406846961156097, 1280709549107552257, 1265476890672672818);
INSERT INTO `sys_user_role` VALUES (1549008694352695298, 1275735541155614721, 1265476890672672818);
INSERT INTO `sys_user_role` VALUES (1551460683594493953, 1551457178297200641, 1551458068714385410);
INSERT INTO `sys_user_role` VALUES (1551462466786062338, 1551462379850723329, 1551458068714385410);

-- ----------------------------
-- Table structure for sys_vis_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_vis_log`;
CREATE TABLE `sys_vis_log`  (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
    `success` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否执行成功（Y-是，N-否）',
    `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '具体消息',
    `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip',
    `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
    `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
    `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
    `vis_type` tinyint(4) NOT NULL COMMENT '操作类型（字典 1登入 2登出）',
    `vis_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
    `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问账号',
    `sign_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签名数据（除ID外）',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问日志表' ROW_FORMAT = Compact;


SET FOREIGN_KEY_CHECKS = 1;



