package vip.xiaonuo.modular.bigScreen.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class WorkOrderProResult {
    //时间
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date time;
    //工单完成数量
    private Integer workOrderFinishNum;
    //工单产出数量
    private Integer workOrderProNum;
}
