/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.bom.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.bom.param.BomParam;
import vip.xiaonuo.modular.bom.service.BomService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 物料清单控制器
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
@RestController
public class BomController {

    @Resource
    private BomService bomService;

    /**
     * 查询物料清单
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @GetMapping("/bom/page")
    @BusinessLog(title = "物料清单_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(BomParam bomParam) {
        return new SuccessResponseData(bomService.page(bomParam));
    }

    /**
     * 添加物料清单
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @PostMapping("/bom/add")
    @BusinessLog(title = "物料清单_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(BomParam.add.class) BomParam bomParam) {
            bomService.add(bomParam);
        return new SuccessResponseData();
    }

    /**
     * 删除物料清单，可批量删除
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @PostMapping("/bom/delete")
    @BusinessLog(title = "物料清单_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(BomParam.delete.class) List<BomParam> bomParamList) {
            bomService.delete(bomParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑物料清单
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @PostMapping("/bom/edit")
    @BusinessLog(title = "物料清单_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(BomParam.edit.class) BomParam bomParam) {
            bomService.edit(bomParam);
        return new SuccessResponseData();
    }

    /**
     * 查看物料清单
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @GetMapping("/bom/detail")
    @BusinessLog(title = "物料清单_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(BomParam.detail.class) BomParam bomParam) {
        return new SuccessResponseData(bomService.detail(bomParam));
    }

    /**
     * 物料清单列表
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @GetMapping("/bom/list")
    @BusinessLog(title = "物料清单_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(BomParam bomParam) {
        return new SuccessResponseData(bomService.list(bomParam));
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @GetMapping("/bom/export")
    @BusinessLog(title = "物料清单_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(BomParam bomParam) {
        bomService.export(bomParam);
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-08-11 09:15:20
     */
    @Permission
    @GetMapping("/bom/sonList")
    @BusinessLog(title = "物料清单_查找子项", opType = LogAnnotionOpTypeEnum.OTHER)
    public ResponseData sonList(BomParam bomParam) {
        return new SuccessResponseData(bomService.sonList(bomParam));
    }


}
