/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.cusperson.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.cusperson.Result.CusPersonResult;
import vip.xiaonuo.modular.cusperson.entity.CusPerson;
import vip.xiaonuo.modular.cusperson.enums.CusPersonEnum;
import vip.xiaonuo.modular.cusperson.enums.CusPersonExceptionEnum;
import vip.xiaonuo.modular.cusperson.mapper.CusPersonMapper;
import vip.xiaonuo.modular.cusperson.param.CusPersonParam;
import vip.xiaonuo.modular.cusperson.service.CusPersonService;

import java.util.List;

/**
 * 客户联系人service接口实现类
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
@Service
public class CusPersonServiceImpl extends ServiceImpl<CusPersonMapper, CusPerson> implements CusPersonService {

    @Override
    public PageResult<CusPersonResult> page(CusPersonParam cusPersonParam) {
        QueryWrapper<CusPersonResult> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(cusPersonParam)) {

            // 根据联系人 查询
            if (ObjectUtil.isNotEmpty(cusPersonParam.getCusPerson())) {
                queryWrapper.lambda().like(CusPerson::getCusPerson, cusPersonParam.getCusPerson());
            }
            // 根据联系电话 查询
            if (ObjectUtil.isNotEmpty(cusPersonParam.getCusPhone())) {
                queryWrapper.lambda().like(CusPerson::getCusPhone, cusPersonParam.getCusPhone());
            }
            // 根据备注信息 查询
            if (ObjectUtil.isNotEmpty(cusPersonParam.getRemark())) {
                queryWrapper.like("n.remark", cusPersonParam.getRemark());
            }
        }
        queryWrapper.orderByDesc("n.create_time");
        return new PageResult<>(baseMapper.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CusPerson> list(CusPersonParam cusPersonParam) {
        return this.list();
    }

    @Override
    public void add(CusPersonParam cusPersonParam) {
        // 唯一主联系人校验
        checkParam(cusPersonParam);
        CusPerson cusPerson = new CusPerson();
        BeanUtil.copyProperties(cusPersonParam, cusPerson);
        this.save(cusPerson);
    }

    private void checkParam(CusPersonParam cusPersonParam) {
        // 不是主联系人 return
        if (CusPersonEnum.NO_MAIN_CONTACTS.getCode().equals(cusPersonParam.getMaiorCusPerson())){
            return;
        }
        QueryWrapper<CusPerson> cusPersonQueryWrapper = new QueryWrapper<>();
        cusPersonQueryWrapper.lambda().eq(CusPerson::getCusInforId,cusPersonParam.getCusInforId());
        List<CusPerson> cusPersonList = this.list(cusPersonQueryWrapper);
        cusPersonList.forEach(cusPerson->{
            if (CusPersonEnum.MAIN_CONTACTS.getCode().equals(cusPerson.getMaiorCusPerson())){
                throw new ServiceException(CusPersonExceptionEnum.NOLY_MAIN_CONTACTS);
            }
        });

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CusPersonParam> cusPersonParamList) {
        cusPersonParamList.forEach(cusPersonParam -> {
            this.removeById(cusPersonParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CusPersonParam cusPersonParam) {
        checkParam(cusPersonParam);
        CusPerson cusPerson = this.queryCusPerson(cusPersonParam);
        BeanUtil.copyProperties(cusPersonParam, cusPerson);
        this.updateById(cusPerson);
    }

    @Override
    public CusPerson detail(CusPersonParam cusPersonParam) {
        return this.queryCusPerson(cusPersonParam);
    }

    /**
     * 获取客户联系人
     *
     * @author czw
     * @date 2022-07-20 12:28:28
     */
    private CusPerson queryCusPerson(CusPersonParam cusPersonParam) {
        CusPerson cusPerson = this.getById(cusPersonParam.getId());
        if (ObjectUtil.isNull(cusPerson)) {
            throw new ServiceException(CusPersonExceptionEnum.NOT_EXIST);
        }
        return cusPerson;
    }

    @Override
    public void export(CusPersonParam cusPersonParam) {
        List<CusPerson> list = this.list(cusPersonParam);
        PoiUtil.exportExcelWithStream("SnowyCusPerson.xls", CusPerson.class, list);
    }

}