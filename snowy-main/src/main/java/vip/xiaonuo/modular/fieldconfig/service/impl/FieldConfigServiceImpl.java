/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.fieldconfig.service.impl;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.math3.geometry.spherical.oned.ArcsSet;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.fieldconfig.entity.FieldConfig;
import vip.xiaonuo.modular.fieldconfig.enums.FieldConfigExceptionEnum;
import vip.xiaonuo.modular.fieldconfig.mapper.FieldConfigMapper;
import vip.xiaonuo.modular.fieldconfig.param.FieldConfigParam;
import vip.xiaonuo.modular.fieldconfig.service.FieldConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.modular.pro.entity.Pro;
import vip.xiaonuo.modular.pro.param.ProParam;
import vip.xiaonuo.modular.protype.entity.ProType;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;

/**
 * 字段配置service接口实现类
 *
 * @author wangpeng
 * @date 2022-06-07 20:42:25
 */
@Service
public class FieldConfigServiceImpl extends ServiceImpl<FieldConfigMapper, FieldConfig> implements FieldConfigService {

    @Override
    public PageResult<FieldConfig> page(FieldConfigParam fieldConfigParam) {
        QueryWrapper<FieldConfig> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(fieldConfigParam)) {

            // 根据字段索引 查询
            if (ObjectUtil.isNotEmpty(fieldConfigParam.getFieldIndex())) {
                queryWrapper.lambda().like(FieldConfig::getFieldIndex, fieldConfigParam.getFieldIndex());
            }
            // 根据字段名称 查询
            if (ObjectUtil.isNotEmpty(fieldConfigParam.getFieldTitle())) {
                queryWrapper.lambda().like(FieldConfig::getFieldTitle, fieldConfigParam.getFieldTitle());
            }
            // 根据字段类型 查询
            if (ObjectUtil.isNotEmpty(fieldConfigParam.getFieldType())) {
                queryWrapper.lambda().eq(FieldConfig::getFieldType, fieldConfigParam.getFieldType());
            }
            // 根据备注信息 查询
            if (ObjectUtil.isNotEmpty(fieldConfigParam.getRemarks())) {
                queryWrapper.lambda().like(FieldConfig::getRemarks, fieldConfigParam.getRemarks());
            }
            // 根据表名称 查询
            if (ObjectUtil.isNotEmpty(fieldConfigParam.getTableName())) {
                queryWrapper.lambda().eq(FieldConfig::getTableName, fieldConfigParam.getTableName());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<FieldConfig> list(FieldConfigParam fieldConfigParam) {
        QueryWrapper<FieldConfig> queryWrapper = new QueryWrapper<>();
        if(ObjectUtil.isNotEmpty(fieldConfigParam)){
            if(ObjectUtil.isNotEmpty(fieldConfigParam.getTableName())){
                queryWrapper.lambda().eq(FieldConfig::getTableName,fieldConfigParam.getTableName());
            }
        }
        return this.list(queryWrapper);
    }

    @Override
    public void add(FieldConfigParam fieldConfigParam) {
        FieldConfig fieldConfig = new FieldConfig();
        checkParam(fieldConfigParam, false);
        BeanUtil.copyProperties(fieldConfigParam, fieldConfig);
        this.save(fieldConfig);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<FieldConfigParam> fieldConfigParamList) {
        fieldConfigParamList.forEach(fieldConfigParam -> {
            this.removeById(fieldConfigParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(FieldConfigParam fieldConfigParam) {
        FieldConfig fieldConfig = this.queryFieldConfig(fieldConfigParam);
        checkParam(fieldConfigParam, true);
        BeanUtil.copyProperties(fieldConfigParam, fieldConfig);
        this.updateById(fieldConfig);
    }

    @Override
    public FieldConfig detail(FieldConfigParam fieldConfigParam) {
        return this.queryFieldConfig(fieldConfigParam);
    }

    /**
     * 获取字段配置
     *
     * @author wangpeng
     * @date 2022-06-07 20:42:25
     */
    private FieldConfig queryFieldConfig(FieldConfigParam fieldConfigParam) {
        FieldConfig fieldConfig = this.getById(fieldConfigParam.getId());
        if (ObjectUtil.isNull(fieldConfig)) {
            throw new ServiceException(FieldConfigExceptionEnum.NOT_EXIST);
        }
        return fieldConfig;
    }

    @Override
    public void export(FieldConfigParam fieldConfigParam) {
        List<FieldConfig> list = this.list(fieldConfigParam);
        PoiUtil.exportExcelWithStream("SnowyFieldConfig.xls", FieldConfig.class, list);
    }

    /**
     * 校验参数，检查是否存在相同的名称和索引
     *
     * @author xuyuxiang
     * @date 2020/3/25 21:23
     */
    private void checkParam(FieldConfigParam fieldConfigParam, boolean isExcludeSelf) {
        Long id = fieldConfigParam.getId();
        String tableName = fieldConfigParam.getTableName();
        String fieldIndex = fieldConfigParam.getFieldIndex();
        String fieldTitle = fieldConfigParam.getFieldTitle();

        String entity = tableName.substring(3);
        String finalString = upperTable(entity);
        String s = finalString.toLowerCase();
//        String result = initcap(finalString);

//        Class<?> clazz=result.getClass();
        Class<?> clazz = null;
        try {
            clazz = Class.forName("vip.xiaonuo.modular."+ s +".entity."+finalString);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if(field.getName().equals(fieldIndex)){
                throw new ServiceException(4, "字段索引重复，请检查索引参数");
            }
            Object value = AnnotationUtil.getAnnotationValue(field,Excel.class,"name");
            if(ObjectUtil.isNotEmpty(value) && value.equals(fieldTitle)){
                throw new ServiceException(3, "字段名称重复，请检查名称参数");
            }
        }


        LambdaQueryWrapper<FieldConfig> queryWrapperByIndex = new LambdaQueryWrapper<>();
        queryWrapperByIndex.eq(FieldConfig::getTableName, tableName);
        queryWrapperByIndex.eq(FieldConfig::getFieldIndex, fieldIndex);

        LambdaQueryWrapper<FieldConfig> queryWrapperByTitle = new LambdaQueryWrapper<>();
        queryWrapperByTitle.eq(FieldConfig::getTableName, tableName);
        queryWrapperByTitle.eq(FieldConfig::getFieldTitle, fieldTitle);
        //如果是编辑，校验名称编码时排除自身
        if (isExcludeSelf) {
            queryWrapperByIndex.ne(FieldConfig::getId, id);
            queryWrapperByTitle.ne(FieldConfig::getId, id);
        }

        int countByIndex = this.count(queryWrapperByIndex);
        int countByTitle = this.count(queryWrapperByTitle);

        if (countByTitle >= 1) {
            throw new ServiceException(3, "字段名称重复，请检查名称参数");
        }
        if (countByIndex >= 1) {
            throw new ServiceException(4, "字段索引重复，请检查索引参数");
        }
    }

    /**
     * 方法说明 :将首字母和带 _ 后第一个字母 转换成大写
     *
     * @return :String
     * @author :czw
     */
    private String upperTable(String str) {
        // 字符串缓冲区
        StringBuffer sbf = new StringBuffer();
        // 如果字符串包含 下划线
        if (str.contains("_"))
        {
            // 按下划线来切割字符串为数组
            String[] split = str.split("_");
            // 循环数组操作其中的字符串
            for (int i = 0, index = split.length; i < index; i++)
            {
                // 递归调用本方法
                String upperTable = upperTable(split[i]);
                // 添加到字符串缓冲区
                sbf.append(upperTable);
            }
        } else
        {// 字符串不包含下划线
            // 转换成字符数组
            char[] ch = str.toCharArray();
            // 判断首字母是否是字母
            if (ch[0] >= 'a' && ch[0] <= 'z')
            {
                // 利用ASCII码实现大写
                ch[0] = (char) (ch[0] - 32);
            }
            // 添加进字符串缓存区
            sbf.append(ch);
        }
        // 返回
        return sbf.toString();
    }

}
