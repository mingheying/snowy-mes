/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.invOut.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.invOut.param.InvOutParam;
import vip.xiaonuo.modular.invOut.service.InvOutService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 出库单控制器
 *
 * @author wz
 * @date 2022-06-06 15:55:34
 */
@RestController
public class InvOutController {

    @Resource
    private InvOutService invOutService;

    /**
     * 查询出库单
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @GetMapping("/invOut/page")
    @BusinessLog(title = "出库单_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(InvOutParam invOutParam) {
        return new SuccessResponseData(invOutService.page(invOutParam));
    }

    /**
     * 添加出库单
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @PostMapping("/invOut/add")
    @BusinessLog(title = "出库单_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(InvOutParam.add.class) InvOutParam invOutParam) {
            invOutService.add(invOutParam);
        return new SuccessResponseData();
    }

    /**
     * 删除出库单，可批量删除
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @PostMapping("/invOut/delete")
    @BusinessLog(title = "出库单_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(InvOutParam.delete.class) List<InvOutParam> invOutParamList) {
            invOutService.delete(invOutParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑出库单
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @PostMapping("/invOut/edit")
    @BusinessLog(title = "出库单_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(InvOutParam.edit.class) InvOutParam invOutParam) {
            invOutService.edit(invOutParam);
        return new SuccessResponseData();
    }

    /**
     * 查看出库单
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @GetMapping("/invOut/detail")
    @BusinessLog(title = "出库单_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(InvOutParam.detail.class) InvOutParam invOutParam) {
        return new SuccessResponseData(invOutService.detail(invOutParam));
    }

    /**
     * 出库单列表
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @GetMapping("/invOut/list")
    @BusinessLog(title = "出库单_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(InvOutParam invOutParam) {
        return new SuccessResponseData(invOutService.list(invOutParam));
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-06-06 15:55:34
     */
    @Permission
    @GetMapping("/invOut/export")
    @BusinessLog(title = "出库单_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(InvOutParam invOutParam) {
        invOutService.export(invOutParam);
    }

}
