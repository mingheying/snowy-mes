/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.invdetail.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.invdetail.param.InvDetailParam;
import vip.xiaonuo.modular.invdetail.service.InvDetailService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 出入库明细控制器
 *
 * @author wz
 * @date 2022-06-10 14:27:42
 */
@RestController
public class InvDetailController {

    @Resource
    private InvDetailService invDetailService;

    /**
     * 查询出入库明细
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @GetMapping("/invDetail/page")
    @BusinessLog(title = "出入库明细_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(InvDetailParam invDetailParam) {
        return new SuccessResponseData(invDetailService.page(invDetailParam));
    }

    /**
     * 添加出入库明细
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @PostMapping("/invDetail/add")
    @BusinessLog(title = "出入库明细_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(InvDetailParam.add.class) InvDetailParam invDetailParam) {
            invDetailService.add(invDetailParam);
        return new SuccessResponseData();
    }

    /**
     * 删除出入库明细，可批量删除
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @PostMapping("/invDetail/delete")
    @BusinessLog(title = "出入库明细_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(InvDetailParam.delete.class) List<InvDetailParam> invDetailParamList) {
            invDetailService.delete(invDetailParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑出入库明细
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @PostMapping("/invDetail/edit")
    @BusinessLog(title = "出入库明细_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(InvDetailParam.edit.class) InvDetailParam invDetailParam) {
            invDetailService.edit(invDetailParam);
        return new SuccessResponseData();
    }

    /**
     * 查看出入库明细
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @GetMapping("/invDetail/detail")
    @BusinessLog(title = "出入库明细_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(InvDetailParam.detail.class) InvDetailParam invDetailParam) {
        return new SuccessResponseData(invDetailService.detail(invDetailParam));
    }

    /**
     * 出入库明细列表
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @GetMapping("/invDetail/list")
    @BusinessLog(title = "出入库明细_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(InvDetailParam invDetailParam) {
        return new SuccessResponseData(invDetailService.list(invDetailParam));
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-06-10 14:27:42
     */
    @Permission
    @GetMapping("/invDetail/export")
    @BusinessLog(title = "出入库明细_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(InvDetailParam invDetailParam) {
        invDetailService.export(invDetailParam);
    }

}
