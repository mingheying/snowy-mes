/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.protype.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.DataScope;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.protype.param.ProTypeParam;
import vip.xiaonuo.modular.protype.service.ProTypeService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 产品类型表控制器
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
@RestController
public class ProTypeController {

    @Resource
    private ProTypeService proTypeService;

    /**
     * 查询产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @GetMapping("/proType/page")
    @BusinessLog(title = "产品类型表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(ProTypeParam proTypeParam) {
        return new SuccessResponseData(proTypeService.page(proTypeParam));
    }

    /**
     * 添加产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @PostMapping("/proType/add")
    @BusinessLog(title = "产品类型表_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(ProTypeParam.add.class) ProTypeParam proTypeParam) {
            proTypeService.add(proTypeParam);
        return new SuccessResponseData();
    }

    /**
     * 删除产品类型表，可批量删除
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @PostMapping("/proType/delete")
    @BusinessLog(title = "产品类型表_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(ProTypeParam.delete.class) List<ProTypeParam> proTypeParamList) {
            proTypeService.delete(proTypeParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @PostMapping("/proType/edit")
    @BusinessLog(title = "产品类型表_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(ProTypeParam.edit.class) ProTypeParam proTypeParam) {
            proTypeService.edit(proTypeParam);
        return new SuccessResponseData();
    }

    /**
     * 查看产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @GetMapping("/proType/detail")
    @BusinessLog(title = "产品类型表_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(ProTypeParam.detail.class) ProTypeParam proTypeParam) {
        return new SuccessResponseData(proTypeService.detail(proTypeParam));
    }

    /**
     * 产品类型表列表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @GetMapping("/proType/list")
    @BusinessLog(title = "产品类型表_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(ProTypeParam proTypeParam) {
        return new SuccessResponseData(proTypeService.list(proTypeParam));
    }

    /**
     * 导出系统用户
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    @Permission
    @GetMapping("/proType/export")
    @BusinessLog(title = "产品类型表_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(ProTypeParam proTypeParam) {
        proTypeService.export(proTypeParam);
    }

    /**
     * 获取组织机构树
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:55
     */
    @Permission
    @DataScope
    @GetMapping("/proType/tree")
    @BusinessLog(title = "产品类型树", opType = LogAnnotionOpTypeEnum.TREE)
    public ResponseData tree(ProTypeParam proTypeParam) {
        return new SuccessResponseData(proTypeService.tree(proTypeParam));
    }

}
