/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.task.param;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
* 任务参数类
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
*/
@Data
@Accessors(chain = true)
public class TaskParam extends BaseParam {

    /**
     * id
     */
    @NotNull(message = "id不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 排序号
     */
    private Integer sortNum;

    /**
     * 编码
     */
//    @NotBlank(message = "编码不能为空，请检查code参数", groups = {add.class, edit.class})
    private String code;

    /**
     * 工单id
     */
    @NotNull(message = "工单id不能为空，请检查workOrderId参数", groups = {add.class, edit.class})
    private Long workOrderId;

    /**
     * 工序id
     */
//    @NotNull(message = "工序id不能为空，请检查workStepId参数", groups = {add.class, edit.class})
    private String workStepId;

    /**
     * 产品类型id
     */
//    @NotNull(message = "产品类型id不能为空，请检查proTypeId参数", groups = {add.class, edit.class})
    private Long proTypeId;

    /**
     * 产品id
     */
    @NotNull(message = "产品id不能为空，请检查proId参数", groups = {add.class, edit.class})
    private Long proId;

    /**
     * 计划开始时间
     */
    //  @NotNull(message = "计划开始时间不能为空，请检查plaStartTime参数", groups = {add.class, edit.class})

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "计划开始时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
    // @Excel(name = "计划开始时间")
    private String plaStartTime;

    /**
     * 计划结束时间
     */
    //  @NotNull(message = "计划结束时间不能为空，请检查plaEndTime参数", groups = {add.class, edit.class})
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "计划结束时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
    //  @Excel(name = "计划结束时间")
    private String plaEndTime;

    /**
     * 实际开始时间
     */
    //@NotNull(message = "实际开始时间不能为空，请检查factStaTime参数", groups = {add.class, edit.class})
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone="GMT+8")
    @Excel(name = "实际开始时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd ", width = 20)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    // @Excel(name = "实际开始时间")
    private String factStaTime;

    /**
     * 实际结束时间
     */
    //@NotNull(message = "实际结束时间不能为空，请检查factEndTime参数", groups = {add.class, edit.class})
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm" ,timezone="GMT+8")
    @Excel(name = "实际结束时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
    // @Excel(name = "实际结束时间")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String factEndTime;

    /**
     * 计划数
     */
    @NotNull(message = "计划数不能为空，请检查plaNum参数", groups = {add.class, edit.class})
    private Integer plaNum;

    /**
     * 良品数
     */
//    @NotNull(message = "良品数不能为空，请检查goodNum参数", groups = {add.class, edit.class})
    private Integer goodNum;

    /**
     * 不良品数
     */
//    @NotNull(message = "不良品数不能为空，请检查badNum参数", groups = {add.class, edit.class})
    private Integer badNum;

    /**
     * 状态
     */
    @NotNull(message = "状态不能为空，请检查status参数", groups = {add.class, edit.class})
    private Integer status;


    /**
     * 计划时间
     */
//    @NotNull(message = "计划时间段不能为空，请检查startEndTime参数", groups = {add.class, edit.class})
    private String startEndTime;

    /**
     * 实际时间
     */
//    @NotNull(message = "实际时间段不能为空，请检查startEndTime参数", groups = {add.class, edit.class})
    private String factTime;
    /**
     * 报工权限
     */
    private  String reportRight;
    /**
     * 不良品项
     */
//    private  String mulBadItem;

}
