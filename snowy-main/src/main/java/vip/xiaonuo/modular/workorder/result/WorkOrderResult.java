package vip.xiaonuo.modular.workorder.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.task.entity.Task;
import vip.xiaonuo.modular.workorder.entity.WorkOrder;

import java.util.Date;
import java.util.List;

@Data
public class WorkOrderResult extends WorkOrder {
    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String proName;

    /**
     * 生产进度
     */
    private List<Integer> progressRateList;

    /**
     * 报工时长
     */
    private Integer reportWorkTime;


    /**
     * 生产任务
     */
    private List<Task> taskList;


    /**
     * 负责人名称
     */
    private String personChargeName;

    /**
     * 下单名称
     */
    private String nextPersonName;

    /**
     * 关联单据编号
     */
    private String workBillCode;

}
