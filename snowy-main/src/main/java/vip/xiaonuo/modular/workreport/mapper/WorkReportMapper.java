/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workreport.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.task.result.TaskResult;
import vip.xiaonuo.modular.workreport.entity.WorkReport;
import vip.xiaonuo.modular.workreport.param.WorkReportParam;
import vip.xiaonuo.modular.workreport.result.WorkReportResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * 报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
@Repository
public interface WorkReportMapper extends BaseMapper<WorkReport> {
    @Select("select r.*,s.name stepName,o.work_order_no as workOrderNo,t.work_step_id,t.status ,u.name as productionUserName from dw_work_report r " +
            "left join dw_work_order  o on r.work_order_id = o.id " +
            "left join dw_task t on r.task_id = t.id\n " +
            "left join dw_work_step  s on s.id=t.work_step_id "+
            "left join sys_user u on u.id = r.production_user"
            + " ${ew.customSqlSegment}")
    Page<WorkReportResult> page(@Param("page") Page page, @Param("ew") QueryWrapper queryWrapper);

    List<WorkReportResult> workReportList(@Param("ew") QueryWrapper queryWrapper);
}


