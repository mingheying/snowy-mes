/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workreport.param;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
* 报工表参数类
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
*/
@Data
@Accessors
public class WorkReportParam extends BaseParam {

    /**
     * 报工表主键Id
     */
    @NotNull(message = "报工表主键Id不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;
    /**
     * 工单id
     */
    @NotNull(message = "工单id不能为空，请检查workOrderId参数", groups = {add.class, edit.class})
    private Long workOrderId;

    /**
     * 任务id
     */
    @NotNull(message = "任务id不能为空，请检查taskId参数", groups = {add.class, edit.class})
    private Long taskId;

//    /**
//     * 工序状态
//     */
//    @NotNull(message = "工序状态不能为空，请检查stepStatus参数", groups = {add.class, edit.class})
//    private Integer stepStatus;

    /**
     * 生产人员
     */
    private Long productionUser;

    /**
     * 报工数
     */
    @NotNull(message = "报工数不能为空，请检查reportNum参数", groups = {add.class, edit.class})
    private Integer reportNum;
    /**
     * 良品数
     */
    @NotNull(message = "良品数不能为空，请检查goodNum参数", groups = {add.class, edit.class})
    private Integer goodNum;
    /**
     * 不良品数
     */
    @NotNull(message = "不良品数不能为空，请检查badNum参数", groups = {add.class, edit.class})
    private Integer badNum;

    /**
     * 不良品项
     */
//    @NotBlank(message = "不良品项不能为空，请检查mulBadItem参数", groups = {add.class, edit.class})
//    private String mulBadItem;



    /**
     * 开始时间
     */
//    @NotNull(message = "开始时间不能为空，请检查startTime参数", groups = {add.class, edit.class})
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private String startTime;
    /**
     * 结束时间
     */
//    @NotNull(message = "结束时间不能为空，请检查endTime参数", groups = {add.class, edit.class})
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private String endTime;
    /**
     * 工作时长
     */
    private Integer workTime;
    //备注
    private String remarks;
    //区分从何处添加
    private Integer   distinction;



    /**
     * 工作时间
     */
    @NotNull(message = "工作时间段不能为空，请检查startEndTime参数", groups = {add.class, edit.class})
    private String startEndTime;
    //工序姓名
    private String stepName;
    //工单编号
    private String workOrderNo;
    //任务状态
private  Integer status;
//工序Id
     private Long stepId ;
     //是否审批，1为已审批，0为未审批
    private Integer approval;
}
