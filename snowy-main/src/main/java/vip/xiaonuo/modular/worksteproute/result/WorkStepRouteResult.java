package vip.xiaonuo.modular.worksteproute.result;

import lombok.Data;
import vip.xiaonuo.modular.worksteproute.entity.WorkStepRoute;
@Data
public class WorkStepRouteResult extends WorkStepRoute {
    //工艺路线名称
    private String workRouteName;
    //工序名称
    private String workStepName;
    //报工权限
    private String reportRight;
    //报工权限名称
    private String reportRightName;

}
