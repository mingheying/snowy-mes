/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.worksteproute.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.worksteproute.entity.WorkStepRoute;
import vip.xiaonuo.modular.worksteproute.param.WorkStepRouteParam;
import vip.xiaonuo.modular.worksteproute.result.WorkStepRouteResult;

import java.util.List;

/**
 * 工序路线关系表service接口
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
public interface WorkStepRouteService extends IService<WorkStepRoute> {

    /**
     * 查询工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    PageResult<WorkStepRoute> page(WorkStepRouteParam workStepRouteParam);

    /**
     * 工序路线关系表列表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     * @return
     */
    List<WorkStepRouteResult> list(WorkStepRouteParam workStepRouteParam);

    /**
     * 添加工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    void add(WorkStepRouteParam workStepRouteParam);
/**
 * 添加复杂工序路线关系表
 *
 * @author 楊楊
 * @date 2022-05-31 11点06分
 */
    void add(Long routeId , List<WorkStepRouteParam> workStepRouteParamList);

    /**
     * 删除工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    void delete(List<WorkStepRouteParam> workStepRouteParamList);
    /**
     * 编辑工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
    void edit(WorkStepRouteParam workStepRouteParam);

    /**
     * 查看工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
     WorkStepRoute detail(WorkStepRouteParam workStepRouteParam);

    /**
     * 导出工序路线关系表
     *
     * @author 楊楊
     * @date 2022-05-27 15:12:44
     */
     void export(WorkStepRouteParam workStepRouteParam);

}